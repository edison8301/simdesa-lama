<?php

class AnggaranController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','export','penerimaan','pengeluaran','exportAll','exportWord','exportExcel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public $menu = array(
			array('label'=>'Tambah Anggaran','url'=>array('create'),'icon'=>'plus'),
			array('label'=>'Kelola Anggaran','url'=>array('admin'),'icon'=>'list'),
			array('label'=>'Export Word','url'=>array('exportWord'),'icon'=>'download-alt'),
			array('label'=>'Export Excel','url'=>array('export'),'icon'=>'download-alt'),
	);

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionPenerimaan()
	{
		$this->render('penerimaan');
	}
	
	public function actionPengeluaran()
	{
		$this->render('pengeluaran');
	}
	
	public function actionExport($id)
	{
		$this->render('export',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExportAll()
	{
		$this->render('export_all',array(
			'model'=>$this->loadModel($id),
		));
	}
	

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	
	public function actionCreate()
	{
		$model=new Anggaran;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
			
		if(isset($_POST['Anggaran']))
		{
			$model->attributes=$_POST['Anggaran'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Anggaran']))
		{
			$model->attributes=$_POST['Anggaran'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else	
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Anggaran');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Anggaran('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Anggaran']))
			$model->attributes=$_GET['Anggaran'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionExportWord()
	{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPWord',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));
		
			$PHPWord = new PHPWord();
		
			$PHPWord->addFontStyle('bold', array('bold'=>true));
			$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
			$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
			$tableStyle = array(
						'borderSize'=>1, 
						'borderColor'=>'000000', 
						'cellMargin'=>80,
						'border'=>true
			);
			
			$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
			$center = array('align'=>'center');
		
			$paraStyle = array('spaceAfter'=>'2');
			$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
			$section = $PHPWord->createSection(array('orientation'=>'landscape','marginLeft'=>500, 'marginRight'=>500, 'marginTop'=>500, 'marginBottom'=>500));
		
			$section->addText('DATA Anggaran',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
			$section->addTextBreak(1);		
		
			$table = $section->addTable('tableStyle');
		
			$table->addRow();
			
			$table->addCell(1000)->addText("No",array('bold'=>'true'),$headStyle);
			
						$table->addCell(1000)->addText('id',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('anggaran_jenis_id',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('kode_1',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('kode_2',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('kode_3',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('kode_4',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('kode_5',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('kode_6',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('uraian',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('jumlah',array('bold'=>'true'),$headStyle);
						$table->addCell(1000)->addText('keterangan',array('bold'=>'true'),$headStyle);
						
			$i=1;
		
			foreach(Anggaran::model()->findAll() as $data)
			{
				$table->addRow();
				$table->addCell(100)->addText($i,array(),$paraStyle);
								$table->addCell(100)->addText($data->id,array(),$paraStyle);
								$table->addCell(100)->addText($data->anggaran_jenis_id,array(),$paraStyle);
								$table->addCell(100)->addText($data->kode_1,array(),$paraStyle);
								$table->addCell(100)->addText($data->kode_2,array(),$paraStyle);
								$table->addCell(100)->addText($data->kode_3,array(),$paraStyle);
								$table->addCell(100)->addText($data->kode_4,array(),$paraStyle);
								$table->addCell(100)->addText($data->kode_5,array(),$paraStyle);
								$table->addCell(100)->addText($data->kode_6,array(),$paraStyle);
								$table->addCell(100)->addText($data->uraian,array(),$paraStyle);
								$table->addCell(100)->addText($data->jumlah,array(),$paraStyle);
								$table->addCell(100)->addText($data->keterangan,array(),$paraStyle);
								
				$i++;
			}		
	
			$section->addTextBreak(1);		
		
			$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
			$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
			$filename = time().'_Anggaran.docx';
		
			$objWriter->save($pathFile.$filename);
		
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
		
	
	}
	
	public function actionExportExcel()
	{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$PHPExcel = new PHPExcel();
			
			$PHPExcel->getActiveSheet()->setCellValue('A1', 'DATA Anggaran');
		
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
			
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'id');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'anggaran_jenis_id');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'kode_1');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'kode_2');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'kode_3');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'kode_4');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'kode_5');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'kode_6');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'uraian');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'jumlah');
						$PHPExcel->getActiveSheet()->setCellValue('B3', 'keterangan');
						
			$i = 1;
			$kolom = 4;

			foreach(Anggaran::model()->findAll() as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->id);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->anggaran_jenis_id);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->kode_1);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->kode_2);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->kode_3);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->kode_4);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->kode_5);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->kode_6);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->uraian);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->jumlah);
								$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->keterangan);
									
				$i++; $kolom++;
			}
	
			$filename = time().'_Anggaran.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=Anggaran::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='anggaran-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
