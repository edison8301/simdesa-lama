<?php

class KasController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','export','exportAll','penerimaan','pengeluaran','umum','exportExcel','exportWord'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExportWord()
	{
		if(isset($_POST['proses']) AND $_POST['proses']==1)
		{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPWord',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));
		
			$PHPWord = new PHPWord();
		
			$PHPWord->addFontStyle('bold', array('bold'=>true));
			$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
			$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
			$tableStyle = array(
						'borderSize'=>1, 
						'borderColor'=>'000000', 
						'cellMargin'=>80,
						'border'=>true
					);
		
			$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
			$center = array('align'=>'center');
		
			$paraStyle = array('spaceAfter'=>'2');
			$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
			$section = $PHPWord->createSection(array('marginLeft'=>750, 'marginRight'=>750, 'marginTop'=>750, 'marginBottom'=>750));
			
			if($_POST['jenis']==1)
			{
				$section->addText('BUKU KAS UMUM',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
				$section->addTextBreak(1);		
		
				$table = $section->addTable('tableStyle');
		
				$table->addRow();
				$table->addCell(500)->addText("No",array('bold'=>'true'),$headStyle);
				$table->addCell(1500)->addText("Tanggal",array('bold'=>'true'),$headStyle);
				$table->addCell(1500)->addText("Kode",array('bold'=>'true'),$headStyle);
				$table->addCell(4000)->addText("Uraian",array('bold'=>'true'),$headStyle);
				$table->addCell(2000)->addText("Penerimaan",array('bold'=>'true'),$headStyle);
				$table->addCell(2000)->addText("Pengeluaran",array('bold'=>'true'),$headStyle);
				$i=1;
		
				foreach(Kas::getDataKeuangan() as $data)
				{
					$table->addRow();
					$table->addCell(100)->addText($i,array(),$paraStyle);
					$table->addCell(100)->addText($data->tanggal,array(),$paraStyle);
					$table->addCell(100)->addText($data->getKodeAnggaran(),array(),$paraStyle);
					$table->addCell(100)->addText($data->uraian,array(),$paraStyle);
					
					if($data->kas_jenis_id == 1) {
						
						$table->addCell(100)->addText(Bantu::rp($data->jumlah),array(),$paraStyle);
						$table->addCell(100)->addText("",array(),$paraStyle);
					
					}
					
					if($data->kas_jenis_id == 2) {
						
						$table->addCell(100)->addText("",array(),$paraStyle);
						$table->addCell(100)->addText(Bantu::rp($data->jumlah),array(),$paraStyle);
					
					}
					
					$i++;
					
				}
				
				$filename = time().'_kas_umum.docx';
			} 
			
			if($_POST['jenis']==2)
			{
				$section->addText('BUKU KAS PENERIMAAN',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
				$section->addTextBreak(1);		
		
				$table = $section->addTable('tableStyle');
		
				$table->addRow();
				$table->addCell(500)->addText("No",array('bold'=>'true'),$headStyle);
				$table->addCell(5000)->addText("Tanggal",array('bold'=>'true'),$headStyle);
				$table->addCell(1000)->addText("Kode",array('bold'=>'true'),$headStyle);
				$table->addCell(2000)->addText("Uraian",array('bold'=>'true'),$headStyle);
				$table->addCell(3000)->addText("Penerimaan",array('bold'=>'true'),$headStyle);
		
				$i=1;
		
				foreach(Kas::getDataKeuangan('penerimaan') as $data)
				{
					$table->addRow();
					$table->addCell(100)->addText($i,array(),$paraStyle);
					$table->addCell(100)->addText($data->tanggal,array(),$paraStyle);
					$table->addCell(100)->addText($data->getKodeAnggaran(),array(),$paraStyle);
					$table->addCell(100)->addText($data->uraian,array(),$paraStyle);	
					$table->addCell(100)->addText("",array(),$paraStyle);
					$table->addCell(100)->addText(Bantu::rp($data->jumlah),array(),$paraStyle);
					$i++;
				}
				
				$filename = time().'_kas_penerimaan.docx';
			}
			
			if($_POST['jenis']==3)
			{
				$section->addText('BUKU KAS PENGELUARAN',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
				$section->addTextBreak(1);		
		
				$table = $section->addTable('tableStyle');
		
				$table->addRow();
				$table->addCell(500)->addText("No",array('bold'=>'true'),$headStyle);
				$table->addCell(5000)->addText("Tanggal",array('bold'=>'true'),$headStyle);
				$table->addCell(1000)->addText("Kode",array('bold'=>'true'),$headStyle);
				$table->addCell(2000)->addText("Uraian",array('bold'=>'true'),$headStyle);
				$table->addCell(2000)->addText("Pengeluaran",array('bold'=>'true'),$headStyle);
				
				$i=1;
		
				foreach(Kas::getDataKeuangan('pengeluaran') as $data)
				{
					$table->addRow();
					$table->addCell(500)->addText($i,array('bold'=>'true'),$headStyle);
					$table->addCell(5000)->addText($data->tanggal,array('bold'=>'true'),$headStyle);
					$table->addCell(1000)->addText($data->getKodeAnggaran(),array('bold'=>'true'),$headStyle);
					$table->addCell(2000)->addText($data->uraian,array('bold'=>'true'),$headStyle);
					$table->addCell(3000)->addText(Bantu::rp($data->jumlah),array('bold'=>'true'),$headStyle);
					
					$i++; $kolom++;
				}
				
				$filename = time().'_kas_pengeluaran.docx';
			}
	
			$section->addTextBreak(1);		
		
			$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
			$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
			$objWriter->save($pathFile.$filename);
		
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
		} else {
		
			$this->render('exportWord');
		}
	}
	
	public function actionExportExcel()
	{
		if(isset($_POST['proses']) AND $_POST['proses']==1)
		{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$PHPExcel = new PHPExcel();
			
			if($_POST['jenis']==1)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A1', 'BUKU KAS UMUM');
		
				$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
				$PHPExcel->getActiveSheet()->setCellValue('B3', 'Tanggal');
				$PHPExcel->getActiveSheet()->setCellValue('C3', 'Nomor Buku');
				$PHPExcel->getActiveSheet()->setCellValue('D3', 'Uraian');
				$PHPExcel->getActiveSheet()->setCellValue('E3', 'Penerimaan');
				$PHPExcel->getActiveSheet()->setCellValue('F3', 'Pengeluaran');
			
				$i = 1;
				$kolom = 4;
				foreach(Kas::getDataKeuangan('umum') as $data)
				{
					$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
					$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->tanggal);
					$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->getKodeAnggaran());
					$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->uraian);
				
					if($data->kas_jenis_id == 1)
						$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->jumlah);
				
					if($data->kas_jenis_id == 2)				
						$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->jumlah);
					
					$i++; $kolom++;
				}
		
				$filename = time().'_kas_umum.xlsx';
		
			}
			
			if($_POST['jenis']==2)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A1', 'BUKU KAS PENERIMAAN');
		
				$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
				$PHPExcel->getActiveSheet()->setCellValue('B3', 'Tanggal');
				$PHPExcel->getActiveSheet()->setCellValue('C3', 'Nomor Buku');
				$PHPExcel->getActiveSheet()->setCellValue('D3', 'Uraian');
				$PHPExcel->getActiveSheet()->setCellValue('E3', 'Jumlah');
			
				$i = 1;
				$kolom = 4;
				foreach(Kas::getDataKeuangan('penerimaan') as $data)
				{
					$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
					$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->tanggal);
					$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->getKodeAnggaran());
					$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->uraian);
					$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->jumlah);
					
					$i++; $kolom++;
				}
	
				$filename = time().'_kas_penerimaan.xlsx';

			}
			
			if($_POST['jenis']==3)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A1', 'BUKU KAS PENERIMAAN');
		
				$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
				$PHPExcel->getActiveSheet()->setCellValue('B3', 'Tanggal');
				$PHPExcel->getActiveSheet()->setCellValue('C3', 'Nomor Buku');
				$PHPExcel->getActiveSheet()->setCellValue('D3', 'Uraian');
				$PHPExcel->getActiveSheet()->setCellValue('E3', 'Jumlah');
			
				$i = 1;
				$kolom = 4;
				foreach(Kas::getDataKeuangan('pengeluaran') as $data)
				{
					$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
					$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->tanggal);
					$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->getKodeAnggaran());
					$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->uraian);
					$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->jumlah);
					
					$i++;
				}
	
				$filename = time().'_kas_pengeluaran.xlsx';

			}
			
			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
			
		} else {
			
			$this->render('exportExcel');
		
		}
	
	}
	
	public function actionUmum()
	{
		$this->render('umum');
	}
	
	public function actionPenerimaan()
	{
		$this->render('penerimaan');
	}
	
	public function actionExcel()
	{
		if(!empty($_POST['excel']))
		{	
			$this->layout = 'excel';
			
			if($_POST['jenis']==1)
				$this->render('umumExcel');
			
			if($_POST['jenis']==2)
				$this->render('penerimaanExcel');
			
			if($_POST['jenis']==3)
				$this->render('pengeluaranExcel');
		} else {
			$this->render('excel');
		}
	}
	
	public function actionPengeluaran()
	{
		
		$this->render('pengeluaran');
	}
	
	public function actionExport($id)
	{
		$this->render('export',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	
	public function actionExportAll()
	{
		$this->render('export_all',array(
			'model'=>$this->loadModel($id),
		));
	}
	

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Kas;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_GET['kas_jenis_id']))
			$model->kas_jenis_id = $_GET['kas_jenis_id'];
			
		if(isset($_POST['Kas']))
		{
			$model->attributes=$_POST['Kas'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				
				if($model->kas_jenis_id==1)
					$this->redirect(array('penerimaan'));
				
				if($model->kas_jenis_id==2)
					$this->redirect(array('pengeluaran'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kas']))
		{
			$model->attributes=$_POST['Kas'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				
				if($model->kas_jenis_id==1)
					$this->redirect(array('penerimaan'));
				
				if($model->kas_jenis_id==2)
					$this->redirect(array('pengeluaran'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else	
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Kas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Kas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Kas']))
			$model->attributes=$_GET['Kas'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=Kas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='kas-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
