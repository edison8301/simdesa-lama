<?php

class PendudukController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/	
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'report'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','export','exportAll','print','printAll',
					'exportWord','exportExcel','usiaBalita','wajibBelajar','pemilihPemilu',
					'pekerjaan','pendidikan','agama','reportPendidikan','reportAgama','reportPekerjaan',
					'reportKedudukanDalamKeluarga','reportWord','reportExcel','reportStatusPerkawinan',
					'reportKewarganegaraan','reportKelompokUmur','reportKelompokUmurWord','reportDusun',
					'reportKelompokUmurExcel','mutasi','rekapitulasi','mutasiWord','mutasiExcel',
					'chart','rekapitulasiExcel',
				),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','report'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public $menu = array(
		array('label'=>'Tambah Penduduk','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Seluruh Penduduk','url'=>array('admin'),'icon'=>'tags'),
		array('label'=>'Usia Balita','url'=>array('usiaBalita'),'icon'=>'tags'),
		array('label'=>'Wajib Belajar','url'=>array('wajibBelajar'),'icon'=>'tags'),
		array('label'=>'Pemilih Pemilu','url'=>array('pemilihPemilu'),'icon'=>'tags'),
		array('label'=>'Export Word','url'=>array('exportWord'),'icon'=>'download-alt'),
		array('label'=>'Export Excel','url'=>array('exportExcel'),'icon'=>'download-alt'),
		array('label'=>'Laporan Pendidikan','url'=>array('reportPendidikan'),'icon'=>'tags'),
		array('label'=>'Laporan Agama','url'=>array('reportAgama'),'icon'=>'tags'),
		array('label'=>'Laporan Pekerjaan','url'=>array('reportPekerjaan'),'icon'=>'tags'),
		array('label'=>'Laporan Kedudukan Dalam Keluarga','url'=>array('reportKedudukanDalamKeluarga'),'icon'=>'tags'),
	);
/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExport($id)
	{
		$this->layout = '//layouts/excel';
		
		$model = $this->loadModel($id);
		
		$this->render('export',array(
			'model'=>$model,
		));
	}
	
	public function actionExportAll()
	{
		if(isset($_POST['proses']) AND $_POST['proses']=='1')
				$this->layout = '//layouts/excel';
			
		$this->render('export_all');
	}
	
	public function actionPrintAll()
	{
		if(isset($_POST['proses']) AND $_POST['proses']=='1')
				$this->layout = '//layouts/print';
			
		$this->render('print_all');
	}
	
	public function actionPrint($id)
	{
		$this->layout = 'print';
		
		$model = $this->loadModel($id);
		
		$this->render('print',array(
			'model'=>$model,
		));
	}


/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Penduduk;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
			
		if(isset($_POST['Penduduk']))
		{
			$model->attributes=$_POST['Penduduk'];
			
			if($model->mutasi_tambah_id==1)
				$model->mutasi_tambah_tanggal = $model->tanggal_lahir;
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Penduduk']))
		{
			$model->attributes=$_POST['Penduduk'];
			
			if($model->mutasi_tambah_id==1)
				$model->mutasi_tambah_tanggal = $model->tanggal_lahir;
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionMutasi()
	{
		$this->render('mutasi');
	}
	
	public function actionRekapitulasi()
	{
		$this->render('rekapitulasi');
	}
	
	public function actionRekapitulasiExcel()
	{
		$this->layout = 'excel';
		$this->render('rekapitulasi');
	}
	
	public function actionReportPendidikan()
	{
		$this->render('reportPendidikan');
	}
	
	public function actionReportDusun()
	{
		$this->render('reportDusun');
	}
	
	public function actionChart($kategori)
	{
		if($kategori=='pekerjaan')
			$model = new Pekerjaan;
			
		if($kategori=='agama')
			$model = new Agama;
		
		Yii::app()->fusioncharts->setChartOptions(array('caption'=>'Laporan Penduduk'));
		
		foreach($model->findAll() as $data) 
		{
			Yii::app()->fusioncharts->addSet(array('label'=>$data->nama, 'value'=>$data->countPenduduk()));	
		}
		
		Yii::app()->fusioncharts->getXMLData(true);
	}
	
	public function actionReportKelompokUmur()
	{
		$this->render('reportKelompokUmur');
	}
	
	public function actionReportKelompokUmurWord()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPWord',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));
		
		$PHPWord = new PHPWord();
		
		$PHPWord->addFontStyle('bold', array('bold'=>true));
		$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
		$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
		$tableStyle = array(
					'borderSize'=>1, 
					'borderColor'=>'000000', 
					'cellMargin'=>80,
					'border'=>true
		);
			
		$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
		$center = array('align'=>'center');
		
		$paraStyle = array('spaceAfter'=>'2','align'=>'center');
		$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
		$section = $PHPWord->createSection(array('marginLeft'=>2000, 'marginRight'=>2000, 'marginTop'=>1000, 'marginBottom'=>1000));
		
		$section->addText('LAPORAN KELOMPOK UMUR',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
		$section->addTextBreak(1);		
		
		$table = $section->addTable('tableStyle');
		
		$table->addRow();
		$table->addCell(500)->addText("No",array('bold'=>'true'),$headStyle);
		$table->addCell(3000)->addText("Kategori",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Laki-laki",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Perempuan",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Total",array('bold'=>'true'),$headStyle);
		
		$i=1;
		
		$total_lk = 0; $total_pr = 0; $total_lk_pr = 0;
		
		$i=1; $total_lk = 0; $total_pr = 0; $total_lk_pr = 0; $umur=0; $interval = 1;

		if(isset($_GET['interval'])) $interval = $_GET['interval'];
		
		while($umur<=100) 
		{

			$lk = Penduduk::model()->countByIntervalUmur($umur,$umur+$interval,'laki-laki'); 
			$pr = Penduduk::model()->countByIntervalUmur($umur,$umur+$interval,'perempuan');
			$lk_pr = $pr + $lk;
	
			$total_lk = $total_lk + $lk;
			$total_pr = $total_pr + $pr;
			$total_lk_pr = $total_lk_pr + $lk_pr;
	
			$umur_2 = $umur+$interval;
			
			$table->addRow();
			$table->addCell(100)->addText($i,array(),$paraStyle);
			$table->addCell(100)->addText($umur." - ".$umur_2." Tahun",array(),$paraStyle);
			$table->addCell(100)->addText($lk,array(),$paraStyle);
			$table->addCell(100)->addText($pr,array(),$paraStyle);
			$table->addCell(100)->addText($lk_pr,array(),$paraStyle);
			
			$umur = $umur + $interval;
			$i++;
			
		}		
		
		$table->addRow();
		$table->addCell(100)->addText('',array(),$paraStyle);
		$table->addCell(100)->addText('Total',array('bold'=>'true'),$headStyle);
		$table->addCell(100)->addText($total_lk,array('bold'=>'true'),$headStyle);
		$table->addCell(100)->addText($total_pr,array('bold'=>'true'),$headStyle);
		$table->addCell(100)->addText($total_lk_pr,array('bold'=>'true'),$headStyle);
		$section->addTextBreak(1);		
		
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
		$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
		$filename = time().'_LaporanKelompokUmur.docx';
		
		$objWriter->save($pathFile.$filename);
		
		$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}
	
	public function actionReportKelompokUmurExcel() 
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPExcel',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));

		$PHPExcel = new PHPExcel();
			
			
		$PHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN PENDUDUK');
		
		$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
		$PHPExcel->getActiveSheet()->setCellValue('B3', 'Kategori');
		$PHPExcel->getActiveSheet()->setCellValue('C3', 'Laki-laki');
		$PHPExcel->getActiveSheet()->setCellValue('D3', 'Perempuan');
		$PHPExcel->getActiveSheet()->setCellValue('E3', 'Total');
	
		$PHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			
		$PHPExcel->getActiveSheet()->getStyle('A3:E3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		$PHPExcel->getActiveSheet()->getStyle('A3:E3')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle('A3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    
		$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);		
		$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		
		$total_lk = 0; $total_pr = 0; $total_lk_pr = 0;
	
		
		$i = 1;
		$kolom = 4;
		
		$interval = 1; if(isset($_GET['interval'])) $interval = $_GET['interval'];
		
		$umur = 0;
		
		while($umur<=100)
		{
			$lk = Penduduk::model()->countByIntervalUmur($umur,$umur+$interval,'laki-laki'); 
			$pr = Penduduk::model()->countByIntervalUmur($umur,$umur+$interval,'perempuan');
			$lk_pr = $pr + $lk;
	
			$total_lk = $total_lk + $lk;
			$total_pr = $total_pr + $pr;
			$total_lk_pr = $total_lk_pr + $lk_pr;
			
			$umur_2 = $umur + $interval;
			
			$PHPExcel->getActiveSheet()->setCellValueExplicit('A'.$kolom, $i, PHPExcel_Cell_DataType::TYPE_STRING);
			$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $umur." - ".$umur_2." Tahun");
			$PHPExcel->getActiveSheet()->setCellValueExplicit('C'.$kolom, $lk, PHPExcel_Cell_DataType::TYPE_STRING);
			$PHPExcel->getActiveSheet()->setCellValueExplicit('D'.$kolom, $pr, PHPExcel_Cell_DataType::TYPE_STRING);
			$PHPExcel->getActiveSheet()->setCellValueExplicit('E'.$kolom, $lk_pr, PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    
			$i++; $kolom++; $umur = $umur + $interval;
		}
		
		$PHPExcel->getActiveSheet()->setCellValueExplicit('A'.$kolom, '', PHPExcel_Cell_DataType::TYPE_STRING);
		$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, 'Total');
		$PHPExcel->getActiveSheet()->setCellValueExplicit('C'.$kolom, $total_lk, PHPExcel_Cell_DataType::TYPE_STRING);
		$PHPExcel->getActiveSheet()->setCellValueExplicit('D'.$kolom, $total_pr, PHPExcel_Cell_DataType::TYPE_STRING);
		$PHPExcel->getActiveSheet()->setCellValueExplicit('E'.$kolom, $total_lk_pr, PHPExcel_Cell_DataType::TYPE_STRING);
			
		$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getFont()->setBold(true);
		
	
		$filename = time().'_LaporanPenduduk.xlsx';
	
		$path = Yii::app()->basePath.'/../uploads/export/';
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
		$objWriter->save($path.$filename);	
		$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}
	
	public function actionMutasiWord()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPWord',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));
		
		$PHPWord = new PHPWord();
		
		$PHPWord->addFontStyle('bold', array('bold'=>true));
		$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
		$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
		$tableStyle = array(
					'borderSize'=>1, 
					'borderColor'=>'000000', 
					'cellMargin'=>80,
					'border'=>true
		);
			
		$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
		$center = array('align'=>'center');
		
		$paraStyle = array('spaceAfter'=>'2','align'=>'center');
		$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
		$section = $PHPWord->createSection(array('marginLeft'=>500, 'marginRight'=>500, 'marginTop'=>500, 'marginBottom'=>500));
		
		$section->addText('MUTASI PENDUDUK',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
		$section->addTextBreak(1);		
		
		$table = $section->addTable('tableStyle');
		
		$table->addRow();
		$table->addCell(500)->addText("No",array('bold'=>'true'),$headStyle);
		$table->addCell(3000)->addText("Nama Lengkap / Panggilan",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Tempat Lahir",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Tanggal Lahir",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Jenis Kelamin",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Kewarganegaraan",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Datang Dari",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Tanggal",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Lahir",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Tanggal",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Pindah Ke",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Tanggal",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Mati",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Tanggal",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Keterangan",array('bold'=>'true'),$headStyle);
		
		
		$tanggal_awal = date('Y-m').'-01';
		$tanggal_akhir = date('Y-m').'-'.date('t');
			
		if(!empty($_GET['tanggal_awal']) AND !empty($_GET['tanggal_akhir']))
		{
			$tanggal_awal = $_GET['tanggal_awal'];
			$tanggal_akhir = $_GET['tanggal_akhir'];
		}
		
		$i=1;
		
		foreach(Penduduk::model()->getDataMutasi() as $data)
		{
			$table->addRow();
			$table->addCell(500)->addText($i,array(),$paraStyle);
			$table->addCell(3000)->addText($data->nama,array(),$paraStyle);
			$table->addCell(2000)->addText($data->tempat_lahir,array(),$paraStyle);
			$table->addCell(2000)->addText(Bantu::tanggalSingkat($data->tanggal_lahir),array(),$paraStyle);
			$table->addCell(2000)->addText($data->jenisKelamin->nama,array(),$paraStyle);
			$table->addCell(2000)->addText($data->kewarganegaraan->nama,array(),$paraStyle);
			
			if($data->mutasi_tambah_tanggal >= $tanggal_awal AND $data->mutasi_tambah_tanggal <= $tanggal_akhir) { 
	
				if($data->mutasi_tambah_id == 2)
				{				
					$table->addCell(2000)->addText($data->mutasi_tambah_keterangan,array(),$paraStyle);
					$table->addCell(2000)->addText(Bantu::tanggalSingkat($data->mutasi_tambah_tanggal),array(),$paraStyle);
				} else {
					$table->addCell(2000)->addText("",array(),$paraStyle);
					$table->addCell(2000)->addText("",array(),$paraStyle);
				}
				
				if($data->mutasi_tambah_id == 1)
				{				
					$table->addCell(2000)->addText("Lahir",array(),$paraStyle);
					$table->addCell(2000)->addText(Bantu::tanggalSingkat($data->mutasi_tambah_tanggal),array(),$paraStyle);
				} else {
					$table->addCell(2000)->addText("",array(),$paraStyle);
					$table->addCell(2000)->addText("",array(),$paraStyle);
				}
						
			} else {
			
				$table->addCell(2000)->addText("",array(),$paraStyle);
				$table->addCell(2000)->addText("",array(),$paraStyle);
				$table->addCell(2000)->addText("",array(),$paraStyle);
				$table->addCell(2000)->addText("",array(),$paraStyle);
			
			}
			
			if($data->mutasi_kurang_tanggal >= $tanggal_awal AND $data->mutasi_kurang_tanggal <= $tanggal_akhir) { 
	
				if($data->mutasi_kurang_id == 2)
				{				
					$table->addCell(2000)->addText($data->mutasi_kurang_keterangan,array(),$headStyle);
					$table->addCell(2000)->addText(Bantu::tanggalSingkat($data->mutasi_kurang_tanggal),array(),$headStyle);
				} else {
					$table->addCell(2000)->addText("",array(),$headStyle);
					$table->addCell(2000)->addText("",array(),$headStyle);
				}
				
				if($data->mutasi_kurang_id == 1)
				{				
					$table->addCell(2000)->addText("Mati",array(),$headStyle);
					$table->addCell(2000)->addText(Bantu::tanggalSingkat($data->mutasi_kurang_tanggal),array(),$headStyle);
				} else {
					$table->addCell(2000)->addText("",array(),$paraStyle);
					$table->addCell(2000)->addText("",array(),$paraStyle);
				}
						
			} else {
			
				$table->addCell(2000)->addText("",array(),$headStyle);
				$table->addCell(2000)->addText("",array(),$headStyle);
				$table->addCell(2000)->addText("",array(),$headStyle);
				$table->addCell(2000)->addText("",array(),$headStyle);
			
			}
			
			$table->addCell(2000)->addText("",array('bold'=>'true'),$headStyle);
			
			$i++;
		}
		
		
		$section->addTextBreak(1);		
		
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
		$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
		$filename = time().'_MutasiPenduduk.docx';
		
		$objWriter->save($pathFile.$filename);
		
		$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}
	
	public function actionMutasiExcel()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPExcel',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));

		$PHPExcel = new PHPExcel();
			
			
		$PHPExcel->getActiveSheet()->setCellValue('A1', 'MUTASI PENDUDUK');
		
		$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
		$PHPExcel->getActiveSheet()->setCellValue('B3', 'Nama Lengkap / Panggilan');
		$PHPExcel->getActiveSheet()->setCellValue('C3', 'Tempat Lahir');
		$PHPExcel->getActiveSheet()->setCellValue('D3', 'Tanggal Lahir');
		$PHPExcel->getActiveSheet()->setCellValue('E3', 'Jenis Kelamin');
		$PHPExcel->getActiveSheet()->setCellValue('F3', 'Kewarganegaraan');
		$PHPExcel->getActiveSheet()->setCellValue('G3', 'Datang Dari');
		$PHPExcel->getActiveSheet()->setCellValue('H3', 'Tanggal');
		$PHPExcel->getActiveSheet()->setCellValue('I3', 'Lahir');
		$PHPExcel->getActiveSheet()->setCellValue('J3', 'Tanggal');
		$PHPExcel->getActiveSheet()->setCellValue('K3', 'Pindah Ke');
		$PHPExcel->getActiveSheet()->setCellValue('L3', 'Tanggal');
		$PHPExcel->getActiveSheet()->setCellValue('M3', 'Mati');
		$PHPExcel->getActiveSheet()->setCellValue('N3', 'Tanggal');
		$PHPExcel->getActiveSheet()->setCellValue('O3', 'Keterangan');
	
		$PHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			
		$PHPExcel->getActiveSheet()->getStyle('A3:O3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		$PHPExcel->getActiveSheet()->getStyle('A3:O3')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle('A3:O3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    
		$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);		
		$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
		
		$tanggal_awal = date('Y-m').'-01';
		$tanggal_akhir = date('Y-m').'-'.date('t');
			
		if(!empty($_GET['tanggal_awal']) AND !empty($_GET['tanggal_akhir']))
		{
			$tanggal_awal = $_GET['tanggal_awal'];
			$tanggal_akhir = $_GET['tanggal_akhir'];
		}
		
		$i=1;
		$kolom = 4;
		
		foreach(Penduduk::model()->getDataMutasi() as $data)
		{
			$PHPExcel->getActiveSheet()->setCellValueExplicit('A'.$kolom, $i, PHPExcel_Cell_DataType::TYPE_STRING);
			$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nama);
			$PHPExcel->getActiveSheet()->setCellValueExplicit('C'.$kolom, $data->tempat_lahir);
			$PHPExcel->getActiveSheet()->setCellValueExplicit('D'.$kolom, Bantu::tanggalSingkat($data->tanggal_lahir));
			$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->jenisKelamin->nama);
			$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->kewarganegaraan->nama);
			
			if($data->mutasi_tambah_tanggal >= $tanggal_awal AND $data->mutasi_tambah_tanggal <= $tanggal_akhir)
			{
				if($data->mutasi_tambah_id==2) $PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->mutasi_tambah_keterangan);
				if($data->mutasi_tambah_id==2) $PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, Bantu::tanggalSingkat($data->mutasi_tambah_tanggal));
				if($data->mutasi_tambah_id==1) $PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, "Lahir");
				if($data->mutasi_tambah_id==1) $PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, Bantu::tanggalSingkat($data->mutasi_tambah_tanggal));
			
			}
			
			if($data->mutasi_kurang_tanggal >= $tanggal_awal AND $data->mutasi_kurang_tanggal <= $tanggal_akhir)
			{
				if($data->mutasi_kurang_id==2) $PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->mutasi_kurang_keterangan);
				if($data->mutasi_kurang_id==2) $PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, Bantu::tanggalSingkat($data->mutasi_kurang_tanggal));
				if($data->mutasi_kurang_id==1) $PHPExcel->getActiveSheet()->setCellValue('M'.$kolom, "Mati");
				if($data->mutasi_kurang_id==1) $PHPExcel->getActiveSheet()->setCellValue('N'.$kolom, Bantu::tanggalSingkat($data->mutasi_kurang_tanggal));
			
			}
			
			$PHPExcel->getActiveSheet()->setCellValue('O'.$kolom, "");
			
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':O'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':O'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    
			$kolom++; $i++;
		}
		
		
		$filename = time().'_MutasiPenduduk.xlsx';
	
		$path = Yii::app()->basePath.'/../uploads/export/';
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
		$objWriter->save($path.$filename);	
		$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	
	}
	
	public function actionReportWord()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPWord',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));
		
		$PHPWord = new PHPWord();
		
		$PHPWord->addFontStyle('bold', array('bold'=>true));
		$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
		$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
		$tableStyle = array(
					'borderSize'=>1, 
					'borderColor'=>'000000', 
					'cellMargin'=>80,
					'border'=>true
		);
			
		$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
		$center = array('align'=>'center');
		
		$paraStyle = array('spaceAfter'=>'2','align'=>'center');
		$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
		$section = $PHPWord->createSection(array('marginLeft'=>2000, 'marginRight'=>2000, 'marginTop'=>1000, 'marginBottom'=>1000));
		
		$section->addText('LAPORAN PENDUDUK',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
		$section->addTextBreak(1);		
		
		$table = $section->addTable('tableStyle');
		
		$table->addRow();
		$table->addCell(500)->addText("No",array('bold'=>'true'),$headStyle);
		$table->addCell(3000)->addText("Kategori",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Laki-laki",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Perempuan",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Total",array('bold'=>'true'),$headStyle);
		
		$i=1;
		
		$total_lk = 0; $total_pr = 0; $total_lk_pr = 0;
		
		$model = new Pendidikan;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='pendidikan')
			$model = new Pendidikan;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='agama')
			$model = new Agama;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='pekerjaan')
			$model = new Pekerjaan;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='pendidikan')
			$model = new Pendidikan;
			
		if(isset($_GET['kategori']) AND $_GET['kategori']=='kedudukanDalamKeluarga')
			$model = new KedudukanDalamKeluarga;
			
		if(isset($_GET['kategori']) AND $_GET['kategori']=='statusPerkawinan')
			$model = new StatusPerkawinan;	
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='kewarganegaraan')
			$model = new Kewarganegaraan;	
		
		
		foreach($model->findAll() as $data)
		{
			$lk = $data->countPenduduk('laki-laki'); 
			$pr = $data->countPenduduk('perempuan');
			$lk_pr = $pr + $lk;
	
			$total_lk = $total_lk + $lk;
			$total_pr = $total_pr + $pr;
			$total_lk_pr = $total_lk_pr + $lk_pr;
	
			$table->addRow();
			$table->addCell(100)->addText($i,array(),$paraStyle);
			$table->addCell(100)->addText($data->nama,array(),$paraStyle);
			$table->addCell(100)->addText($lk,array(),$paraStyle);
			$table->addCell(100)->addText($pr,array(),$paraStyle);
			$table->addCell(100)->addText($lk_pr,array(),$paraStyle);
			
			$i++;
		}		
		
		$table->addRow();
		$table->addCell(100)->addText('',array(),$paraStyle);
		$table->addCell(100)->addText('Total',array('bold'=>'true'),$headStyle);
		$table->addCell(100)->addText($total_lk,array('bold'=>'true'),$headStyle);
		$table->addCell(100)->addText($total_pr,array('bold'=>'true'),$headStyle);
		$table->addCell(100)->addText($total_lk_pr,array('bold'=>'true'),$headStyle);
		$section->addTextBreak(1);		
		
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
		$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
		$filename = time().'_LaporanPendududuk.docx';
		
		$objWriter->save($pathFile.$filename);
		
		$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}
	
	public function actionReportExcel()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPExcel',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));

		$PHPExcel = new PHPExcel();
			
			
		$PHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN PENDUDUK');
		
		$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
		$PHPExcel->getActiveSheet()->setCellValue('B3', 'Kategori');
		$PHPExcel->getActiveSheet()->setCellValue('C3', 'Laki-laki');
		$PHPExcel->getActiveSheet()->setCellValue('D3', 'Perempuan');
		$PHPExcel->getActiveSheet()->setCellValue('E3', 'Total');
	
		$PHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			
		$PHPExcel->getActiveSheet()->getStyle('A3:E3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		$PHPExcel->getActiveSheet()->getStyle('A3:E3')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle('A3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    
		$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);		
		$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		
		$total_lk = 0; $total_pr = 0; $total_lk_pr = 0;
		
		$model = new Pendidikan;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='pendidikan')
			$model = new Pendidikan;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='agama')
			$model = new Agama;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='pekerjaan')
			$model = new Pekerjaan;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='pendidikan')
			$model = new Pendidikan;
			
		if(isset($_GET['kategori']) AND $_GET['kategori']=='kedudukanDalamKeluarga')
			$model = new KedudukanDalamKeluarga;
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='statusPerkawinan')
			$model = new StatusPerkawinan;	
		
		if(isset($_GET['kategori']) AND $_GET['kategori']=='kewarganegaraan')
			$model = new Kewarganegaraan;	
		
		$i = 1;
		$kolom = 4;
		
		foreach($model->findAll() as $data)
		{
			$lk = $data->countPenduduk('laki-laki'); 
			$pr = $data->countPenduduk('perempuan');
			$lk_pr = $pr + $lk;
	
			$total_lk = $total_lk + $lk;
			$total_pr = $total_pr + $pr;
			$total_lk_pr = $total_lk_pr + $lk_pr;
			
			$PHPExcel->getActiveSheet()->setCellValueExplicit('A'.$kolom, $i, PHPExcel_Cell_DataType::TYPE_STRING);
			$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nama);
			$PHPExcel->getActiveSheet()->setCellValueExplicit('C'.$kolom, $lk, PHPExcel_Cell_DataType::TYPE_STRING);
			$PHPExcel->getActiveSheet()->setCellValueExplicit('D'.$kolom, $pr, PHPExcel_Cell_DataType::TYPE_STRING);
			$PHPExcel->getActiveSheet()->setCellValueExplicit('E'.$kolom, $lk_pr, PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    
			$i++; $kolom++;
		}
		
		$PHPExcel->getActiveSheet()->setCellValueExplicit('A'.$kolom, '', PHPExcel_Cell_DataType::TYPE_STRING);
		$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, 'Total');
		$PHPExcel->getActiveSheet()->setCellValueExplicit('C'.$kolom, $total_lk, PHPExcel_Cell_DataType::TYPE_STRING);
		$PHPExcel->getActiveSheet()->setCellValueExplicit('D'.$kolom, $total_pr, PHPExcel_Cell_DataType::TYPE_STRING);
		$PHPExcel->getActiveSheet()->setCellValueExplicit('E'.$kolom, $total_lk_pr, PHPExcel_Cell_DataType::TYPE_STRING);
			
		$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getFont()->setBold(true);
		
	
		$filename = time().'_LaporanPenduduk.xlsx';
	
		$path = Yii::app()->basePath.'/../uploads/export/';
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
		$objWriter->save($path.$filename);	
		$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}
	
	public function actionReportAgama()
	{
		$this->render('reportAgama');
	}
	
	public function actionReportPekerjaan()
	{
		$this->render('reportPekerjaan');
	}
	
	public function actionReportKepalaKeluarga()
	{
		$this->render('reportKepalaKeluarga');
	}
	
	public function actionReportStatusPerkawinan()
	{
		$this->render('reportStatusPerkawinan');
	}
	
	public function actionReportKewarganegaraan()
	{
		$this->render('reportKewarganegaraan');
	}
	
	public function actionReportKedudukanDalamKeluarga()
	{
		$this->render('reportKedudukanDalamKeluarga');
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else	
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Penduduk');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Penduduk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Penduduk']))
			$model->attributes=$_GET['Penduduk'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionUsiaBalita()
	{
		$model=new Penduduk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Penduduk']))
			$model->attributes=$_GET['Penduduk'];

		$this->render('usiaBalita',array(
			'model'=>$model,
		));
	}
	
	public function actionWajibBelajar()
	{
		$model=new Penduduk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Penduduk']))
			$model->attributes=$_GET['Penduduk'];

		$this->render('wajibBelajar',array(
			'model'=>$model,
		));
	}
	
	public function actionPemilihPemilu()
	{
		$model=new Penduduk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Penduduk']))
			$model->attributes=$_GET['Penduduk'];

		$this->render('pemilihPemilu',array(
			'model'=>$model,
		));
	}
	
	public function actionPendidikan()
	{
		$model=new Penduduk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Penduduk']))
			$model->attributes=$_GET['Penduduk'];

		$this->render('pendidikan',array(
			'model'=>$model,
		));
	}
	
	public function actionPekerjaan()
	{
		$model=new Penduduk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Penduduk']))
			$model->attributes=$_GET['Penduduk'];

		$this->render('pekerjaan',array(
			'model'=>$model,
		));
	}
	
	public function actionAgama()
	{
		$model=new Penduduk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Penduduk']))
			$model->attributes=$_GET['Penduduk'];

		$this->render('agama',array(
			'model'=>$model,
		));
	}
	
	public function actionExportWord()
	{
		if(isset($_POST['proses']) AND $_POST['proses']==1) 
		{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPWord',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));
		
			$PHPWord = new PHPWord();
		
			$PHPWord->addFontStyle('bold', array('bold'=>true));
			$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
			$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
			$tableStyle = array(
						'borderSize'=>1, 
						'borderColor'=>'000000', 
						'cellMargin'=>80,
						'border'=>true
			);
			
			$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
			$center = array('align'=>'center');
		
			$paraStyle = array('spaceAfter'=>'2');
			$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
			$section = $PHPWord->createSection(array('orientation'=>'landscape','marginLeft'=>500, 'marginRight'=>500, 'marginTop'=>500, 'marginBottom'=>500));
		
			$section->addText('DATA PENDUDUK',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
			$section->addTextBreak(1);		
		
			$table = $section->addTable('tableStyle');
		
			$table->addRow();
			$table->addCell(500)->addText("No",array('bold'=>'true'),$headStyle);
			$table->addCell(5000)->addText("Nama Lengkap / Panggilan",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Jenis Kelamin",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Status Pernikahan",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Tempat Lahir",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Tanggal Lahir",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Agama",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Pendidikan Terakhir",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Pekerjaan",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Dapat Membaca Huruf",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Kewarganegaraan",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Alamat",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Dusun",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("RW",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("RT",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Kedudukan Dalam Keluarga",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Nomor KTP",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Nomor KSK",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText("Ket",array('bold'=>'true'),$headStyle);
			
			$i=1;
		
			foreach(Penduduk::model()->getDataPenduduk() as $data)
			{
				$table->addRow();
				$table->addCell(100)->addText($i,array(),$paraStyle);
				$table->addCell(100)->addText($data->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->jenisKelamin->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->statusPerkawinan->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->tempat_lahir,array(),$paraStyle);
				$table->addCell(100)->addText(Bantu::tanggalSingkat($data->tanggal_lahir),array(),$paraStyle);
				$table->addCell(100)->addText($data->agama->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->pendidikan->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->pekerjaan->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->dapat_membaca,array(),$paraStyle);
				$table->addCell(100)->addText($data->kewarganegaraan->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->alamat,array(),$paraStyle);
				$table->addCell(100)->addText($data->dusun->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->rw,array(),$paraStyle);
				$table->addCell(100)->addText($data->rt,array(),$paraStyle);
				$table->addCell(100)->addText($data->kedudukanDalamKeluarga->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->nomor_ktp,array(),$paraStyle);
				$table->addCell(100)->addText($data->nomor_ksk,array(),$paraStyle);
				$table->addCell(100)->addText($data->keterangan,array(),$paraStyle);
				$i++;
			}		
	
			$section->addTextBreak(1);		
		
			$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
			$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
			$filename = time().'_penduduk.docx';
		
			$objWriter->save($pathFile.$filename);
		
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
		
		} else {
			
			$this->render('exportWord');
		}
		
	}
	
	public function actionExportExcel()
	{
		if(isset($_POST['proses']) AND $_POST['proses']==1)
		{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$PHPExcel = new PHPExcel();
			
			
			$PHPExcel->getActiveSheet()->setCellValue('A1', 'DATA PENDUDUK');
		
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
			$PHPExcel->getActiveSheet()->setCellValue('B3', 'Nama Lengkap / Panggilan');
			$PHPExcel->getActiveSheet()->setCellValue('C3', 'Jenis Kelamin');
			$PHPExcel->getActiveSheet()->setCellValue('D3', 'Status Perkawinan');
			$PHPExcel->getActiveSheet()->setCellValue('E3', 'Tempat Lahir');
			$PHPExcel->getActiveSheet()->setCellValue('F3', 'Tanggal Lahir');
			$PHPExcel->getActiveSheet()->setCellValue('G3', 'Agama');
			$PHPExcel->getActiveSheet()->setCellValue('H3', 'Pendidikan Terakhir');
			$PHPExcel->getActiveSheet()->setCellValue('I3', 'Pekerjaan');
			$PHPExcel->getActiveSheet()->setCellValue('J3', 'Dapat Membaca Huruf');
			$PHPExcel->getActiveSheet()->setCellValue('K3', 'Kewarganegaraan');
			$PHPExcel->getActiveSheet()->setCellValue('L3', 'Alamat');
			$PHPExcel->getActiveSheet()->setCellValue('M3', 'Dusun');
			$PHPExcel->getActiveSheet()->setCellValue('N3', 'RW');
			$PHPExcel->getActiveSheet()->setCellValue('O3', 'RT');
			$PHPExcel->getActiveSheet()->setCellValue('P3', 'Kedudukan Dalam Keluarga');
			$PHPExcel->getActiveSheet()->setCellValue('Q3', 'Nomor KTP');
			$PHPExcel->getActiveSheet()->setCellValue('R3', 'Nomor KSK');
			
			$PHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			
			$PHPExcel->getActiveSheet()->getStyle('A3:R3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
			$PHPExcel->getActiveSheet()->getStyle('A3:R3')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getStyle('A3:R3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    
			$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
			$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);		
			$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
			$PHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
			$PHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
			
			$i = 1;
			$kolom = 4;

			foreach(Penduduk::model()->getDataPenduduk() as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nama);
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->jenisKelamin->nama);
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->statusPerkawinan->nama);
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->tempat_lahir);
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, Bantu::tanggalSingkat($data->tanggal_lahir));
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->agama->nama);
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->pendidikan->nama);
				$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->pekerjaan->nama);
				$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->dapat_membaca);
				$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->kewarganegaraan->nama);
				$PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, $data->alamat);
				$PHPExcel->getActiveSheet()->setCellValue('M'.$kolom, $data->dusun->nama);
				$PHPExcel->getActiveSheet()->setCellValueExplicit('N'.$kolom, $data->rw, PHPExcel_Cell_DataType::TYPE_STRING);
				$PHPExcel->getActiveSheet()->setCellValueExplicit('O'.$kolom, $data->rt, PHPExcel_Cell_DataType::TYPE_STRING);
				$PHPExcel->getActiveSheet()->setCellValue('P'.$kolom, $data->kedudukanDalamKeluarga->nama);
				$PHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$kolom, $data->nomor_ktp, PHPExcel_Cell_DataType::TYPE_STRING);
				$PHPExcel->getActiveSheet()->setCellValueExplicit('R'.$kolom, $data->nomor_ksk, PHPExcel_Cell_DataType::TYPE_STRING);
		
				$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':R'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				

				
				$i++; $kolom++;
			}
	
			$filename = time().'_penduduk.xlsx';

			
			
			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
			
		} else {
			
			$this->render('exportExcel');
		
		}
	
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=Penduduk::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='penduduk-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
