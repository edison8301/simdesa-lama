<?php

class RencanaPembangunanController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','export','exportAll','print','exportExcel','exportWord'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public $menu = array(
			array('label'=>'Rencana Pembangunan','url'=>array('admin'),'icon'=>'list'),
			array('label'=>'Tambah','url'=>array('create'),'icon'=>'plus'),
			array('label'=>'Export Word','url'=>array('exportWord'),'icon'=>'download-alt'),
			array('label'=>'Export Excel','url'=>array('exportExcel'),'icon'=>'download-alt'),
	);

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExport($id)
	{
		$this->layout = '//layouts/excel';
		
		$model = $this->loadModel($id);
		
		$this->render('export',array(
			'model'=>$model,
		));
	}
	
	public function actionExportAll()
	{
		if(isset($_POST['proses']) AND $_POST['proses']=='1')
				$this->layout = '//layouts/excel';
			
		$this->render('export_all');
	}
	
	public function actionPrintAll()
	{
		if(isset($_POST['proses']) AND $_POST['proses']=='1')
				$this->layout = '//layouts/print';
			
		$this->render('print_all');
	}
	
	public function actionPrint($id)
	{
		$this->layout = 'print';
		
		$model = $this->loadModel($id);
		
		$this->render('print',array(
			'model'=>$model,
		));
	}
	

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new RencanaPembangunan;
		
		$model->tahun = date('Y');
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
			
		if(isset($_POST['RencanaPembangunan']))
		{
			$model->attributes=$_POST['RencanaPembangunan'];
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RencanaPembangunan']))
		{
			$model->attributes=$_POST['RencanaPembangunan'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else	
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('RencanaPembangunan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new RencanaPembangunan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RencanaPembangunan']))
			$model->attributes=$_GET['RencanaPembangunan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionExportWord()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPWord',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));
		
		$PHPWord = new PHPWord();
		
		$PHPWord->addFontStyle('bold', array('bold'=>true));
		$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
		$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
		$tableStyle = array(
						'borderSize'=>1, 
						'borderColor'=>'000000', 
						'cellMargin'=>80,
						'border'=>true
					);
		
		$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
		$center = array('align'=>'center');
		
		$paraStyle = array('spaceAfter'=>'2');
		$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
		$section = $PHPWord->createSection(array('orientation'=>'landscape','marginLeft'=>750, 'marginRight'=>750, 'marginTop'=>750, 'marginBottom'=>750));
		
		$section->addText('RENCANA PEMBANGUNAN',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
		$section->addTextBreak(1);		
		
		$table = $section->addTable('tableStyle');
		
		$table->addRow();
		$table->addCell(500)->addText("No",array('bold'=>'true'),$headStyle);
		$table->addCell(4000)->addText("Nama Proyek / Kegiatan",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Lokasi",array('bold'=>'true'),$headStyle);
		$table->addCell(1000)->addText("Pemerintah",array('bold'=>'true'),$headStyle);
		$table->addCell(1000)->addText("Provinsi",array('bold'=>'true'),$headStyle);
		$table->addCell(1000)->addText("Kab/Kota",array('bold'=>'true'),$headStyle);
		$table->addCell(1000)->addText("Swadaya",array('bold'=>'true'),$headStyle);
		$table->addCell(1000)->addText("Jumlah",array('bold'=>'true'),$headStyle);
		$table->addCell(1000)->addText("Pelaksana",array('bold'=>'true'),$headStyle);
		$table->addCell(1000)->addText("Manfaat",array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText("Keterangan",array('bold'=>'true'),$headStyle);
		
		$i=1;
		
		foreach(RencanaPembangunan::model()->findAll() as $data)
		{
			$table->addRow();
			$table->addCell(100)->addText($i,array(),$paraStyle);
			$table->addCell(100)->addText($data->nama_proyek,array(),$paraStyle);
			$table->addCell(100)->addText($data->lokasi,array(),$paraStyle);
			$table->addCell(100)->addText($data->dana_pemerintah,array(),$paraStyle);
			$table->addCell(100)->addText($data->dana_provinsi,array(),$paraStyle);
			$table->addCell(100)->addText($data->dana_kabkota,array(),$paraStyle);
			$table->addCell(100)->addText($data->dana_swadaya,array(),$paraStyle);
			$table->addCell(100)->addText($data->jumlah,array(),$paraStyle);
			$table->addCell(100)->addText($data->pelaksana,array(),$paraStyle);
			$table->addCell(100)->addText($data->manfaat,array(),$paraStyle);
			$table->addCell(100)->addText($data->keterangan,array(),$paraStyle);
			$i++;
		}		
	
		$section->addTextBreak(1);		
		
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
		$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
		$filename = time().'_rencana_pembangunan.docx';
		
		$objWriter->save($pathFile.$filename);
		
		$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
		
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=RencanaPembangunan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='rencana-pembangunan-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
