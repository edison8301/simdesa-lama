<?php

class SuratController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','export','exportAll','docx','pilihPenduduk'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public $menu = array(
		array('label'=>'Surat','url'=>array('admin'),'icon'=>'list'),
		array('label'=>'Tambah','url'=>array('create'),'icon'=>'plus'),
	);
/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		
		if(!empty($_POST['cetak']))
		{
			if(!empty($_POST['SuratAtribut']))
				$model->saveSuratAtribut($_POST['SuratAtribut']);
			
			$this->redirect(array('surat/docx','id'=>$id));
		}
		
		$this->render('view',array(
			'model'=>$model,
		));
	}
	
	public function actionPilihPenduduk()
	{
		$model=new Penduduk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Penduduk']))
			$model->attributes=$_GET['Penduduk'];

		$this->render('pilihPenduduk',array(
			'model'=>$model,
		));
	}
	
	public function actionDocx($id)
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPWord',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));
		
		$model = $this->loadModel($id);
		$penduduk = Penduduk::model()->findByPk($model->penduduk_id);
		
		$PHPWord = new PHPWord();
		
		$pathTemplate = Yii::app()->basePath.'/../uploads/suratTemplate/';
		
		$document = $PHPWord->loadTemplate($pathTemplate.$model->surat_template->file);
		
		$document->setValue('nama_desa',Pengaturan::model()->findByPk(1)->nilai);
		$document->setValue('NAMA_DESA',strtoupper(Pengaturan::model()->findByPk(1)->nilai));
		
		$document->setValue('alamat_desa',Pengaturan::model()->findByPk(2)->nilai);
		$document->setValue('ALAMAT_DESA',strtoupper(Pengaturan::model()->findByPk(2)->nilai));
		
		$document->setValue('kecamatan',Pengaturan::model()->findByPk(3)->nilai);
		$document->setValue('KECAMATAN',strtoupper(Pengaturan::model()->findByPk(3)->nilai));
		
		$document->setValue('telepon',Pengaturan::model()->findByPk(4)->nilai);
		$document->setValue('TELEPON',strtoupper(Pengaturan::model()->findByPk(4)->nilai));
		
		$document->setValue('website',Pengaturan::model()->findByPk(5)->nilai);
		$document->setValue('WEBSITE',strtoupper(Pengaturan::model()->findByPk(5)->nilai));
		
		$document->setValue('email',Pengaturan::model()->findByPk(6)->nilai);
		$document->setValue('EMAIL',strtoupper(Pengaturan::model()->findByPk(6)->nilai));
		
		$document->setValue('kepala_desa',Pengaturan::model()->findByPk(7)->nilai);
		$document->setValue('KEPALA_DESA',strtoupper(Pengaturan::model()->findByPk(7)->nilai));
		
		$document->setValue('nip_kepala_desa',Pengaturan::model()->findByPk(8)->nilai);
		$document->setValue('NIP_KEPALA_DESA',strtoupper(Pengaturan::model()->findByPk(8)->nilai));
		
		$document->setValue('sekretaris',Pengaturan::model()->findByPk(9)->nilai);
		$document->setValue('SEKRETARIS',strtoupper(Pengaturan::model()->findByPk(9)->nilai));
		
		$document->setValue('nip_sekretaris',Pengaturan::model()->findByPk(10)->nilai);
		$document->setValue('NIP_SEKRETARIS',strtoupper(Pengaturan::model()->findByPk(10)->nilai));
		
		$document->setValue('nomor',$model->nomor);
		$document->setValue('nama',$penduduk->nama);
		$document->setValue('jenis_kelamin',$penduduk->jenisKelamin->nama);
		$document->setValue('status_perkawinan',$penduduk->statusPerkawinan->nama);
		$document->setValue('tempat_lahir',$penduduk->tempat_lahir);
		$document->setValue('tanggal_lahir',Bantu::tanggalSingkat($penduduk->tanggal_lahir));
		$document->setValue('agama',$penduduk->agama->nama);
		$document->setValue('pendidikan',$penduduk->pendidikan->nama);
		$document->setValue('pekerjaan',$penduduk->pekerjaan->nama);
		$document->setValue('dapat_membaca',$penduduk->dapat_membaca);
		$document->setValue('kewarganegaraan',$penduduk->kewarganegaraan->nama);
		$document->setValue('alamat',$penduduk->alamat);
		$document->setValue('kedudukan_dalam_keluarga',$penduduk->nama);
		$document->setValue('nomor_ktp',$penduduk->nomor_ktp);
		$document->setValue('nomor_ksk',$penduduk->nomor_ksk);
		
		$document->setValue('tanggal',Bantu::tanggalSingkat(date('Y-m-d')));
		
		foreach($model->getDataSuratAtribut() as $data)
		{
			$document->setValue($data->key, $data->value);
		}		
		
		$pathFile = Yii::app()->basePath.'/../exports/';
		$filename = time().'_'.'surat.docx';
		
		$document->save($pathFile.$filename);
		
		$urlFile = Yii::app()->request->baseUrl.'/exports/';
		
		$this->redirect($urlFile.$filename);
	}
	
	public function actionExport($id)
	{
		$this->render('export',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExportAll()
	{
		$this->render('export_all',array(
			'model'=>$this->loadModel($id),
		));
	}
	

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Surat;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
			
		if(isset($_POST['Surat']))
		{
			$model->attributes=$_POST['Surat'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Surat']))
		{
			$model->attributes=$_POST['Surat'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else	
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Surat');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Surat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Surat']))
			$model->attributes=$_GET['Surat'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=Surat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='surat-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
