<?php

class TanahController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','export','exportAll','exportWord','exportExcel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public $menu = array(
			array('label'=>'Tanah di Desa','url'=>array('admin'),'icon'=>'list'),
			array('label'=>'Tambah','url'=>array('create'),'icon'=>'plus'),
			array('label'=>'Export Word','url'=>array('exportWord'),'icon'=>'download-alt'),
			array('label'=>'Export Excel','url'=>array('exportExcel'),'icon'=>'download-alt'),
	);

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExport($id)
	{
		$this->render('export',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExportAll()
	{
		$this->render('export_all',array(
			'model'=>$this->loadModel($id),
		));
	}
	

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	
	public function actionCreate()
	{
		$model=new Tanah;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
			
		if(isset($_POST['Tanah']))
		{
			$model->attributes=$_POST['Tanah'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tanah']))
		{
			$model->attributes=$_POST['Tanah'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else	
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Tanah');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Tanah('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tanah']))
			$model->attributes=$_GET['Tanah'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionExportWord()
	{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPWord',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));
		
			$PHPWord = new PHPWord();
		
			$PHPWord->addFontStyle('bold', array('bold'=>true));
			$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
			$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
			$tableStyle = array(
						'borderSize'=>1, 
						'borderColor'=>'000000', 
						'cellMargin'=>80,
						'border'=>true
			);
			
			$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
			$center = array('align'=>'center');
		
			$paraStyle = array('spaceAfter'=>'2');
			$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
			$section = $PHPWord->createSection(array('orientation'=>'landscape','marginLeft'=>500, 'marginRight'=>500, 'marginTop'=>500, 'marginBottom'=>500));
		
			$section->addText('DATA Tanah',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
			$section->addTextBreak(1);		
		
			$table = $section->addTable('tableStyle');
		
			$table->addRow();
			
			$table->addCell(1000)->addText("No",array('bold'=>'true'),$headStyle);
			
			$table->addCell(1000)->addText('Nama',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Jumlah',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('HM',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('HGB',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('HP',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('HGU',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('HPL',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('MA',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('VI',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('TN',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Perumahan',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Perdagangan',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Perkantoran',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Industri',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Fasilitas_umum',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Sawah',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Tegalan',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Perkebunan',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Peternakan',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Hutan Belukar',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Hutan Lindung',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Tanah Kosong',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Lain-lain',array('bold'=>'true'),$headStyle);
						
			$i=1;
		
			foreach(Tanah::model()->findAll() as $data)
			{
				$table->addRow();
				$table->addCell(100)->addText($data->nama,array(),$paraStyle);
				$table->addCell(100)->addText($data->jumlah,array(),$paraStyle);
				$table->addCell(100)->addText($data->hm,array(),$paraStyle);
				$table->addCell(100)->addText($data->hgb,array(),$paraStyle);
				$table->addCell(100)->addText($data->hp,array(),$paraStyle);
				$table->addCell(100)->addText($data->hgu,array(),$paraStyle);
				$table->addCell(100)->addText($data->hpl,array(),$paraStyle);
				$table->addCell(100)->addText($data->ma,array(),$paraStyle);
				$table->addCell(100)->addText($data->vi,array(),$paraStyle);
				$table->addCell(100)->addText($data->tn,array(),$paraStyle);
				$table->addCell(100)->addText($data->perumahan,array(),$paraStyle);
				$table->addCell(100)->addText($data->perdagangan,array(),$paraStyle);
				$table->addCell(100)->addText($data->perkantoran,array(),$paraStyle);
				$table->addCell(100)->addText($data->industri,array(),$paraStyle);
				$table->addCell(100)->addText($data->fasilitas_umum,array(),$paraStyle);
				$table->addCell(100)->addText($data->sawah,array(),$paraStyle);
				$table->addCell(100)->addText($data->tegalan,array(),$paraStyle);
				$table->addCell(100)->addText($data->perkebunan,array(),$paraStyle);
				$table->addCell(100)->addText($data->peternakan,array(),$paraStyle);
				$table->addCell(100)->addText($data->hutan_belukar,array(),$paraStyle);
				$table->addCell(100)->addText($data->hutan_lindung,array(),$paraStyle);
				$table->addCell(100)->addText($data->tanah_kosong,array(),$paraStyle);
				$table->addCell(100)->addText($data->lain_lain,array(),$paraStyle);
								
				$i++;
			}		
	
			$section->addTextBreak(1);		
		
			$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
			$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
			$filename = time().'_Tanah.docx';
		
			$objWriter->save($pathFile.$filename);
		
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
		
	
	}
	
	public function actionExportExcel()
	{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$PHPExcel = new PHPExcel();
			
			$PHPExcel->getActiveSheet()->setCellValue('A1', 'DATA TANAH');
		
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
			
			$PHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
			$PHPExcel->getActiveSheet()->setCellValue('C3', 'Jumlah');
			$PHPExcel->getActiveSheet()->setCellValue('D3', 'HM');
			$PHPExcel->getActiveSheet()->setCellValue('E3', 'HGB');
			$PHPExcel->getActiveSheet()->setCellValue('F3', 'HP');
			$PHPExcel->getActiveSheet()->setCellValue('G3', 'HGU');
			$PHPExcel->getActiveSheet()->setCellValue('H3', 'HPL');
			$PHPExcel->getActiveSheet()->setCellValue('I3', 'MA');
			$PHPExcel->getActiveSheet()->setCellValue('J3', 'VI');
			$PHPExcel->getActiveSheet()->setCellValue('K3', 'TN');
			$PHPExcel->getActiveSheet()->setCellValue('L3', 'Perumahan');
			$PHPExcel->getActiveSheet()->setCellValue('M3', 'Perdagangan');
			$PHPExcel->getActiveSheet()->setCellValue('N3', 'Perkantoran');
			$PHPExcel->getActiveSheet()->setCellValue('O3', 'Industri');
			$PHPExcel->getActiveSheet()->setCellValue('P3', 'Fasilitas Umum');
			$PHPExcel->getActiveSheet()->setCellValue('Q3', 'Sawah');
			$PHPExcel->getActiveSheet()->setCellValue('R3', 'Tegalan');
			$PHPExcel->getActiveSheet()->setCellValue('S3', 'Perkebunan');
			$PHPExcel->getActiveSheet()->setCellValue('T3', 'Peternakan');
			$PHPExcel->getActiveSheet()->setCellValue('U3', 'Hutan Belukar');
			$PHPExcel->getActiveSheet()->setCellValue('V3', 'Hutan Lindung');
			$PHPExcel->getActiveSheet()->setCellValue('W3', 'Tanah Kosong');
			$PHPExcel->getActiveSheet()->setCellValue('X3', 'Lain-lain');
						
			$i = 1;
			$kolom = 4;

			foreach(Tanah::model()->findAll() as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nama);
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->jumlah);
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->hm);
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->hgb);
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->hp);
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->hgu);
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->hpl);
				$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->ma);
				$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->vi);
				$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->tn);
				$PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, $data->perumahan);
				$PHPExcel->getActiveSheet()->setCellValue('M'.$kolom, $data->perdagangan);
				$PHPExcel->getActiveSheet()->setCellValue('N'.$kolom, $data->perkantoran);
				$PHPExcel->getActiveSheet()->setCellValue('O'.$kolom, $data->industri);
				$PHPExcel->getActiveSheet()->setCellValue('P'.$kolom, $data->fasilitas_umum);
				$PHPExcel->getActiveSheet()->setCellValue('Q'.$kolom, $data->sawah);
				$PHPExcel->getActiveSheet()->setCellValue('R'.$kolom, $data->tegalan);
				$PHPExcel->getActiveSheet()->setCellValue('S'.$kolom, $data->perkebunan);
				$PHPExcel->getActiveSheet()->setCellValue('T'.$kolom, $data->peternakan);
				$PHPExcel->getActiveSheet()->setCellValue('U'.$kolom, $data->hutan_belukar);
				$PHPExcel->getActiveSheet()->setCellValue('V'.$kolom, $data->hutan_lindung);
				$PHPExcel->getActiveSheet()->setCellValue('W'.$kolom, $data->tanah_kosong);
				$PHPExcel->getActiveSheet()->setCellValue('X'.$kolom, $data->lain_lain);
									
				$i++; $kolom++;
			}
	
			$filename = time().'_Tanah.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=Tanah::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tanah-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
