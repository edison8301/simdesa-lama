<?php

class TanahMilikController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','export','exportAll','exportWord','exportExcel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public $menu = array(
			array('label'=>'Tanah Kas Desa','url'=>array('admin'),'icon'=>'list'),
			array('label'=>'Tambah','url'=>array('create'),'icon'=>'plus'),
			array('label'=>'Export Word','url'=>array('exportWord'),'icon'=>'download-alt'),
			array('label'=>'Export Excel','url'=>array('exportExcel'),'icon'=>'download-alt'),
	);

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExport($id)
	{
		$this->render('export',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExportAll()
	{
		$this->render('export_all',array(
			'model'=>$this->loadModel($id),
		));
	}
	

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	
	public function actionCreate()
	{
		$model=new TanahMilik;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
			
		if(isset($_POST['TanahMilik']))
		{
			$model->attributes=$_POST['TanahMilik'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TanahMilik']))
		{
			$model->attributes=$_POST['TanahMilik'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else	
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TanahMilik');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new TanahMilik('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TanahMilik']))
			$model->attributes=$_GET['TanahMilik'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionExportWord()
	{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPWord',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));
		
			$PHPWord = new PHPWord();
		
			$PHPWord->addFontStyle('bold', array('bold'=>true));
			$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
			$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
			$tableStyle = array(
						'borderSize'=>1, 
						'borderColor'=>'000000', 
						'cellMargin'=>80,
						'border'=>true
			);
			
			$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
			$center = array('align'=>'center');
		
			$paraStyle = array('spaceAfter'=>'2');
			$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
			$section = $PHPWord->createSection(array('orientation'=>'landscape','marginLeft'=>500, 'marginRight'=>500, 'marginTop'=>500, 'marginBottom'=>500));
		
			$section->addText('DATA TanahMilik',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
			$section->addTextBreak(1);		
		
			$table = $section->addTable('tableStyle');
		
			$table->addRow();
			
			$table->addCell(1000)->addText("No",array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Asal Tanah Milik Desa / Tanah Kas Desa',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Nomor Sertifikat Buku Letter C / Persil',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Luas (Ha)',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Klas',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Asli',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Bantuan Pemerintah',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Bantuan Provinsi',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Bantuan Kab/Kota',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Lain-lain',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Tanggal Perolehan',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Sawah',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Tegal',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Kebun',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Tambak',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Tanah Kering',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Patok Ada',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Patok Tidak Ada',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Papan Ada',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Papan Tidak Ada',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Lokasi',array('bold'=>'true'),$headStyle);
			$table->addCell(1000)->addText('Peruntukkan',array('bold'=>'true'),$headStyle);
						
			$i=1;
		
			foreach(TanahMilik::model()->findAll() as $data)
			{
				$table->addRow();
				$table->addCell(100)->addText($i,array(),$paraStyle);
				$table->addCell(100)->addText($data->asal_tanah,array(),$paraStyle);
				$table->addCell(100)->addText($data->nomor_sertifikat,array(),$paraStyle);
				$table->addCell(100)->addText($data->luas,array(),$paraStyle);
				$table->addCell(100)->addText($data->klas,array(),$paraStyle);
				$table->addCell(100)->addText($data->asli,array(),$paraStyle);
				$table->addCell(100)->addText($data->bantuan_pemerintah,array(),$paraStyle);
				$table->addCell(100)->addText($data->bantuan_provinsi,array(),$paraStyle);
				$table->addCell(100)->addText($data->bantuan_kabkota,array(),$paraStyle);
				$table->addCell(100)->addText($data->lain_lain,array(),$paraStyle);
				$table->addCell(100)->addText($data->tanggal_perolehan,array(),$paraStyle);
				$table->addCell(100)->addText($data->sawah,array(),$paraStyle);
				$table->addCell(100)->addText($data->tegal,array(),$paraStyle);
				$table->addCell(100)->addText($data->kebun,array(),$paraStyle);
				$table->addCell(100)->addText($data->tambak,array(),$paraStyle);
				$table->addCell(100)->addText($data->tanah_kering,array(),$paraStyle);
				$table->addCell(100)->addText($data->patok_ada,array(),$paraStyle);
				$table->addCell(100)->addText($data->patok_tidak_ada,array(),$paraStyle);
				$table->addCell(100)->addText($data->papan_ada,array(),$paraStyle);
				$table->addCell(100)->addText($data->papan_tidak_ada,array(),$paraStyle);
				$table->addCell(100)->addText($data->lokasi,array(),$paraStyle);
				$table->addCell(100)->addText($data->peruntukkan,array(),$paraStyle);
								
				$i++;
			}		
	
			$section->addTextBreak(1);		
		
			$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
			$pathFile = Yii::app()->basePath.'/../uploads/export/';
		
			$filename = time().'_TanahMilik.docx';
		
			$objWriter->save($pathFile.$filename);
		
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
		
	
	}
	
	public function actionExportExcel()
	{
			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$PHPExcel = new PHPExcel();
			
			$PHPExcel->getActiveSheet()->setCellValue('A1', 'DATA TanahMilik');
		
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
			$PHPExcel->getActiveSheet()->setCellValue('B3', 'Asal Tanah');
			$PHPExcel->getActiveSheet()->setCellValue('C3', 'Nomor Sertifikat');
			$PHPExcel->getActiveSheet()->setCellValue('D3', 'Luas');
			$PHPExcel->getActiveSheet()->setCellValue('E3', 'Klas');
			$PHPExcel->getActiveSheet()->setCellValue('F3', 'Asli');
			$PHPExcel->getActiveSheet()->setCellValue('G3', 'Bantuan Pemerintah');
			$PHPExcel->getActiveSheet()->setCellValue('H3', 'Bantuan Provinsi');
			$PHPExcel->getActiveSheet()->setCellValue('I3', 'Bantuan_Kab/Kota');
			$PHPExcel->getActiveSheet()->setCellValue('J3', 'Lain-lain');
			$PHPExcel->getActiveSheet()->setCellValue('K3', 'Tanggal Perolehan');
			$PHPExcel->getActiveSheet()->setCellValue('L3', 'Sawah');
			$PHPExcel->getActiveSheet()->setCellValue('M3', 'Tegal');
			$PHPExcel->getActiveSheet()->setCellValue('N3', 'Kebun');
			$PHPExcel->getActiveSheet()->setCellValue('O3', 'Tambak');
			$PHPExcel->getActiveSheet()->setCellValue('P3', 'Tanah Kering');
			$PHPExcel->getActiveSheet()->setCellValue('Q3', 'Patok Ada');
			$PHPExcel->getActiveSheet()->setCellValue('R3', 'Patok Tidak Ada');
			$PHPExcel->getActiveSheet()->setCellValue('S3', 'Papan Ada');
			$PHPExcel->getActiveSheet()->setCellValue('T3', 'Papan Tidak Ada');
			$PHPExcel->getActiveSheet()->setCellValue('U3', 'Lokasi');
			$PHPExcel->getActiveSheet()->setCellValue('V3', 'Peruntukkan');
						
			$i = 1;
			$kolom = 4;

			foreach(TanahMilik::model()->findAll() as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->asal_tanah);
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->nomor_sertifikat);
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->luas);
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->klas);
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->asli);
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->bantuan_pemerintah);
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->bantuan_provinsi);
				$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->bantuan_kabkota);
				$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->lain_lain);
				$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->tanggal_perolehan);
				$PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, $data->sawah);
				$PHPExcel->getActiveSheet()->setCellValue('M'.$kolom, $data->tegal);
				$PHPExcel->getActiveSheet()->setCellValue('N'.$kolom, $data->kebun);
				$PHPExcel->getActiveSheet()->setCellValue('O'.$kolom, $data->tambak);
				$PHPExcel->getActiveSheet()->setCellValue('P'.$kolom, $data->tanah_kering);
				$PHPExcel->getActiveSheet()->setCellValue('Q'.$kolom, $data->patok_ada);
				$PHPExcel->getActiveSheet()->setCellValue('R'.$kolom, $data->patok_tidak_ada);
				$PHPExcel->getActiveSheet()->setCellValue('S'.$kolom, $data->papan_ada);
				$PHPExcel->getActiveSheet()->setCellValue('T'.$kolom, $data->papan_tidak_ada);
				$PHPExcel->getActiveSheet()->setCellValue('U'.$kolom, $data->lokasi);
				$PHPExcel->getActiveSheet()->setCellValue('V'.$kolom, $data->peruntukkan);
									
				$i++; $kolom++;
			}
	
			$filename = time().'_TanahMilik.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=TanahMilik::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tanah-milik-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
