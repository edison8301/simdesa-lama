-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 29. Nopember 2013 jam 08:28
-- Versi Server: 5.1.33
-- Versi PHP: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simdesa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agama`
--

CREATE TABLE IF NOT EXISTS `agama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `agama`
--

INSERT INTO `agama` (`id`, `nama`) VALUES
(1, 'Islam'),
(2, 'Kristen Katolik'),
(3, 'Kristen Protestan'),
(4, 'Budha'),
(5, 'Hindu'),
(6, 'Konghucu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `masuk_nomor` varchar(255) DEFAULT NULL,
  `masuk_tanggal` date DEFAULT NULL,
  `masuk_pengirim` varchar(255) DEFAULT NULL,
  `masuk_isi` varchar(255) DEFAULT NULL,
  `keluar_isi` varchar(255) DEFAULT NULL,
  `keluar_nomor` varchar(255) DEFAULT NULL,
  `keluar_tanggal` date DEFAULT NULL,
  `keluar_tujuan` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `agenda`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `anggaran`
--

CREATE TABLE IF NOT EXISTS `anggaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anggaran_jenis_id` int(11) DEFAULT NULL,
  `kode_1` int(2) DEFAULT NULL,
  `kode_2` int(2) DEFAULT NULL,
  `kode_3` int(2) DEFAULT NULL,
  `kode_4` int(2) DEFAULT NULL,
  `kode_5` int(2) DEFAULT NULL,
  `kode_6` int(2) DEFAULT NULL,
  `uraian` text,
  `jumlah` decimal(15,0) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `anggaran`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `aparat_pemerintah`
--

CREATE TABLE IF NOT EXISTS `aparat_pemerintah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `niap` varchar(255) DEFAULT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `pangkat_golongan_id` int(11) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `pendidikan_id` int(11) DEFAULT NULL,
  `nomor_pengangkatan` varchar(255) DEFAULT NULL,
  `tanggal_pengangkatan` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `aparat_pemerintah`
--

INSERT INTO `aparat_pemerintah` (`id`, `nama`, `niap`, `nip`, `jenis_kelamin_id`, `tempat_lahir`, `tanggal_lahir`, `agama_id`, `pangkat_golongan_id`, `jabatan`, `pendidikan_id`, `nomor_pengangkatan`, `tanggal_pengangkatan`, `keterangan`) VALUES
(2, 'HASNAN', '', '', 1, 'Tanjung Batu Itam', '2013-11-11', 1, 1, 'Kepala Desa', 3, '108.45/550/KEP/I/2009', '2009-12-10', 'Masa Tugas 6 Tahun'),
(3, 'SUANDI', '', '197108152002121003', 1, 'Air Seru', '1971-08-15', 1, 7, 'Sekertaris Desa', 8, '188.5-163 TH 2012', '2012-03-02', 'PNS');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dusun`
--

CREATE TABLE IF NOT EXISTS `dusun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `dusun`
--

INSERT INTO `dusun` (`id`, `nama`) VALUES
(1, 'Dusun A'),
(2, 'Dusun B');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ekspedisi`
--

CREATE TABLE IF NOT EXISTS `ekspedisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pengiriman` date NOT NULL,
  `nomor_surat` varchar(255) DEFAULT NULL,
  `tanggal_surat` date DEFAULT NULL,
  `isi_singkat` varchar(255) DEFAULT NULL,
  `tujuan_surat` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `ekspedisi`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris_desa`
--

CREATE TABLE IF NOT EXISTS `inventaris_desa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_barang` varchar(255) NOT NULL,
  `dibeli_sendiri` varchar(255) DEFAULT NULL,
  `bantuan_pemerintah` varchar(255) DEFAULT NULL,
  `bantuan_provinsi` varchar(255) DEFAULT NULL,
  `bantuan_kabkota` varchar(255) DEFAULT NULL,
  `sumbangan` varchar(255) DEFAULT NULL,
  `keadaan_awal_baik` varchar(255) DEFAULT NULL,
  `keadaan_awal_rusak` varchar(255) DEFAULT NULL,
  `penghapusan_rusak` varchar(255) DEFAULT NULL,
  `penghapusan_dijual` varchar(255) DEFAULT NULL,
  `penghapusan_disumbangkan` varchar(255) DEFAULT NULL,
  `tanggal_penghapusan` date DEFAULT NULL,
  `keadaan_akhir_baik` varchar(255) DEFAULT NULL,
  `keadaan_akhir_rusak` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `inventaris_desa`
--

INSERT INTO `inventaris_desa` (`id`, `jenis_barang`, `dibeli_sendiri`, `bantuan_pemerintah`, `bantuan_provinsi`, `bantuan_kabkota`, `sumbangan`, `keadaan_awal_baik`, `keadaan_awal_rusak`, `penghapusan_rusak`, `penghapusan_dijual`, `penghapusan_disumbangkan`, `tanggal_penghapusan`, `keadaan_akhir_baik`, `keadaan_akhir_rusak`, `keterangan`) VALUES
(3, 'Meja Rapat', 'DAU', '', '', '', '', '5 buah', '', '', '', '', '0000-00-00', '', '', '2001'),
(2, 'Meja Tulis', '', '', '', '', '', '25 buah', '', '', '', '', '0000-00-00', '', '', '1998 s/d 2013');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris_proyek`
--

CREATE TABLE IF NOT EXISTS `inventaris_proyek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_proyek` varchar(255) NOT NULL,
  `volume` varchar(255) DEFAULT NULL,
  `biaya` decimal(15,0) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `tahun` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `inventaris_proyek`
--

INSERT INTO `inventaris_proyek` (`id`, `nama_proyek`, `volume`, `biaya`, `lokasi`, `keterangan`, `tahun`) VALUES
(2, 'Kebun Desa', '17 Ha', 165742000, 'Dusun Gunong Rt.08 Desa Jangkar Asam', 'ADDP', 2008),
(3, 'Gedung PAUD', '1 Unit', 169590000, 'Dusun Padang Rt.02 Desa Jangkar Asam', 'PNPM-MP', 2008);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_kelamin`
--

CREATE TABLE IF NOT EXISTS `jenis_kelamin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `jenis_kelamin`
--

INSERT INTO `jenis_kelamin` (`id`, `nama`) VALUES
(1, 'Laki-laki'),
(2, 'Perempuan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kader_pembangunan`
--

CREATE TABLE IF NOT EXISTS `kader_pembangunan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `umur` int(11) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `pendidikan` varchar(255) DEFAULT NULL,
  `bidang` varchar(255) DEFAULT NULL,
  `alamat` text,
  `keterangan` text,
  PRIMARY KEY (`id`),
  KEY `FK_kader_pembangunan` (`jenis_kelamin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `kader_pembangunan`
--

INSERT INTO `kader_pembangunan` (`id`, `nama`, `umur`, `jenis_kelamin_id`, `pendidikan`, `bidang`, `alamat`, `keterangan`) VALUES
(3, 'SUMADI', 39, 1, 'Paket B', 'PNPM TPPMD', 'Dusun Gunung RT 10 Desa Jangkar Asam', ''),
(4, 'RIANTI WULANDARI', 21, 2, 'SMA', 'TPPMD', 'Dusun Gunung RT 8 Desa Jangkar Asam', ''),
(5, 'Haifriani Megawati', 29, 2, 'SMA', 'TPPMD-LPM', 'Dusun Gunong Rt.13 Desa Jangkar Asam', ''),
(6, 'ZANI HASIMAN', 57, 1, 'SD', 'TPPMD', 'Dusun Padang Rt.01 Desa Jangkar Asam', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kas`
--

CREATE TABLE IF NOT EXISTS `kas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kas_jenis_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `uraian` text,
  `nomor_bukti` varchar(255) DEFAULT NULL,
  `kode_1` int(2) DEFAULT NULL,
  `kode_2` int(2) DEFAULT NULL,
  `kode_3` int(2) DEFAULT NULL,
  `kode_4` int(2) DEFAULT NULL,
  `kode_5` int(2) DEFAULT NULL,
  `kode_6` int(2) DEFAULT NULL,
  `jumlah` decimal(15,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `kas`
--

INSERT INTO `kas` (`id`, `kas_jenis_id`, `tanggal`, `uraian`, `nomor_bukti`, `kode_1`, `kode_2`, `kode_3`, `kode_4`, `kode_5`, `kode_6`, `jumlah`) VALUES
(1, 1, '2013-11-13', 'Pembuatan irigasi sawah\r\n', 'SDfsa', 5, 1, 1, 3, NULL, NULL, 3000000),
(5, 1, '2013-11-07', 'Belanja bensin', 'KS002', 5, 2, 1, 18, NULL, NULL, 30000),
(6, 1, '2013-11-06', 'tes', 'AS34', 5, 2, 1, NULL, NULL, NULL, 5000000),
(4, 2, '2013-11-20', 'Belanja jasa STNK', '', 5, 2, 2, 18, 10, NULL, 450000),
(7, 2, '2013-11-15', 'TES PENGELUARAN', '', 5, 2, 1, NULL, NULL, NULL, 450000),
(8, 1, '2013-11-19', 'uraian pengeluaran', '', 5, 1, 2, 14, NULL, NULL, 250000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kas_jenis`
--

CREATE TABLE IF NOT EXISTS `kas_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kas_jenis`
--

INSERT INTO `kas_jenis` (`id`, `nama`) VALUES
(1, 'Penerimaan'),
(2, 'Pengeluaran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kedudukan_dalam_keluarga`
--

CREATE TABLE IF NOT EXISTS `kedudukan_dalam_keluarga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `kedudukan_dalam_keluarga`
--

INSERT INTO `kedudukan_dalam_keluarga` (`id`, `nama`) VALUES
(1, 'KK'),
(2, 'Ist'),
(3, 'AK'),
(4, 'AA'),
(5, 'Pemb');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan_pembangunan`
--

CREATE TABLE IF NOT EXISTS `kegiatan_pembangunan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_proyek` varchar(255) NOT NULL,
  `volume` varchar(255) DEFAULT NULL,
  `dana_pemerintah` varchar(255) DEFAULT NULL,
  `dana_provinsi` varchar(255) DEFAULT NULL,
  `dana_kabkota` varchar(255) DEFAULT NULL,
  `dana_swadaya` varchar(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  `waktu` varchar(255) DEFAULT NULL,
  `baru` varchar(255) DEFAULT NULL,
  `lanjutan` varchar(255) DEFAULT NULL,
  `pelaksana` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `tahun` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kegiatan_pembangunan`
--

INSERT INTO `kegiatan_pembangunan` (`id`, `nama_proyek`, `volume`, `dana_pemerintah`, `dana_provinsi`, `dana_kabkota`, `dana_swadaya`, `jumlah`, `waktu`, `baru`, `lanjutan`, `pelaksana`, `keterangan`, `tahun`) VALUES
(2, 'Kantor BPD', '1(unit)', '', '', '', '', '1 (satu)', '', 'v', '', 'TPPMD', '', 2013);

-- --------------------------------------------------------

--
-- Struktur dari tabel `keputusan_kades`
--

CREATE TABLE IF NOT EXISTS `keputusan_kades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_keputusan` varchar(255) NOT NULL,
  `tanggal_keputusan` date DEFAULT NULL,
  `tentang` varchar(255) DEFAULT NULL,
  `uraian_singkat` varchar(255) DEFAULT NULL,
  `nomor_dilaporkan` varchar(255) DEFAULT NULL,
  `tanggal_dilaporkan` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `keputusan_kades`
--

INSERT INTO `keputusan_kades` (`id`, `nomor_keputusan`, `tanggal_keputusan`, `tentang`, `uraian_singkat`, `nomor_dilaporkan`, `tanggal_dilaporkan`, `keterangan`) VALUES
(3, '02/SK-TBI/I/2012', '2012-01-09', 'Penunjukan petugas kebersihan kantor PKK', 'Penugasan untuk melakukan pembersihan kantor PKK', '02/SK-TBI/I/2012', '2012-01-09', ''),
(2, '01/SK-TBI/I/2012', '2012-01-07', 'Penunjukan Bendaharaan desa Tanjung Batu Itam', 'Keputusan kepala desa tanjung batu hitam tentang penunjukkan bendaharawan desa tanjung batu itam', '', '2012-01-07', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kewarganegaraan`
--

CREATE TABLE IF NOT EXISTS `kewarganegaraan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kewarganegaraan`
--

INSERT INTO `kewarganegaraan` (`id`, `nama`) VALUES
(1, 'WNI'),
(2, 'WNA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi_kurang`
--

CREATE TABLE IF NOT EXISTS `mutasi_kurang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `mutasi_kurang`
--

INSERT INTO `mutasi_kurang` (`id`, `nama`) VALUES
(1, 'Mati'),
(2, 'Pindah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi_penduduk`
--

CREATE TABLE IF NOT EXISTS `mutasi_penduduk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `kewarganegaraan_id` int(11) DEFAULT NULL,
  `datang` varchar(255) DEFAULT NULL,
  `datang_tanggal` date DEFAULT NULL,
  `lahir` varchar(255) DEFAULT NULL,
  `lahir_tanggal` date DEFAULT NULL,
  `pindah` varchar(255) DEFAULT NULL,
  `pindah_tanggal` date DEFAULT NULL,
  `mati` varchar(255) DEFAULT NULL,
  `mati_tanggal` date DEFAULT NULL,
  `dusun_id` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `mutasi_penduduk`
--

INSERT INTO `mutasi_penduduk` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin_id`, `kewarganegaraan_id`, `datang`, `datang_tanggal`, `lahir`, `lahir_tanggal`, `pindah`, `pindah_tanggal`, `mati`, `mati_tanggal`, `dusun_id`, `keterangan`) VALUES
(1, 'Thomas', '', NULL, 1, 1, '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', NULL, ''),
(2, 'asdfas', '', '0000-00-00', NULL, 0, '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi_tambah`
--

CREATE TABLE IF NOT EXISTS `mutasi_tambah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `mutasi_tambah`
--

INSERT INTO `mutasi_tambah` (`id`, `nama`) VALUES
(1, 'Lahir'),
(2, 'Datang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pangkat_golongan`
--

CREATE TABLE IF NOT EXISTS `pangkat_golongan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `pangkat_golongan`
--

INSERT INTO `pangkat_golongan` (`id`, `nama`) VALUES
(1, 'I/A'),
(2, 'I/B'),
(3, 'I/C'),
(4, 'I/D'),
(5, 'II/A'),
(6, 'II/B'),
(7, 'II/C'),
(8, 'II/D'),
(9, 'III/A'),
(10, 'III/B'),
(11, 'III/C'),
(12, 'III/D'),
(13, 'IV/A'),
(14, 'IV/B'),
(15, 'IV/C'),
(16, 'IV/D'),
(17, 'IV/E');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pekerjaan`
--

CREATE TABLE IF NOT EXISTS `pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `pekerjaan`
--

INSERT INTO `pekerjaan` (`id`, `nama`) VALUES
(1, 'PNS'),
(2, 'TNI/POLRI'),
(3, 'Karyawan'),
(4, 'Buruh'),
(5, 'Nelayan'),
(6, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan`
--

CREATE TABLE IF NOT EXISTS `pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `pendidikan`
--

INSERT INTO `pendidikan` (`id`, `nama`) VALUES
(1, 'SD'),
(2, 'SMP'),
(3, 'SMA'),
(4, 'D1'),
(5, 'D2'),
(6, 'D3'),
(7, 'D4'),
(8, 'S1'),
(9, 'S2'),
(10, 'S3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penduduk`
--

CREATE TABLE IF NOT EXISTS `penduduk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `status_perkawinan_id` int(11) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `pendidikan_id` int(11) DEFAULT NULL,
  `pekerjaan_id` int(11) DEFAULT NULL,
  `dapat_membaca` varchar(10) DEFAULT NULL,
  `kewarganegaraan_id` int(11) DEFAULT NULL,
  `alamat` text,
  `dusun_id` int(11) DEFAULT NULL,
  `rw` varchar(10) DEFAULT NULL,
  `rt` varchar(10) DEFAULT NULL,
  `kedudukan_dalam_keluarga_id` int(11) DEFAULT NULL,
  `nomor_ktp` varchar(255) DEFAULT NULL,
  `nomor_ksk` varchar(255) DEFAULT NULL,
  `mutasi_tambah_id` int(11) DEFAULT NULL,
  `mutasi_tambah_tanggal` date DEFAULT NULL,
  `mutasi_tambah_keterangan` varchar(255) DEFAULT NULL,
  `mutasi_kurang_id` int(11) DEFAULT NULL,
  `mutasi_kurang_tanggal` date DEFAULT NULL,
  `mutasi_kurang_keterangan` varchar(255) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`),
  KEY `FK_penduduk-agama` (`agama_id`),
  KEY `FK_penduduk` (`jenis_kelamin_id`),
  KEY `FK_penduduk-status` (`status_perkawinan_id`),
  KEY `FK_penduduk-pendidikan` (`pendidikan_id`),
  KEY `FK_penduduk-pekerjaan` (`pekerjaan_id`),
  KEY `FK_penduduk-kewarganegaraan` (`kewarganegaraan_id`),
  KEY `FK_penduduk-kedudukan` (`kedudukan_dalam_keluarga_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `penduduk`
--

INSERT INTO `penduduk` (`id`, `nama`, `foto`, `jenis_kelamin_id`, `status_perkawinan_id`, `tempat_lahir`, `tanggal_lahir`, `agama_id`, `pendidikan_id`, `pekerjaan_id`, `dapat_membaca`, `kewarganegaraan_id`, `alamat`, `dusun_id`, `rw`, `rt`, `kedudukan_dalam_keluarga_id`, `nomor_ktp`, `nomor_ksk`, `mutasi_tambah_id`, `mutasi_tambah_tanggal`, `mutasi_tambah_keterangan`, `mutasi_kurang_id`, `mutasi_kurang_tanggal`, `mutasi_kurang_keterangan`, `keterangan`) VALUES
(3, 'Reza Wajib Belajar', NULL, 2, 4, 'Manggar', '2013-11-12', 2, 1, 1, '1', 1, 'Jln Burung gereja', 1, '', '', 1, '0782368971864728', '98650887766877987', 1, '2013-11-12', '', NULL, '0000-00-00', '', 'OK'),
(4, 'Thomas Pemilih', NULL, 1, 1, 'Manggar', '1986-06-03', 1, 8, 1, '1', 1, '', 1, '', '', 1, '', '', 2, '2013-11-15', 'Bandung', NULL, '0000-00-00', '', ''),
(5, 'Budi Balita', NULL, 1, 1, '', '2010-02-17', 1, 1, 1, '1', 1, '', 1, '1', '2', 1, '', '', 1, '2010-02-17', '', 2, '2013-11-28', 'Cikampek', ''),
(6, 'Thomas', NULL, 1, 1, '', '2013-08-13', 1, 1, 1, '1', 1, '', 1, '', '', 2, '', '', 1, '2013-08-13', '', NULL, '2013-11-06', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penduduk_sementara`
--

CREATE TABLE IF NOT EXISTS `penduduk_sementara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `nomor_identitas` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `kebangsaan` varchar(255) DEFAULT NULL,
  `keturunan` varchar(255) DEFAULT NULL,
  `datang_dari` varchar(255) DEFAULT NULL,
  `maksud_kedatangan` varchar(255) DEFAULT NULL,
  `nama_alamat_tujuan` varchar(255) DEFAULT NULL,
  `datang_tanggal` date DEFAULT NULL,
  `pergi_tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `penduduk_sementara`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE IF NOT EXISTS `pengaturan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `pengaturan`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `peraturan_desa`
--

CREATE TABLE IF NOT EXISTS `peraturan_desa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(255) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `tentang` varchar(255) DEFAULT NULL,
  `uraian_singkat` varchar(255) DEFAULT NULL,
  `nomor_persetujuan` varchar(255) DEFAULT NULL,
  `tanggal_persetujuan` date DEFAULT NULL,
  `nomor_dilaporkan` varchar(255) DEFAULT NULL,
  `tanggal_dilaporkan` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `peraturan_desa`
--

INSERT INTO `peraturan_desa` (`id`, `nomor`, `tanggal`, `tentang`, `uraian_singkat`, `nomor_persetujuan`, `tanggal_persetujuan`, `nomor_dilaporkan`, `tanggal_dilaporkan`, `keterangan`) VALUES
(1, '1234234 adsfa sfda sdfasdfa', '2013-11-27', '', '', '', '2013-11-13', '', '2013-11-29', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rencana_pembangunan`
--

CREATE TABLE IF NOT EXISTS `rencana_pembangunan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_proyek` varchar(255) NOT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `dana_pemerintah` varchar(255) DEFAULT NULL,
  `dana_provinsi` varchar(255) DEFAULT NULL,
  `dana_kabkota` varchar(255) DEFAULT NULL,
  `dana_swadaya` varchar(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  `pelaksana` varchar(255) DEFAULT NULL,
  `manfaat` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `tahun` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `rencana_pembangunan`
--

INSERT INTO `rencana_pembangunan` (`id`, `nama_proyek`, `lokasi`, `dana_pemerintah`, `dana_provinsi`, `dana_kabkota`, `dana_swadaya`, `jumlah`, `pelaksana`, `manfaat`, `keterangan`, `tahun`) VALUES
(2, 'Pembuatan Jembatan AIR NIKI', 'Dusun Gunong Rt.11Desa Jangkar Asam', '', '', '', '', '1 (satu) Unit', 'PNPM', 'Menghubungkan Jalan dari Rt.11 ke Rt.14', '', 2013),
(3, 'Gapura', 'Dusun gunong Rt.08 Desa Jangkar Asam', 'v', '', '', '', '1 (satu) Unit', 'TPPMD', '', '', 2013),
(4, 'Tempat Parkir Nelayan', 'Pangkalan Tanjung Batu Itam', '', '', 'Alokasi Dana Desa', '', '1 (satu) Unit, 15 Juta', 'TPPMD', 'Untuk parkir kendaraan nelayan yang melaut', '', 2013);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `nama`) VALUES
(1, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_perkawinan`
--

CREATE TABLE IF NOT EXISTS `status_perkawinan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `status_perkawinan`
--

INSERT INTO `status_perkawinan` (`id`, `nama`) VALUES
(1, 'K'),
(2, 'BK'),
(3, 'D'),
(4, 'J');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat`
--

CREATE TABLE IF NOT EXISTS `surat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(255) DEFAULT NULL,
  `penduduk_id` int(11) DEFAULT NULL,
  `surat_template_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data untuk tabel `surat`
--

INSERT INTO `surat` (`id`, `nomor`, `penduduk_id`, `surat_template_id`) VALUES
(1, '234234', NULL, 1),
(2, '123', 1, 2),
(3, '212', NULL, 2),
(4, '234fad', 1, 1),
(5, '2342', 3, 2),
(6, '12', 3, 2),
(7, '123', 3, 2),
(8, '', 3, 9),
(9, '32', 3, 9),
(10, '45', 3, 7),
(11, '78', 4, 5),
(12, '23', 3, 5),
(13, '23', 5, 9),
(14, '35', 3, 10),
(15, '23', 3, 10),
(16, '12', 4, 6),
(17, '23', 3, 2),
(18, '1232', 3, 5),
(19, '', 4, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_atribut`
--

CREATE TABLE IF NOT EXISTS `surat_atribut` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surat_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data untuk tabel `surat_atribut`
--

INSERT INTO `surat_atribut` (`id`, `surat_id`, `key`, `value`) VALUES
(1, 1, 'keperluan', 'tes'),
(2, 2, 'keperluan', 'tes'),
(3, 2, 'tanggal', '21 Januari 2014'),
(4, 1, 'tanggal', ''),
(5, 3, 'keperluan', ''),
(6, 3, 'tanggal', ''),
(7, 3, 'batas_utara', ''),
(8, 4, 'keperluan', ''),
(9, 4, 'tanggal', ''),
(10, 5, 'keperluan', ''),
(11, 5, 'tanggal', ''),
(12, 5, 'batas_utara', ''),
(13, 6, 'keperluan', ''),
(14, 6, 'tanggal', ''),
(15, 6, 'batas_utara', ''),
(16, 7, 'keperluan', ''),
(17, 7, 'tanggal', ''),
(18, 7, 'batas_utara', ''),
(19, 17, 'keperluan', ''),
(20, 17, 'tanggal', ''),
(21, 17, 'batas_utara', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_template`
--

CREATE TABLE IF NOT EXISTS `surat_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `surat_template`
--

INSERT INTO `surat_template` (`id`, `nama`, `file`) VALUES
(1, 'Surat Kelahiran', '1385183809_Daftar-Harga-Layanan-Maintenance-Website-Reguler.docx'),
(2, 'Surat Kematian', '1385334632_template-surat-kematian.docx'),
(3, 'Surat Pindah Penduduk', ''),
(4, 'Surat SKCK', ''),
(5, 'Surat Belum Pernah Menikah', '1385432120_Surat-Keterangan-Belum-Menikah.docx'),
(6, 'Surat Keterangan / Pengantar', ''),
(7, 'Surat Keterangan Tidak Mampu', '1385335118_template-surat-keterangan-tidak-mampu.docx'),
(8, 'Surat Kedatangan Menjadi Penduduk', ''),
(9, 'Surat Keterangan Usaha', '1385334866_template-surat-keterangan-usaha.docx'),
(10, 'SKU Baru', '1385356495_format-surat-keterangan-usaha.docx');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_template_atribut`
--

CREATE TABLE IF NOT EXISTS `surat_template_atribut` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surat_template_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `tipe` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `surat_template_atribut`
--

INSERT INTO `surat_template_atribut` (`id`, `surat_template_id`, `key`, `tipe`) VALUES
(1, 1, 'keperluan', ''),
(3, 2, 'keperluan', ''),
(4, 2, 'tanggal', ''),
(5, 2, 'batas_utara', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tanah`
--

CREATE TABLE IF NOT EXISTS `tanah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `jumlah` decimal(10,2) DEFAULT NULL,
  `hm` decimal(10,2) DEFAULT NULL,
  `hgb` decimal(10,2) DEFAULT NULL,
  `hp` decimal(10,2) DEFAULT NULL,
  `hgu` decimal(10,2) DEFAULT NULL,
  `hpl` decimal(10,2) DEFAULT NULL,
  `ma` decimal(10,2) DEFAULT NULL,
  `vi` decimal(10,2) DEFAULT NULL,
  `tn` decimal(10,2) DEFAULT NULL,
  `perumahan` decimal(10,2) DEFAULT NULL,
  `perdagangan` decimal(10,2) DEFAULT NULL,
  `perkantoran` decimal(10,2) DEFAULT NULL,
  `industri` decimal(10,2) DEFAULT NULL,
  `fasilitas_umum` decimal(10,2) DEFAULT NULL,
  `sawah` decimal(10,2) DEFAULT NULL,
  `tegalan` decimal(10,2) DEFAULT NULL,
  `perkebunan` decimal(10,2) DEFAULT NULL,
  `peternakan` decimal(10,2) DEFAULT NULL,
  `hutan_belukar` decimal(10,2) DEFAULT NULL,
  `hutan_lindung` decimal(10,2) DEFAULT NULL,
  `tanah_kosong` decimal(10,2) DEFAULT NULL,
  `lain_lain` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `tanah`
--

INSERT INTO `tanah` (`id`, `nama`, `jumlah`, `hm`, `hgb`, `hp`, `hgu`, `hpl`, `ma`, `vi`, `tn`, `perumahan`, `perdagangan`, `perkantoran`, `industri`, `fasilitas_umum`, `sawah`, `tegalan`, `perkebunan`, `peternakan`, `hutan_belukar`, `hutan_lindung`, `tanah_kosong`, `lain_lain`) VALUES
(1, 'Thomas Alfa Edison', 12.00, 280.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(2, 'Pemerintah Desa', 8630.00, 2000.00, 1000.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3000.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tanah_milik`
--

CREATE TABLE IF NOT EXISTS `tanah_milik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asal_tanah` varchar(255) NOT NULL,
  `nomor_sertifikat` varchar(255) DEFAULT NULL,
  `luas` varchar(255) DEFAULT NULL,
  `klas` varchar(255) DEFAULT NULL,
  `asli` varchar(255) DEFAULT NULL,
  `bantuan_pemerintah` varchar(255) DEFAULT NULL,
  `bantuan_provinsi` varchar(255) DEFAULT NULL,
  `bantuan_kabkota` varchar(255) DEFAULT NULL,
  `lain_lain` varchar(255) DEFAULT NULL,
  `tanggal_perolehan` varchar(255) DEFAULT NULL,
  `sawah` varchar(255) DEFAULT NULL,
  `tegal` varchar(255) DEFAULT NULL,
  `kebun` varchar(255) DEFAULT NULL,
  `tambak` varchar(255) DEFAULT NULL,
  `tanah_kering` varchar(255) DEFAULT NULL,
  `patok_ada` varchar(255) DEFAULT NULL,
  `patok_tidak_ada` varchar(255) DEFAULT NULL,
  `papan_ada` varchar(255) DEFAULT NULL,
  `papan_tidak_ada` varchar(255) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `peruntukkan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `tanah_milik`
--

INSERT INTO `tanah_milik` (`id`, `asal_tanah`, `nomor_sertifikat`, `luas`, `klas`, `asli`, `bantuan_pemerintah`, `bantuan_provinsi`, `bantuan_kabkota`, `lain_lain`, `tanggal_perolehan`, `sawah`, `tegal`, `kebun`, `tambak`, `tanah_kering`, `patok_ada`, `patok_tidak_ada`, `papan_ada`, `papan_tidak_ada`, `lokasi`, `peruntukkan`) VALUES
(1, 'Tanah Kas Desa', '-', '8630', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Kebun Kelapa'),
(2, 'Tanah Lapangan Bola Kaki', '', '8800', '', 'Asli', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Lapangan Bola Kali Dusun Tanjung Batu Itam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role_id`) VALUES
(1, 'admin', 'admin', 1),
(2, 'bendahara', 'bendahara', NULL);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kader_pembangunan`
--
ALTER TABLE `kader_pembangunan`
  ADD CONSTRAINT `FK_kader_pembangunan` FOREIGN KEY (`jenis_kelamin_id`) REFERENCES `jenis_kelamin` (`id`);

--
-- Ketidakleluasaan untuk tabel `penduduk`
--
ALTER TABLE `penduduk`
  ADD CONSTRAINT `FK_penduduk` FOREIGN KEY (`jenis_kelamin_id`) REFERENCES `jenis_kelamin` (`id`),
  ADD CONSTRAINT `FK_penduduk-agama` FOREIGN KEY (`agama_id`) REFERENCES `agama` (`id`),
  ADD CONSTRAINT `FK_penduduk-kedudukan` FOREIGN KEY (`kedudukan_dalam_keluarga_id`) REFERENCES `kedudukan_dalam_keluarga` (`id`),
  ADD CONSTRAINT `FK_penduduk-kewarganegaraan` FOREIGN KEY (`kewarganegaraan_id`) REFERENCES `kewarganegaraan` (`id`),
  ADD CONSTRAINT `FK_penduduk-pekerjaan` FOREIGN KEY (`pekerjaan_id`) REFERENCES `pekerjaan` (`id`),
  ADD CONSTRAINT `FK_penduduk-pendidikan` FOREIGN KEY (`pendidikan_id`) REFERENCES `pendidikan` (`id`),
  ADD CONSTRAINT `FK_penduduk-status` FOREIGN KEY (`status_perkawinan_id`) REFERENCES `status_perkawinan` (`id`);
