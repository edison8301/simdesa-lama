<?php

/**
 * This is the model class for table "agenda".
 *
 * The followings are the available columns in table 'agenda':
 * @property integer $id
 * @property string $tanggal
 * @property string $masuk_nomor
 * @property string $masuk_tanggal
 * @property string $masuk_pengirim
 * @property string $masuk_isi
 * @property string $keluar_isi
 * @property string $keluar_nomor
 * @property string $keluar_tanggal
 * @property string $keluar_tujuan
 * @property string $keterangan
 */
class Agenda extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Agenda the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agenda';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tanggal', 'required'),
			array('masuk_nomor, masuk_pengirim, masuk_isi, keluar_isi, keluar_nomor, keluar_tujuan, keterangan', 'length', 'max'=>255),
			array('masuk_tanggal, keluar_tanggal', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tanggal, masuk_nomor, masuk_tanggal, masuk_pengirim, masuk_isi, keluar_isi, keluar_nomor, keluar_tanggal, keluar_tujuan, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal',
			'masuk_nomor' => 'Nomor Surat',
			'masuk_tanggal' => 'Tanggal Surat',
			'masuk_pengirim' => 'Pengirim',
			'masuk_isi' => 'Isi Singkat',
			'keluar_isi' => 'Isi Singkat',
			'keluar_nomor' => 'Nomor Pengiriman',
			'keluar_tanggal' => 'Tanggal Pengiriman',
			'keluar_tujuan' => 'Tujuan',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('masuk_nomor',$this->masuk_nomor,true);
		$criteria->compare('masuk_tanggal',$this->masuk_tanggal,true);
		$criteria->compare('masuk_pengirim',$this->masuk_pengirim,true);
		$criteria->compare('masuk_isi',$this->masuk_isi,true);
		$criteria->compare('keluar_isi',$this->keluar_isi,true);
		$criteria->compare('keluar_nomor',$this->keluar_nomor,true);
		$criteria->compare('keluar_tanggal',$this->keluar_tanggal,true);
		$criteria->compare('keluar_tujuan',$this->keluar_tujuan,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}