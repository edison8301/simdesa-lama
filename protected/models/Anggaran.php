<?php

/**
 * This is the model class for table "anggaran".
 *
 * The followings are the available columns in table 'anggaran':
 * @property integer $id
 * @property integer $anggaran_jenis_id
 * @property integer $kode_1
 * @property integer $kode_2
 * @property integer $kode_3
 * @property integer $kode_4
 * @property integer $kode_5
 * @property integer $kode_6
 * @property string $uraian
 * @property string $jumlah
 * @property string $keterangan
 */
class Anggaran extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Anggaran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'anggaran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('anggaran_jenis_id, kode_1, kode_2, kode_3, kode_4, kode_5, kode_6', 'numerical', 'integerOnly'=>true),
			array('jumlah', 'length', 'max'=>15),
			array('uraian, keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, anggaran_jenis_id, kode_1, kode_2, kode_3, kode_4, kode_5, kode_6, uraian, jumlah, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'anggaran_jenis_id' => 'Anggaran Jenis',
			'kode_1' => 'Kode 1',
			'kode_2' => 'Kode 2',
			'kode_3' => 'Kode 3',
			'kode_4' => 'Kode 4',
			'kode_5' => 'Kode 5',
			'kode_6' => 'Kode 6',
			'uraian' => 'Uraian',
			'jumlah' => 'Jumlah',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('anggaran_jenis_id',$this->anggaran_jenis_id);
		$criteria->compare('kode_1',$this->kode_1);
		$criteria->compare('kode_2',$this->kode_2);
		$criteria->compare('kode_3',$this->kode_3);
		$criteria->compare('kode_4',$this->kode_4);
		$criteria->compare('kode_5',$this->kode_5);
		$criteria->compare('kode_6',$this->kode_6);
		$criteria->compare('uraian',$this->uraian,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getDataAnggaran($jenis=null)
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'id ASC';
		
		$tanggal_awal = date('Y-m').'-01';
		$tanggal_akhir = date('Y-m').'-'.date('t');
		
		if(!empty($_POST['tanggal_awal']) AND !empty($_POST['tanggal_akhir']))
		{
			$tanggal_awal = $_POST['tanggal_awal'];
			$tanggal_akhir = $_POST['tanggal_akhir'];
		}
		
		
		if($jenis=='penerimaan')
			$criteria->addCondition('anggaran_jenis_id=1');
		
		if($jenis=='pengeluaran')
			$criteria->addCondition('anggaran_jenis_id=2');
		
		if(isset($_POST['kode_1']) AND $_POST['kode_1']!=null)
			$criteria->addCondition('kode_1='.$_POST['kode_1']);
			
		if(isset($_POST['kode_2']) AND $_POST['kode_2']!=null)
			$criteria->addCondition('kode_2='.$_POST['kode_2']);
		
		if(isset($_POST['kode_3']) AND $_POST['kode_3']!=null)
			$criteria->addCondition('kode_3='.$_POST['kode_3']);
		
		if(isset($_POST['kode_4']) AND $_POST['kode_4']!=null)
			$criteria->addCondition('kode_4='.$_POST['kode_4']);
		
		if(isset($_POST['kode_5']) AND $_POST['kode_5']!=null)
			$criteria->addCondition('kode_5='.$_POST['kode_5']);
		
		if(isset($_POST['kode_6']) AND $_POST['kode_6']!=null)
			$criteria->addCondition('kode_6='.$_POST['kode_6']);
		
		$model = Anggaran::model()->findAll($criteria);
		
		if($model!==null)
			return $model;
		else
			return false;
	}
}