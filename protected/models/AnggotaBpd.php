<?php

/**
 * This is the model class for table "anggota_bpd".
 *
 * The followings are the available columns in table 'anggota_bpd':
 * @property integer $id
 * @property string $nama
 * @property integer $jenis_kelamin_id
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $agama_id
 * @property string $jabatan
 * @property integer $pendidikan_id
 * @property string $tanggal_pengangkatan
 * @property string $nomor_pengangkatan
 * @property string $tanggal_pemberhentian
 * @property string $nomor_pemberhentian
 * @property string $keterangan
 */
class AnggotaBpd extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnggotaBpd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'anggota_bpd';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('jenis_kelamin_id, agama_id, pendidikan_id', 'numerical', 'integerOnly'=>true),
			array('nama, tempat_lahir, jabatan, nomor_pengangkatan, nomor_pemberhentian, keterangan', 'length', 'max'=>255),
			array('tanggal_lahir, tanggal_pengangkatan, tanggal_pemberhentian', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, jenis_kelamin_id, tempat_lahir, tanggal_lahir, agama_id, jabatan, pendidikan_id, tanggal_pengangkatan, nomor_pengangkatan, tanggal_pemberhentian, nomor_pemberhentian, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisKelamin'=>array(self::BELONGS_TO,'JenisKelamin','jenis_kelamin_id'),
			'agama'=>array(self::BELONGS_TO,'Agama','agama_id'),
			'pendidikan'=>array(self::BELONGS_TO,'Pendidikan','pendidikan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'jenis_kelamin_id' => 'Jenis Kelamin',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'agama_id' => 'Agama',
			'jabatan' => 'Jabatan',
			'pendidikan_id' => 'Pendidikan',
			'tanggal_pengangkatan' => 'Tanggal Pengangkatan',
			'nomor_pengangkatan' => 'Nomor Pengangkatan',
			'tanggal_pemberhentian' => 'Tanggal Pemberhentian',
			'nomor_pemberhentian' => 'Nomor Pemberhentian',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jenis_kelamin_id',$this->jenis_kelamin_id);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('agama_id',$this->agama_id);
		$criteria->compare('jabatan',$this->jabatan,true);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('tanggal_pengangkatan',$this->tanggal_pengangkatan,true);
		$criteria->compare('nomor_pengangkatan',$this->nomor_pengangkatan,true);
		$criteria->compare('tanggal_pemberhentian',$this->tanggal_pemberhentian,true);
		$criteria->compare('nomor_pemberhentian',$this->nomor_pemberhentian,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}