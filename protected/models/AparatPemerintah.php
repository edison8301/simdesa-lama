<?php

/**
 * This is the model class for table "aparat_pemerintah".
 *
 * The followings are the available columns in table 'aparat_pemerintah':
 * @property integer $id
 * @property string $nama
 * @property string $niap
 * @property string $nip
 * @property integer $jenis_kelamin_id
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $agama_id
 * @property integer $pangkat_golongan_id
 * @property string $jabatan
 * @property integer $pendidikan_id
 * @property string $nomor_pengangkatan
 * @property string $tanggal_pengangkatan
 * @property string $keterangan
 */
class AparatPemerintah extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AparatPemerintah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'aparat_pemerintah';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('jenis_kelamin_id, agama_id, pangkat_golongan_id, pendidikan_id', 'numerical', 'integerOnly'=>true),
			array('nama, niap, nip, tempat_lahir, jabatan, nomor_pengangkatan, keterangan', 'length', 'max'=>255),
			array('tanggal_lahir, tanggal_pengangkatan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, niap, nip, jenis_kelamin_id, tempat_lahir, tanggal_lahir, agama_id, pangkat_golongan_id, jabatan, pendidikan_id, nomor_pengangkatan, tanggal_pengangkatan, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisKelamin' => array(self::BELONGS_TO, 'JenisKelamin', 'jenis_kelamin_id'),
			'agama' => array(self::BELONGS_TO, 'Agama', 'agama_id'),
			'pekerjaan' => array(self::BELONGS_TO, 'Pekerjaan', 'pekerjaan_id'),
			'pendidikan' => array(self::BELONGS_TO, 'Pendidikan', 'pendidikan_id'),
			'pangkatGolongan' => array(self::BELONGS_TO, 'PangkatGolongan', 'pangkat_golongan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'niap' => 'NIAP',
			'nip' => 'NIP',
			'jenis_kelamin_id' => 'Jenis Kelamin',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'agama_id' => 'Agama',
			'pangkat_golongan_id' => 'Pangkat Golongan',
			'jabatan' => 'Jabatan',
			'pendidikan_id' => 'Pendidikan',
			'nomor_pengangkatan' => 'Nomor Pengangkatan',
			'tanggal_pengangkatan' => 'Tanggal Pengangkatan',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('niap',$this->niap,true);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('jenis_kelamin_id',$this->jenis_kelamin_id);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('agama_id',$this->agama_id);
		$criteria->compare('pangkat_golongan_id',$this->pangkat_golongan_id);
		$criteria->compare('jabatan',$this->jabatan,true);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('nomor_pengangkatan',$this->nomor_pengangkatan,true);
		$criteria->compare('tanggal_pengangkatan',$this->tanggal_pengangkatan,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}