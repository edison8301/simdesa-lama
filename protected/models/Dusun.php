<?php

/**
 * This is the model class for table "dusun".
 *
 * The followings are the available columns in table 'dusun':
 * @property integer $id
 * @property string $nama
 */
class Dusun extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Dusun the static model class
	 */
	
	public $tanggal_awal;
	
	public $tanggal_akhir;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dusun';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('nama', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function tanggalAwal()
	{
		$tanggal_awal = date('Y-m').'-01';
		
		if(!empty($_POST['tanggal_awal']))
		{
			$tanggal_awal = $_POST['tanggal_awal'];
		}
		
		if(!empty($_GET['tanggal_awal']))
		{
			$tanggal_awal = $_GET['tanggal_awal'];
		}
		
		return $tanggal_awal;
	}
	
	public function tanggalAkhir()
	{
		$tanggal_akhir = date('Y-m').'-'.date('t');
		
		if(!empty($_POST['tanggal_akhir']))
		{
			$tanggal_akhir = $_POST['tanggal_akhir'];
		}
		
		if(!empty($_GET['tanggal_akhir']))
		{
			$tanggal_akhir = $_GET['tanggal_akhir'];
		}
		
		return $tanggal_akhir;
	}
	
	public function countKk($status=null,$tanggal_awal,$tanggal_akhir)
	{
		$criteria = new CDbCriteria;
		
		
		$criteria->addCondition('dusun_id='.$this->id);
		$criteria->addCondition('kedudukan_dalam_keluarga_id=1');
		
		if($status=='awal')
		{
			$criteria->addCondition('mutasi_tambah_tanggal <= '.$tanggal_awal);
		}
		
		if($status=='akhir')
		{
			$criteria->addCondition('mutasi_tambah_tanggal <= '.$tanggal_akhir);
		}
		
		return Penduduk::model()->count($criteria);
	}
	
	public function countPenduduk($jenis_kelamin=null)
	{
		$criteria = new CDbCriteria;
		
		$criteria->addCondition('dusun_id='.$this->id);	
		
		if($jenis_kelamin=='laki-laki')
			$criteria->addCondition('jenis_kelamin_id=1');
		
		if($jenis_kelamin=='perempuan')
			$criteria->addCondition('jenis_kelamin_id=2');
		
		
		return Penduduk::model()->count($criteria);
	}
	
	public function countRekapitulasi($array)
	{
		$criteria = new CDbCriteria;
		$criteriaKurang = new CDbCriteria;
		
		if($array['status']=='awal')
		{
			$criteria->condition = 'mutasi_tambah_tanggal < :tanggal_awal';
			$criteria->params = array(':tanggal_awal'=>$array['tanggal_awal']);
			
			$criteriaKurang->condition = 'mutasi_kurang_id <> null AND mutasi_kurang_tanggal < :tanggal_awal';
			$criteriaKurang->params = array(':tanggal_awal'=>$array['tanggal_awal']);
		}
		
		if($array['status']=='akhir')
		{
			$criteria->condition = 'mutasi_tambah_tanggal <= :tanggal_akhir';
			$criteria->params = array(':tanggal_akhir'=>$array['tanggal_akhir']);
			
			$criteriaKurang->condition = 'mutasi_kurang_id <> 0 AND mutasi_kurang_tanggal <= :tanggal_akhir';
			$criteriaKurang->params = array(':tanggal_akhir'=>$array['tanggal_akhir']);
		}
		
		
		$criteria->addCondition('dusun_id='.$this->id);
		$criteriaKurang->addCondition('dusun_id='.$this->id);
		
		if($array['kedudukan_dalam_keluarga']=='kk')
		{
			$criteria->addCondition('kedudukan_dalam_keluarga_id = 1');
			$criteriaKurang->addCondition('kedudukan_dalam_keluarga_id = 1');
		}
		
		if($array['kedudukan_dalam_keluarga']=='nonkk')
		{
			$criteria->addCondition('kedudukan_dalam_keluarga_id <> 1');
			$criteriaKurang->addCondition('kedudukan_dalam_keluarga_id <> 1');
		}
		
		if($array['jenis_kelamin_id']!=null)
		{
			$criteria->addCondition('jenis_kelamin_id='.$array['jenis_kelamin_id']);
			$criteriaKurang->addCondition('jenis_kelamin_id='.$array['jenis_kelamin_id']);
		}
		
		if($array['kewarganegaraan_id']!=null)
		{
			$criteria->addCondition('kewarganegaraan_id='.$array['kewarganegaraan_id']);
			$criteriaKurang->addCondition('kewarganegaraan_id='.$array['kewarganegaraan_id']);
		}
		
		$tambah = Penduduk::model()->count($criteria);
		$kurang = Penduduk::model()->count($criteriaKurang);
		
		return intval($tambah)-intval($kurang);
		
	}
	
	public function countMutasi($array)
	{
		$criteria = new CDbCriteria;
		
		if($array['mutasi_tambah_id']!=null)
		{
			$criteria->condition = 'mutasi_tambah_tanggal >= :tanggal_awal AND mutasi_tambah_tanggal <= :tanggal_akhir';
			$criteria->params = array(':tanggal_awal'=>$array['tanggal_awal'],':tanggal_akhir'=>$array['tanggal_akhir']);
			
			$criteria->addCondition('mutasi_tambah_id='.$array['mutasi_tambah_id']);
		}
		
		if($array['mutasi_kurang_id']!=null)
		{
			$criteria->condition = 'mutasi_kurang_tanggal >= :tanggal_awal AND mutasi_kurang_tanggal <= :tanggal_akhir';
			$criteria->params = array(':tanggal_awal'=>$array['tanggal_awal'],':tanggal_akhir'=>$array['tanggal_akhir']);
			
			$criteria->addCondition('mutasi_kurang_id='.$array['mutasi_kurang_id']);
		}
		
		
		
		$criteria->addCondition('dusun_id='.$this->id);
		
		if($array['jenis_kelamin_id']!=null)
			$criteria->addCondition('jenis_kelamin_id='.$array['jenis_kelamin_id']);
		
		if($array['kewarganegaraan_id']!=null)
			$criteria->addCondition('kewarganegaraan_id='.$array['kewarganegaraan_id']);
		
		return Penduduk::model()->count($criteria);
		
		
	}
	
	
	
	
}