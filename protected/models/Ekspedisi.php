<?php

/**
 * This is the model class for table "ekspedisi".
 *
 * The followings are the available columns in table 'ekspedisi':
 * @property integer $id
 * @property string $tanggal_pengiriman
 * @property string $nomor_surat
 * @property string $tanggal_surat
 * @property string $isi_singkat
 * @property string $tujuan_surat
 * @property string $keterangan
 */
class Ekspedisi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ekspedisi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ekspedisi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tanggal_pengiriman', 'required'),
			array('nomor_surat, isi_singkat, tujuan_surat, keterangan', 'length', 'max'=>255),
			array('tanggal_surat', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tanggal_pengiriman, nomor_surat, tanggal_surat, isi_singkat, tujuan_surat, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal_pengiriman' => 'Tanggal Pengiriman',
			'nomor_surat' => 'Nomor Surat',
			'tanggal_surat' => 'Tanggal Surat',
			'isi_singkat' => 'Isi Singkat Surat Yang Dikirim',
			'tujuan_surat' => 'Tujuan Surat',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal_pengiriman',$this->tanggal_pengiriman,true);
		$criteria->compare('nomor_surat',$this->nomor_surat,true);
		$criteria->compare('tanggal_surat',$this->tanggal_surat,true);
		$criteria->compare('isi_singkat',$this->isi_singkat,true);
		$criteria->compare('tujuan_surat',$this->tujuan_surat,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}