<?php

/**
 * This is the model class for table "inventaris_desa".
 *
 * The followings are the available columns in table 'inventaris_desa':
 * @property integer $id
 * @property string $jenis_barang
 * @property string $dibeli_sendiri
 * @property string $bantuan_pemerintah
 * @property string $bantuan_provinsi
 * @property string $bantuan_kabkota
 * @property string $sumbangan
 * @property string $keadaan_awal_baik
 * @property string $keadaan_awal_rusak
 * @property string $penghapusan_rusak
 * @property string $penghapusan_dijual
 * @property string $penghapusan_disumbangkan
 * @property string $tanggal_penghapusan
 * @property string $keadaan_akhir_baik
 * @property string $keadaan_akhir_rusak
 * @property string $keterangan
 */
class InventarisDesa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InventarisDesa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventaris_desa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_barang', 'required'),
			array('jenis_barang, dibeli_sendiri, bantuan_pemerintah, bantuan_provinsi, bantuan_kabkota, sumbangan, keadaan_awal_baik, keadaan_awal_rusak, penghapusan_rusak, penghapusan_dijual, penghapusan_disumbangkan, keadaan_akhir_baik, keadaan_akhir_rusak, keterangan', 'length', 'max'=>255),
			array('tanggal_penghapusan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jenis_barang, dibeli_sendiri, bantuan_pemerintah, bantuan_provinsi, bantuan_kabkota, sumbangan, keadaan_awal_baik, keadaan_awal_rusak, penghapusan_rusak, penghapusan_dijual, penghapusan_disumbangkan, tanggal_penghapusan, keadaan_akhir_baik, keadaan_akhir_rusak, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jenis_barang' => 'Jenis Barang',
			'dibeli_sendiri' => 'Dibeli Sendiri',
			'bantuan_pemerintah' => 'Bantuan Pemerintah',
			'bantuan_provinsi' => 'Bantuan Provinsi',
			'bantuan_kabkota' => 'Bantuan Kab/kota',
			'sumbangan' => 'Sumbangan',
			'keadaan_awal_baik' => 'Baik',
			'keadaan_awal_rusak' => 'Rusak',
			'penghapusan_rusak' => 'Rusak',
			'penghapusan_dijual' => 'Dijual',
			'penghapusan_disumbangkan' => 'Disumbangkan',
			'tanggal_penghapusan' => 'Tanggal Penghapusan',
			'keadaan_akhir_baik' => 'Baik',
			'keadaan_akhir_rusak' => 'Rusak',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jenis_barang',$this->jenis_barang,true);
		$criteria->compare('dibeli_sendiri',$this->dibeli_sendiri,true);
		$criteria->compare('bantuan_pemerintah',$this->bantuan_pemerintah,true);
		$criteria->compare('bantuan_provinsi',$this->bantuan_provinsi,true);
		$criteria->compare('bantuan_kabkota',$this->bantuan_kabkota,true);
		$criteria->compare('sumbangan',$this->sumbangan,true);
		$criteria->compare('keadaan_awal_baik',$this->keadaan_awal_baik,true);
		$criteria->compare('keadaan_awal_rusak',$this->keadaan_awal_rusak,true);
		$criteria->compare('penghapusan_rusak',$this->penghapusan_rusak,true);
		$criteria->compare('penghapusan_dijual',$this->penghapusan_dijual,true);
		$criteria->compare('penghapusan_disumbangkan',$this->penghapusan_disumbangkan,true);
		$criteria->compare('tanggal_penghapusan',$this->tanggal_penghapusan,true);
		$criteria->compare('keadaan_akhir_baik',$this->keadaan_akhir_baik,true);
		$criteria->compare('keadaan_akhir_rusak',$this->keadaan_akhir_rusak,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}