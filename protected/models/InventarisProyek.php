<?php

/**
 * This is the model class for table "inventaris_proyek".
 *
 * The followings are the available columns in table 'inventaris_proyek':
 * @property integer $id
 * @property string $nama_proyek
 * @property string $volume
 * @property string $biaya
 * @property string $lokasi
 * @property string $keterangan
 * @property string $tahun
 */
class InventarisProyek extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InventarisProyek the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventaris_proyek';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_proyek', 'required'),
			array('nama_proyek, lokasi,volume', 'length', 'max'=>255),
			array('biaya', 'length', 'max'=>15),
			array('tahun', 'length', 'max'=>4),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_proyek, volume, biaya, lokasi, keterangan, tahun', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_proyek' => 'Jenis / Nama Proyek',
			'volume' => 'Volume',
			'biaya' => 'Biaya',
			'lokasi' => 'Lokasi',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_proyek',$this->nama_proyek,true);
		$criteria->compare('volume',$this->volume,true);
		$criteria->compare('biaya',$this->biaya,true);
		$criteria->compare('lokasi',$this->lokasi,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}