<?php

/**
 * This is the model class for table "kader_pembangunan".
 *
 * The followings are the available columns in table 'kader_pembangunan':
 * @property integer $id
 * @property string $nama
 * @property integer $umur
 * @property integer $jenis_kelamin_id
 * @property string $pendidikan
 * @property string $bidang
 * @property string $alamat
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property JenisKelamin $jenisKelamin
 */
class KaderPembangunan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KaderPembangunan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kader_pembangunan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('umur, jenis_kelamin_id', 'numerical', 'integerOnly'=>true),
			array('nama, pendidikan, bidang', 'length', 'max'=>255),
			array('alamat, keterangan, umur, jenis_kelamin_id, pendidikan, bidang', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, umur, jenis_kelamin_id, pendidikan, bidang, alamat, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisKelamin' => array(self::BELONGS_TO, 'JenisKelamin', 'jenis_kelamin_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'umur' => 'Umur',
			'jenis_kelamin_id' => 'Jenis Kelamin',
			'pendidikan' => 'Pendidikan / Kursus',
			'bidang' => 'Bidang',
			'alamat' => 'Alamat',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('umur',$this->umur);
		$criteria->compare('jenis_kelamin_id',$this->jenis_kelamin_id);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('bidang',$this->bidang,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}