<?php

/**
 * This is the model class for table "kegiatan_bpd".
 *
 * The followings are the available columns in table 'kegiatan_bpd':
 * @property integer $id
 * @property string $tentang
 * @property string $pelaksana
 * @property string $pokok_kegiatan
 * @property string $hasil_kegiatan
 * @property string $keterangan
 * @property string $tahun
 */
class KegiatanBpd extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KegiatanBpd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kegiatan_bpd';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tentang', 'required'),
			array('tentang, pelaksana, pokok_kegiatan, hasil_kegiatan, keterangan', 'length', 'max'=>255),
			array('tahun', 'length', 'max'=>4),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tentang, pelaksana, pokok_kegiatan, hasil_kegiatan, keterangan, tahun', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tentang' => 'Tentang',
			'pelaksana' => 'Pelaksana',
			'pokok_kegiatan' => 'Pokok Kegiatan',
			'hasil_kegiatan' => 'Hasil Kegiatan',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tentang',$this->tentang,true);
		$criteria->compare('pelaksana',$this->pelaksana,true);
		$criteria->compare('pokok_kegiatan',$this->pokok_kegiatan,true);
		$criteria->compare('hasil_kegiatan',$this->hasil_kegiatan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}