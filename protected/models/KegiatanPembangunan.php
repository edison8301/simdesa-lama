<?php

/**
 * This is the model class for table "kegiatan_pembangunan".
 *
 * The followings are the available columns in table 'kegiatan_pembangunan':
 * @property integer $id
 * @property string $nama_proyek
 * @property string $volume
 * @property string $dana_pemerintah
 * @property string $dana_provinsi
 * @property string $dana_kabkota
 * @property string $dana_swadaya
 * @property string $jumlah
 * @property string $waktu
 * @property string $baru
 * @property string $lanjutan
 * @property string $pelaksana
 * @property string $keterangan
 * @property string $tahun
 */
class KegiatanPembangunan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KegiatanPembangunan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kegiatan_pembangunan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_proyek', 'required'),
			array('nama_proyek, waktu, baru, lanjutan, pelaksana', 'length', 'max'=>255),
			array('volume, dana_pemerintah, dana_provinsi, dana_kabkota, dana_swadaya, jumlah', 'length', 'max'=>255),
			array('tahun', 'length', 'max'=>4),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_proyek, volume, dana_pemerintah, dana_provinsi, dana_kabkota, dana_swadaya, jumlah, waktu, baru, lanjutan, pelaksana, keterangan, tahun', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_proyek' => 'Nama Proyek',
			'volume' => 'Volume',
			'dana_pemerintah' => 'Pemerintah',
			'dana_provinsi' => 'Provinsi',
			'dana_kabkota' => 'Kab/kota',
			'dana_swadaya' => 'Swadaya',
			'jumlah' => 'Jumlah',
			'waktu' => 'Waktu',
			'baru' => 'Baru',
			'lanjutan' => 'Lanjutan',
			'pelaksana' => 'Pelaksana',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_proyek',$this->nama_proyek,true);
		$criteria->compare('volume',$this->volume,true);
		$criteria->compare('dana_pemerintah',$this->dana_pemerintah,true);
		$criteria->compare('dana_provinsi',$this->dana_provinsi,true);
		$criteria->compare('dana_kabkota',$this->dana_kabkota,true);
		$criteria->compare('dana_swadaya',$this->dana_swadaya,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('waktu',$this->waktu,true);
		$criteria->compare('baru',$this->baru,true);
		$criteria->compare('lanjutan',$this->lanjutan,true);
		$criteria->compare('pelaksana',$this->pelaksana,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}