<?php

/**
 * This is the model class for table "keputusan_kades".
 *
 * The followings are the available columns in table 'keputusan_kades':
 * @property integer $id
 * @property string $nomor_keputusan
 * @property string $tanggal_keputusan
 * @property string $tentang
 * @property string $uraian_singkat
 * @property string $nomor_dilaporkan
 * @property string $tanggal_dilaporkan
 * @property string $keterangan
 */
class KeputusanKades extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KeputusanKades the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'keputusan_kades';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nomor_keputusan', 'required'),
			array('nomor_keputusan, tentang, uraian_singkat, nomor_dilaporkan, keterangan', 'length', 'max'=>255),
			array('tanggal_keputusan, tanggal_dilaporkan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nomor_keputusan, tanggal_keputusan, tentang, uraian_singkat, nomor_dilaporkan, tanggal_dilaporkan, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor_keputusan' => 'Nomor Keputusan',
			'tanggal_keputusan' => 'Tanggal Keputusan',
			'tentang' => 'Tentang',
			'uraian_singkat' => 'Uraian Singkat',
			'nomor_dilaporkan' => 'Nomor Dilaporkan',
			'tanggal_dilaporkan' => 'Tanggal Dilaporkan',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor_keputusan',$this->nomor_keputusan,true);
		$criteria->compare('tanggal_keputusan',$this->tanggal_keputusan,true);
		$criteria->compare('tentang',$this->tentang,true);
		$criteria->compare('uraian_singkat',$this->uraian_singkat,true);
		$criteria->compare('nomor_dilaporkan',$this->nomor_dilaporkan,true);
		$criteria->compare('tanggal_dilaporkan',$this->tanggal_dilaporkan,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}