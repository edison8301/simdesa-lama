<?php

/**
 * This is the model class for table "mutasi_penduduk".
 *
 * The followings are the available columns in table 'mutasi_penduduk':
 * @property integer $id
 * @property string $nama
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $jenis_kelamin_id
 * @property string $kewarganegaraan
 * @property string $datang
 * @property string $datang_tanggal
 * @property string $lahir
 * @property string $lahir_tanggal
 * @property string $pindah
 * @property string $pindah_tanggal
 * @property string $mati
 * @property string $mati_tanggal
 * @property string $keterangan
 */
class MutasiPenduduk extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MutasiPenduduk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mutasi_penduduk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('jenis_kelamin_id, dusun_id, kewarganegaraan_id', 'numerical', 'integerOnly'=>true),
			array('nama, tempat_lahir, datang, lahir, pindah, mati, keterangan', 'length', 'max'=>255),
			array('tanggal_lahir, datang_tanggal, lahir_tanggal, pindah_tanggal, mati_tanggal', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, tempat_lahir, tanggal_lahir, jenis_kelamin_id, kewarganegaraan, datang, datang_tanggal, lahir, lahir_tanggal, pindah, pindah_tanggal, mati, mati_tanggal, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisKelamin'=>array(self::BELONGS_TO,'JenisKelamin','jenis_kelamin_id'),
			'dusun'=>array(self::BELONGS_TO,'Dusun','dusun_id'),
			'kewarganegaraan'=>array(self::BELONGS_TO,'Kewarganegaraan','kewarganegaraan_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'jenis_kelamin_id' => 'Jenis Kelamin',
			'kewarganegaraan_id' => 'Kewarganegaraan',
			'datang' => 'Datang',
			'datang_tanggal' => 'Datang Tanggal',
			'lahir' => 'Lahir',
			'dusun_id'=>'Dusun',
			'lahir_tanggal' => 'Lahir Tanggal',
			'pindah' => 'Pindah',
			'pindah_tanggal' => 'Pindah Tanggal',
			'mati' => 'Mati',
			'mati_tanggal' => 'Mati Tanggal',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('jenis_kelamin_id',$this->jenis_kelamin_id);
		$criteria->compare('dusun_id',$this->dusun_id);
		$criteria->compare('kewarganegaraan_id',$this->kewarganegaraan_id);
		$criteria->compare('datang',$this->datang,true);
		$criteria->compare('datang_tanggal',$this->datang_tanggal,true);
		$criteria->compare('lahir',$this->lahir,true);
		$criteria->compare('lahir_tanggal',$this->lahir_tanggal,true);
		$criteria->compare('pindah',$this->pindah,true);
		$criteria->compare('pindah_tanggal',$this->pindah_tanggal,true);
		$criteria->compare('mati',$this->mati,true);
		$criteria->compare('mati_tanggal',$this->mati_tanggal,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}