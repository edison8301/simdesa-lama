<?php

/**
 * This is the model class for table "penduduk".
 *
 * The followings are the available columns in table 'penduduk':
 * @property integer $id
 * @property string $nama
 * @property integer $jenis_kelamin_id
 * @property integer $status_perkawinan_id
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $agama_id
 * @property integer $pendidikan_id
 * @property integer $pekerjaan_id
 * @property integer $dapat_membaca
 * @property integer $kewarganegaraan_id
 * @property string $alamat
 * @property integer $kedudukan_dalam_keluarga_id
 * @property string $nomor_ktp
 * @property string $nomor_ksk
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property KedudukanDalamKeluarga $kedudukanDalamKeluarga
 * @property JenisKelamin $jenisKelamin
 * @property Agama $agama
 * @property Kewarganegaraan $kewarganegaraan
 * @property Pekerjaan $pekerjaan
 * @property Pendidikan $pendidikan
 * @property StatusPerkawinan $statusPerkawinan
 */
class Penduduk extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Penduduk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penduduk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('jenis_kelamin_id, mutasi_tambah_id, mutasi_kurang_id, status_perkawinan_id, dusun_id, agama_id, pendidikan_id, pekerjaan_id, dapat_membaca, kewarganegaraan_id, kedudukan_dalam_keluarga_id', 'numerical', 'integerOnly'=>true),
			array('nama, tempat_lahir, nomor_ktp, mutasi_tambah_keterangan, mutasi_kurang_keterangan, nomor_ksk', 'length', 'max'=>255),
			array('tanggal_lahir, alamat, keterangan, mutasi_tambah_tanggal, mutasi_kurang_tanggal, rw, rt', 'safe'),
			array('foto','safe'),
			//array('foto','file','types'=>'jpg,png,gif','maxSize'=>1024*150,'tooLarge'=>'Ukuran file terlalu besar, silahkan resize terlebih dahulu'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, jenis_kelamin_id, status_perkawinan_id, mutasi_tambah_id, mutasi_kurang_id, tempat_lahir, tanggal_lahir, agama_id, pendidikan_id, pekerjaan_id, dapat_membaca, kewarganegaraan_id, alamat, kedudukan_dalam_keluarga_id, nomor_ktp, nomor_ksk, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kedudukanDalamKeluarga' => array(self::BELONGS_TO, 'KedudukanDalamKeluarga', 'kedudukan_dalam_keluarga_id'),
			'jenisKelamin' => array(self::BELONGS_TO, 'JenisKelamin', 'jenis_kelamin_id'),
			'agama' => array(self::BELONGS_TO, 'Agama', 'agama_id'),
			'kewarganegaraan' => array(self::BELONGS_TO, 'Kewarganegaraan', 'kewarganegaraan_id'),
			'pekerjaan' => array(self::BELONGS_TO, 'Pekerjaan', 'pekerjaan_id'),
			'pendidikan' => array(self::BELONGS_TO, 'Pendidikan', 'pendidikan_id'),
			'mutasiTambah' => array(self::BELONGS_TO, 'MutasiTambah', 'mutasi_tambah_id'),
			'mutasiKurang' => array(self::BELONGS_TO, 'MutasiKurang', 'mutasi_kurang_id'),
			'dusun' => array(self::BELONGS_TO, 'Dusun', 'dusun_id'),
			'statusPerkawinan' => array(self::BELONGS_TO, 'StatusPerkawinan', 'status_perkawinan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama Lengkap / Panggilan',
			'jenis_kelamin_id' => 'Jenis Kelamin',
			'status_perkawinan_id' => 'Status Perkawinan',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'mutasi_tambah_id'=>'Jenis Mutasi Penambahan',
			'mutasi_tambah_tanggal'=>'Tanggal Datang',
			'mutasi_tambah_keterangan'=>'Datang Dari',
			'mutasi_kurang_id'=>'Jenis Mutasi Pengurangan',
			'mutasi_kurang_tanggal'=>'Tanggal Mati / Pergi',
			'mutasi_kurang_keterangan'=>'Pindah Ke',
			'agama_id' => 'Agama',
			'dusun_id' => 'Dusun',
			'rw' => 'RW',
			'rt' => 'RT',
			'pendidikan_id' => 'Pendidikan Terakhir',
			'pekerjaan_id' => 'Pekerjaan',
			'dapat_membaca' => 'Dapat Membaca',
			'kewarganegaraan_id' => 'Kewarganegaraan',
			'alamat' => 'Alamat',
			'kedudukan_dalam_keluarga_id' => 'Kedudukan Dalam Keluarga',
			'nomor_ktp' => 'Nomor KTP',
			'nomor_ksk' => 'Nomor KSK',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jenis_kelamin_id',$this->jenis_kelamin_id);
		$criteria->compare('status_perkawinan_id',$this->status_perkawinan_id);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('agama_id',$this->agama_id);
		$criteria->compare('dusun_id',$this->dusun_id);
		$criteria->compare('rw',$this->rw);
		$criteria->compare('rt',$this->rt);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('pekerjaan_id',$this->pekerjaan_id);
		$criteria->compare('mutasi_tambah_id',$this->mutasi_tambah_id);
		$criteria->compare('mutasi_kurang_id',$this->mutasi_kurang_id);
		$criteria->compare('dapat_membaca',$this->dapat_membaca);
		$criteria->compare('kewarganegaraan_id',$this->kewarganegaraan_id);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kedudukan_dalam_keluarga_id',$this->kedudukan_dalam_keluarga_id);
		$criteria->compare('nomor_ktp',$this->nomor_ktp,true);
		$criteria->compare('nomor_ksk',$this->nomor_ksk,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		if(Yii::app()->controller->action->id=='usiaBalita')
		{
			$hari_ini = date('Y-m-d');
			$tahun = date('Y-m-d',strtotime(date('Y-m-d',strtotime($hari_ini))."- 5 year"));
			$criteria->condition = 'tanggal_lahir >= :tahun';
			$criteria->params = array(':tahun'=>$tahun);
		}
		
		if(Yii::app()->controller->action->id=='pemilihPemilu')
		{
			$hari_ini = date('Y-m-d');
			$tahun = date('Y-m-d',strtotime(date('Y-m-d',strtotime($hari_ini))."- 17 year"));
			$criteria->condition = 'tanggal_lahir <= :tahun';
			$criteria->params = array(':tahun'=>$tahun);
		}
		
		if(Yii::app()->controller->action->id=='wajibBelajar')
		{
			$hari_ini = date('Y-m-d');
			$tahun_awal = date('Y-m-d',strtotime(date('Y-m-d',strtotime($hari_ini))."- 7 year"));
			$tahun_akhir = date('Y-m-d',strtotime(date('Y-m-d',strtotime($hari_ini))."- 19 year"));
			$criteria->condition = 'tanggal_lahir >= :tahun_akhir AND tanggal_lahir <= :tahun_awal';
			$criteria->params = array(':tahun_akhir'=>$tahun_akhir,':tahun_awal'=>$tahun_awal);
		}
		
		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getDataPenduduk()
	{
		$criteria = new CDbCriteria;
		
		if(!empty($_POST['jenis_kelamin_id']))
			$criteria->addCondition('jenis_kelamin_id = '.$_POST['jenis_kelamin_id']);
		
		if(!empty($_POST['status_perkawinan_id']))
			$criteria->addCondition('status_perkawinan_id = '.$_POST['status_perkawinan_id']);
			
		if(!empty($_POST['agama_id']))
			$criteria->addCondition('agama_id = '.$_POST['agama_id']);
			
		if(!empty($_POST['pendidikan_id']))
			$criteria->addCondition('pendidikan_id = '.$_POST['pendidikan_id']);
			
		if(!empty($_POST['pekerjaan_id']))
			$criteria->addCondition('pekerjaan_id = '.$_POST['pekerjaan_id']);
			
		if(!empty($_POST['dusun_id']))
			$criteria->addCondition('dusun_id = '.$_POST['dusun_id']);
		
		if(!empty($_POST['rw']))
			$criteria->addCondition('rw = '.$_POST['rw']);
			
		if(!empty($_POST['rt']))
			$criteria->addCondition('rt = '.$_POST['rt']);
		
		if(!empty($_POST['kewarganegaraan_id']))
			$criteria->addCondition('kewarganegaraan_id = '.$_POST['kewarganegaraan_id']);
			
		if(!empty($_POST['kedudukan_dalam_keluarga_id']))
			$criteria->addCondition('kedudukan_dalam_keluarga_id = '.$_POST['kedudukan_dalam_keluarga_id']);
		
		
		$model = Penduduk::model()->findAll($criteria);
		
		if($model!==null)
			return $model;
		else
			return false;
		
	}
	
	public function countByIntervalUmur($umur_awal=0,$umur_akhir=0,$jenis_kelamin=null)
	{
		date_default_timezone_set('Asia/Jakarta');
		
		$hari_ini = date('Y-m-d');
		$tanggal_akhir = date('Y-m-d',strtotime(date('Y-m-d',strtotime($hari_ini))."- ".$umur_awal." year"));
		
		$tanggal_awal = date('Y-m-d',strtotime(date('Y-m-d',strtotime($hari_ini))."- ".$umur_akhir." year"));
		
		$criteria = new CDbCriteria;
		
		$criteria->condition = 'tanggal_lahir >= :tanggal_awal AND tanggal_lahir < :tanggal_akhir';
		$criteria->params = array(':tanggal_awal'=>$tanggal_awal,':tanggal_akhir'=>$tanggal_akhir);
		
		if($jenis_kelamin == 'laki-laki')
			$criteria->addCondition('jenis_kelamin_id=1');
			
		if($jenis_kelamin == 'perempuan')
			$criteria->addCondition('jenis_kelamin_id=2');
			
		return Penduduk::model()->count($criteria);
		
	}
	
	public function countByUmur($umur=0,$jenis_kelamin=null)
	{
		date_default_timezone_set('Asia/Jakarta');
		
		$hari_ini = date('Y-m-d');
		$tanggal_akhir = date('Y-m-d',strtotime(date('Y-m-d',strtotime($hari_ini))."- ".$umur." year"));
		
		if($umur==0)
			$tanggal_akhir = $hari_ini;
		
		$umur++;
		$tanggal_awal = date('Y-m-d',strtotime(date('Y-m-d',strtotime($hari_ini))."- ".$umur." year"));
		
		$criteria = new CDbCriteria;
		
		$criteria->condition = 'tanggal_lahir >= :tanggal_awal AND tanggal_lahir < :tanggal_akhir';
		$criteria->params = array(':tanggal_awal'=>$tanggal_awal,':tanggal_akhir'=>$tanggal_akhir);
		
		if($jenis_kelamin == 'laki-laki')
			$criteria->addCondition('jenis_kelamin_id=1');
			
		if($jenis_kelamin == 'perempuan')
			$criteria->addCondition('jenis_kelamin_id=2');
			
		return Penduduk::model()->count($criteria);
	}
	
	public function getDataMutasi($jenis=null) {
		
		date_default_timezone_set('Asia/Jakarta');
		
		$tanggal_awal = date('Y-m').'-01';
		$tanggal_akhir = date('Y-m').'-'.date('t');
		
		if(!empty($_POST['tanggal_awal']) AND !empty($_POST['tanggal_akhir']))
		{
			$tanggal_awal = $_POST['tanggal_awal'];
			$tanggal_akhir = $_POST['tanggal_akhir'];
		}
		
		if(!empty($_GET['tanggal_awal']) AND !empty($_GET['tanggal_akhir']))
		{
			$tanggal_awal = $_GET['tanggal_awal'];
			$tanggal_akhir = $_GET['tanggal_akhir'];
		}
		
		$criteria = new CDbCriteria;
		
		$criteria->condition = '(mutasi_tambah_tanggal >= :tanggal_awal AND mutasi_tambah_tanggal <= :tanggal_akhir) OR (mutasi_kurang_tanggal >= :tanggal_awal AND mutasi_kurang_tanggal <= :tanggal_akhir AND mutasi_kurang_id <> 0)';
	
		$criteria->params = array(':tanggal_awal'=>$tanggal_awal,':tanggal_akhir'=>$tanggal_akhir);
		
		$model = Penduduk::model()->findAll($criteria);
		
		return $model;
	}
}