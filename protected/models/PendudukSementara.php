<?php

/**
 * This is the model class for table "penduduk_sementara".
 *
 * The followings are the available columns in table 'penduduk_sementara':
 * @property integer $id
 * @property string $nama
 * @property integer $jenis_kelamin_id
 * @property string $nomor_identitas
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $pekerjaan
 * @property string $kebangsaan
 * @property string $keturunan
 * @property string $datang_dari
 * @property string $maksud_kedatangan
 * @property string $nama_alamat_tujuan
 * @property string $datang_tanggal
 * @property string $pergi_tanggal
 * @property string $keterangan
 */
class PendudukSementara extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PendudukSementara the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penduduk_sementara';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('jenis_kelamin_id', 'numerical', 'integerOnly'=>true),
			array('nama, nomor_identitas, tempat_lahir, pekerjaan, kebangsaan, keturunan, datang_dari, maksud_kedatangan, nama_alamat_tujuan, keterangan', 'length', 'max'=>255),
			array('tanggal_lahir, datang_tanggal, pergi_tanggal', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, jenis_kelamin_id, nomor_identitas, tempat_lahir, tanggal_lahir, pekerjaan, kebangsaan, keturunan, datang_dari, maksud_kedatangan, nama_alamat_tujuan, datang_tanggal, pergi_tanggal, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisKelamin'=>array(self::BELONGS_TO,'JenisKelamin','jenis_kelamin_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'jenis_kelamin_id' => 'Jenis Kelamin',
			'nomor_identitas' => 'Nomor Identitas',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'pekerjaan' => 'Pekerjaan',
			'kebangsaan' => 'Kebangsaan',
			'keturunan' => 'Keturunan',
			'datang_dari' => 'Datang Dari',
			'maksud_kedatangan' => 'Maksud Kedatangan',
			'nama_alamat_tujuan' => 'Nama Alamat Tujuan',
			'datang_tanggal' => 'Datang Tanggal',
			'pergi_tanggal' => 'Pergi Tanggal',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jenis_kelamin_id',$this->jenis_kelamin_id);
		$criteria->compare('nomor_identitas',$this->nomor_identitas,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('kebangsaan',$this->kebangsaan,true);
		$criteria->compare('keturunan',$this->keturunan,true);
		$criteria->compare('datang_dari',$this->datang_dari,true);
		$criteria->compare('maksud_kedatangan',$this->maksud_kedatangan,true);
		$criteria->compare('nama_alamat_tujuan',$this->nama_alamat_tujuan,true);
		$criteria->compare('datang_tanggal',$this->datang_tanggal,true);
		$criteria->compare('pergi_tanggal',$this->pergi_tanggal,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}