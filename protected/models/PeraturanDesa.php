<?php

/**
 * This is the model class for table "peraturan_desa".
 *
 * The followings are the available columns in table 'peraturan_desa':
 * @property integer $id
 * @property string $nomor
 * @property string $tanggal
 * @property string $tentang
 * @property string $uraian_singkat
 * @property string $nomor_persetujuan
 * @property string $tanggal_persetujuan
 * @property string $nomor_dilaporkan
 * @property string $tanggal_dilaporkan
 * @property string $keterangan
 */
class PeraturanDesa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PeraturanDesa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peraturan_desa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nomor', 'required'),
			array('nomor, tentang, uraian_singkat, nomor_persetujuan, nomor_dilaporkan, keterangan', 'length', 'max'=>255),
			array('tanggal, tanggal_persetujuan, tanggal_dilaporkan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nomor, tanggal, tentang, uraian_singkat, nomor_persetujuan, tanggal_persetujuan, nomor_dilaporkan, tanggal_dilaporkan, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor' => 'Nomor',
			'tanggal' => 'Tanggal',
			'tentang' => 'Tentang',
			'uraian_singkat' => 'Uraian Singkat',
			'nomor_persetujuan' => 'Nomor Persetujuan',
			'tanggal_persetujuan' => 'Tanggal Persetujuan',
			'nomor_dilaporkan' => 'Nomor Dilaporkan',
			'tanggal_dilaporkan' => 'Tanggal Dilaporkan',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('tentang',$this->tentang,true);
		$criteria->compare('uraian_singkat',$this->uraian_singkat,true);
		$criteria->compare('nomor_persetujuan',$this->nomor_persetujuan,true);
		$criteria->compare('tanggal_persetujuan',$this->tanggal_persetujuan,true);
		$criteria->compare('nomor_dilaporkan',$this->nomor_dilaporkan,true);
		$criteria->compare('tanggal_dilaporkan',$this->tanggal_dilaporkan,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}