<?php

/**
 * This is the model class for table "rencana_pembangunan".
 *
 * The followings are the available columns in table 'rencana_pembangunan':
 * @property integer $id
 * @property string $nama_proyek
 * @property string $lokasi
 * @property string $dana_pemerintah
 * @property string $dana_provinsi
 * @property string $dana_kabkota
 * @property string $dana_swadaya
 * @property string $jumlah
 * @property string $pelaksana
 * @property string $manfaat
 * @property string $keterangan
 * @property string $tahun
 */
class RencanaPembangunan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RencanaPembangunan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rencana_pembangunan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_proyek', 'required'),
			array('nama_proyek, lokasi, pelaksana, manfaat', 'length', 'max'=>255),
			array('dana_pemerintah, dana_provinsi, dana_kabkota, dana_swadaya, jumlah', 'length', 'max'=>255),
			array('tahun', 'length', 'max'=>4),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_proyek, lokasi, dana_pemerintah, dana_provinsi, dana_kabkota, dana_swadaya, jumlah, pelaksana, manfaat, keterangan, tahun', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_proyek' => 'Nama Proyek / Kegiatan',
			'lokasi' => 'Lokasi',
			'dana_pemerintah' => 'Pemerintah',
			'dana_provinsi' => 'Provinsi',
			'dana_kabkota' => 'Kab/kota',
			'dana_swadaya' => 'Swadaya',
			'jumlah' => 'Jumlah',
			'pelaksana' => 'Pelaksana',
			'manfaat' => 'Manfaat',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_proyek',$this->nama_proyek,true);
		$criteria->compare('lokasi',$this->lokasi,true);
		$criteria->compare('dana_pemerintah',$this->dana_pemerintah,true);
		$criteria->compare('dana_provinsi',$this->dana_provinsi,true);
		$criteria->compare('dana_kabkota',$this->dana_kabkota,true);
		$criteria->compare('dana_swadaya',$this->dana_swadaya,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('pelaksana',$this->pelaksana,true);
		$criteria->compare('manfaat',$this->manfaat,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}