<?php

/**
 * This is the model class for table "surat".
 *
 * The followings are the available columns in table 'surat':
 * @property integer $id
 * @property integer $nomor
 * @property integer $penduduk_id
 * @property integer $surat_template_id
 */
class Surat extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Surat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'surat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('surat_template_id', 'required'),
			array('penduduk_id, surat_template_id', 'numerical', 'integerOnly'=>true),
			array('nomor, ','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nomor, penduduk_id, surat_template_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'surat_template'=>array(self::BELONGS_TO,'SuratTemplate','surat_template_id'),
			'penduduk'=>array(self::BELONGS_TO,'Penduduk','penduduk_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor' => 'Nomor',
			'penduduk_id' => 'Penduduk',
			'surat_template_id' => 'Surat Template',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor',$this->nomor);
		$criteria->compare('penduduk_id',$this->penduduk_id);
		$criteria->compare('surat_template_id',$this->surat_template_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getDataSuratAtribut()
	{
		$model = SuratAtribut::model()->findAllByAttributes(array('surat_id'=>$this->id));
		if($model!==null)
			return $model;
		else
			return false;
	}	
	
	public function getSuratAtributValueByKey($key)
	{
		$model = SuratAtribut::model()->findByAttributes(array('surat_id'=>$this->id,'key'=>$key));
		if($model===null)
		{
			$model = new SuratAtribut;
			$model->surat_id = $this->id;
			$model->key = $key;
			$model->value = '';
			$model->save();
		}
		
		return $model->value;
	}
	
	public function saveSuratAtribut($array)
	{
		foreach($array as $key => $value)
		{
			$this->saveSuratAtributByKey($key,$value);
		}
	}
	
	public function saveSuratAtributByKey($key,$value)
	{
		$model = SuratAtribut::model()->findByAttributes(array('key'=>$key,'surat_id'=>$this->id));
		$model->value = $value;
		$model->save();
	}
}