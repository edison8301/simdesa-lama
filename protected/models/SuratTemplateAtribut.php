<?php

/**
 * This is the model class for table "surat_template_atribut".
 *
 * The followings are the available columns in table 'surat_template_atribut':
 * @property integer $id
 * @property integer $surat_template_id
 * @property string $key
 * @property string $tipe
 */
class SuratTemplateAtribut extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SuratTemplateAtribut the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'surat_template_atribut';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('surat_template_id, key', 'required'),
			array('surat_template_id', 'numerical', 'integerOnly'=>true),
			array('key, tipe', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, surat_template_id, key, tipe', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'surat_template_id' => 'Surat Template',
			'key' => 'Key',
			'tipe' => 'Tipe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('surat_template_id',$this->surat_template_id);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('tipe',$this->tipe,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}