<?php

/**
 * This is the model class for table "tanah".
 *
 * The followings are the available columns in table 'tanah':
 * @property integer $id
 * @property string $nama
 * @property string $jumlah
 * @property string $hm
 * @property string $hgb
 * @property string $hp
 * @property string $hgu
 * @property string $hpl
 * @property string $ma
 * @property string $vi
 * @property string $tn
 * @property string $perumahan
 * @property string $perdagangan
 * @property string $perkantoran
 * @property string $industri
 * @property string $fasilitas_umum
 * @property string $sawah
 * @property string $tegalan
 * @property string $perkebunan
 * @property string $peternakan
 * @property string $hutan_belukar
 * @property string $hutan_lindung
 * @property string $tanah_kosong
 * @property string $lain_lain
 */
class Tanah extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tanah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tanah';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('nama', 'length', 'max'=>255),
			array('jumlah, hm, hgb, hp, hgu, hpl, ma, vi, tn, perumahan, perdagangan, perkantoran, industri, fasilitas_umum, sawah, tegalan, perkebunan, peternakan, hutan_belukar, hutan_lindung, tanah_kosong, lain_lain', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, jumlah, hm, hgb, hp, hgu, hpl, ma, vi, tn, perumahan, perdagangan, perkantoran, industri, fasilitas_umum, sawah, tegalan, perkebunan, peternakan, hutan_belukar, hutan_lindung, tanah_kosong, lain_lain', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'jumlah' => 'Jumlah',
			'hm' => 'HM',
			'hgb' => 'HGB',
			'hp' => 'HP',
			'hgu' => 'HGU',
			'hpl' => 'HPL',
			'ma' => 'MA',
			'vi' => 'VI',
			'tn' => 'TN',
			'perumahan' => 'Perumahan',
			'perdagangan' => 'Perdagangan',
			'perkantoran' => 'Perkantoran',
			'industri' => 'Industri',
			'fasilitas_umum' => 'Fasilitas Umum',
			'sawah' => 'Sawah',
			'tegalan' => 'Tegalan',
			'perkebunan' => 'Perkebunan',
			'peternakan' => 'Peternakan',
			'hutan_belukar' => 'Hutan Belukar',
			'hutan_lindung' => 'Hutan Lindung',
			'tanah_kosong' => 'Tanah Kosong',
			'lain_lain' => 'Lain Lain',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('hm',$this->hm,true);
		$criteria->compare('hgb',$this->hgb,true);
		$criteria->compare('hp',$this->hp,true);
		$criteria->compare('hgu',$this->hgu,true);
		$criteria->compare('hpl',$this->hpl,true);
		$criteria->compare('ma',$this->ma,true);
		$criteria->compare('vi',$this->vi,true);
		$criteria->compare('tn',$this->tn,true);
		$criteria->compare('perumahan',$this->perumahan,true);
		$criteria->compare('perdagangan',$this->perdagangan,true);
		$criteria->compare('perkantoran',$this->perkantoran,true);
		$criteria->compare('industri',$this->industri,true);
		$criteria->compare('fasilitas_umum',$this->fasilitas_umum,true);
		$criteria->compare('sawah',$this->sawah,true);
		$criteria->compare('tegalan',$this->tegalan,true);
		$criteria->compare('perkebunan',$this->perkebunan,true);
		$criteria->compare('peternakan',$this->peternakan,true);
		$criteria->compare('hutan_belukar',$this->hutan_belukar,true);
		$criteria->compare('hutan_lindung',$this->hutan_lindung,true);
		$criteria->compare('tanah_kosong',$this->tanah_kosong,true);
		$criteria->compare('lain_lain',$this->lain_lain,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}