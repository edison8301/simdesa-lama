<?php

/**
 * This is the model class for table "tanah_milik".
 *
 * The followings are the available columns in table 'tanah_milik':
 * @property integer $id
 * @property string $asal_tanah
 * @property string $nomor_sertifikat
 * @property string $luas
 * @property string $klas
 * @property string $asli
 * @property string $bantuan_pemerintah
 * @property string $bantuan_provinsi
 * @property string $bantuan_kabkota
 * @property string $lain_lain
 * @property string $tanggal_perolehan
 * @property string $sawah
 * @property string $tegal
 * @property string $kebun
 * @property string $tambak
 * @property string $tanah_kering
 * @property string $patok_ada
 * @property string $patok_tidak_ada
 * @property string $papan_ada
 * @property string $papan_tidak_ada
 * @property string $lokasi
 * @property string $peruntukkan
 */
class TanahMilik extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TanahMilik the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tanah_milik';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('asal_tanah', 'required'),
			array('asal_tanah, nomor_sertifikat, luas, klas, asli, bantuan_pemerintah, bantuan_provinsi, bantuan_kabkota, lain_lain, tanggal_perolehan, sawah, tegal, kebun, tambak, tanah_kering, patok_ada, patok_tidak_ada, papan_ada, papan_tidak_ada, lokasi, peruntukkan', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, asal_tanah, nomor_sertifikat, luas, klas, asli, bantuan_pemerintah, bantuan_provinsi, bantuan_kabkota, lain_lain, tanggal_perolehan, sawah, tegal, kebun, tambak, tanah_kering, patok_ada, patok_tidak_ada, papan_ada, papan_tidak_ada, lokasi, peruntukkan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'asal_tanah' => 'Asal Tanah Milik Desa / Tanah Kas Desa',
			'nomor_sertifikat' => 'Nomor Sertifikat Buku Letter C / Persil',
			'luas' => 'Luas (HA)',
			'klas' => 'Klas',
			'asli' => 'Asli',
			'bantuan_pemerintah' => 'Bantuan Pemerintah',
			'bantuan_provinsi' => 'Bantuan Provinsi',
			'bantuan_kabkota' => 'Bantuan Kab/kota',
			'lain_lain' => 'Lain Lain',
			'tanggal_perolehan' => 'Tanggal Perolehan',
			'sawah' => 'Sawah',
			'tegal' => 'Tegal',
			'kebun' => 'Kebun',
			'tambak' => 'Tambak',
			'tanah_kering' => 'Tanah Kering',
			'patok_ada' => 'Ada',
			'patok_tidak_ada' => 'Tidak Ada',
			'papan_ada' => 'Ada',
			'papan_tidak_ada' => 'Tidak Ada',
			'lokasi' => 'Lokasi',
			'peruntukkan' => 'Peruntukkan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('asal_tanah',$this->asal_tanah,true);
		$criteria->compare('nomor_sertifikat',$this->nomor_sertifikat,true);
		$criteria->compare('luas',$this->luas,true);
		$criteria->compare('klas',$this->klas,true);
		$criteria->compare('asli',$this->asli,true);
		$criteria->compare('bantuan_pemerintah',$this->bantuan_pemerintah,true);
		$criteria->compare('bantuan_provinsi',$this->bantuan_provinsi,true);
		$criteria->compare('bantuan_kabkota',$this->bantuan_kabkota,true);
		$criteria->compare('lain_lain',$this->lain_lain,true);
		$criteria->compare('tanggal_perolehan',$this->tanggal_perolehan,true);
		$criteria->compare('sawah',$this->sawah,true);
		$criteria->compare('tegal',$this->tegal,true);
		$criteria->compare('kebun',$this->kebun,true);
		$criteria->compare('tambak',$this->tambak,true);
		$criteria->compare('tanah_kering',$this->tanah_kering,true);
		$criteria->compare('patok_ada',$this->patok_ada,true);
		$criteria->compare('patok_tidak_ada',$this->patok_tidak_ada,true);
		$criteria->compare('papan_ada',$this->papan_ada,true);
		$criteria->compare('papan_tidak_ada',$this->papan_tidak_ada,true);
		$criteria->compare('lokasi',$this->lokasi,true);
		$criteria->compare('peruntukkan',$this->peruntukkan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}