<?php
$this->breadcrumbs=array(
	'Agama'=>array('admin'),
	'Kelola',
);

?>

<h1>Agama</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'agama-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
