<?php
$this->breadcrumbs=array(
	'Agamas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Agama','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Agama','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Agama','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Agama','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Agama</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
