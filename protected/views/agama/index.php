<?php
$this->breadcrumbs=array(
	'Agamas',
);

$this->menu=array(
	array('label'=>'Tambah Agama','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Agama','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Agamas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
