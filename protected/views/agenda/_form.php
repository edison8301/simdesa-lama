<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'agenda-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->datepickerRow($model,'tanggal',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	<div>&nbsp;</div>
	<legend>Surat Masuk</legend>
	<?php echo $form->textFieldRow($model,'masuk_nomor',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'masuk_tanggal',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>

	<?php print $form->error($model,'masuk_tanggal'); ?>
	
	<?php echo $form->textFieldRow($model,'masuk_pengirim',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'masuk_isi',array('class'=>'span5','maxlength'=>255)); ?>
	
	<div>&nbsp;</div>
	
	<legend>Surat Keluar</legend>
	<?php echo $form->textFieldRow($model,'keluar_isi',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keluar_nomor',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'keluar_tanggal',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	
	<?php echo $form->textFieldRow($model,'keluar_tujuan',array('class'=>'span5','maxlength'=>255)); ?>
	
	<hr>
	
	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
