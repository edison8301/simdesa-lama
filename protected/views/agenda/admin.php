<?php
$this->breadcrumbs=array(
	'Agenda'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('agenda-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Agenda</h1>


<div style="overflow:auto">
<?php $this->widget('TbGridViewPlus',array(
		'id'=>'agenda-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'addingHeaders' => array(	
			array(''=>1,'Surat Masuk'=>4,'Surat Keluar'=>4,'&nbsp;'=>1),
			
		),
		'columns'=>array(
			'tanggal',
			'masuk_nomor',
			'masuk_tanggal',
			'masuk_pengirim',
			'masuk_isi',
			'keluar_isi',
			'keluar_nomor',
			'keluar_tanggal',
			'keluar_tujuan',
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),	
		),
)); ?>

</div>
