<?php
$this->breadcrumbs=array(
	'Agendas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Agenda','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Agenda','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Agenda','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Agenda','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Agenda</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'tanggal',
		'masuk_nomor',
		'masuk_tanggal',
		'masuk_pengirim',
		'masuk_isi',
		'keluar_isi',
		'keluar_nomor',
		'keluar_tanggal',
		'keluar_tujuan',
		'keterangan',
),
)); ?>
