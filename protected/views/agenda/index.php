<?php
$this->breadcrumbs=array(
	'Agendas',
);

$this->menu=array(
	array('label'=>'Tambah Agenda','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Agenda','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Agendas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
