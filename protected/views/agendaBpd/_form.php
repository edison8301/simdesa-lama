<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'agenda-bpd-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'tanggal',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'masuk_nomor',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'masuk_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'masuk_pengirim',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'masuk_isi',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keluar_isi',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keluar_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'keluar_tujuan',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
