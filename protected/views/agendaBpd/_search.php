<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'tanggal',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'masuk_nomor',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'masuk_tanggal',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'masuk_pengirim',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'masuk_isi',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'keluar_isi',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'keluar_tanggal',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'keluar_tujuan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'icon'=>'search white',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
