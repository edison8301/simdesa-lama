<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('masuk_nomor')); ?>:</b>
	<?php echo CHtml::encode($data->masuk_nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('masuk_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->masuk_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('masuk_pengirim')); ?>:</b>
	<?php echo CHtml::encode($data->masuk_pengirim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('masuk_isi')); ?>:</b>
	<?php echo CHtml::encode($data->masuk_isi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keluar_isi')); ?>:</b>
	<?php echo CHtml::encode($data->keluar_isi); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('keluar_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->keluar_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keluar_tujuan')); ?>:</b>
	<?php echo CHtml::encode($data->keluar_tujuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	*/ ?>

</div>