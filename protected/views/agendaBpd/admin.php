<?php
$this->breadcrumbs=array(
	'Agenda BPD'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('agenda-bpd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Agenda BPD</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'agenda-bpd-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'tanggal',
			'masuk_nomor',
			'masuk_tanggal',
			'masuk_pengirim',
			'masuk_isi',
			'keluar_isi',
			'keluar_tanggal',
			'keluar_tujuan',
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
