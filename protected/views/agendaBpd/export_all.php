<?php
$this->breadcrumbs=array(
	'Agenda Bpds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah AgendaBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting AgendaBpd','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus AgendaBpd','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola AgendaBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat AgendaBpd</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'tanggal',
		'masuk_nomor',
		'masuk_tanggal',
		'masuk_pengirim',
		'masuk_isi',
		'keluar_isi',
		'keluar_tanggal',
		'keluar_tujuan',
		'keterangan',
),
)); ?>
