<?php
$this->breadcrumbs=array(
	'Agenda Bpds',
);

$this->menu=array(
	array('label'=>'Tambah AgendaBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola AgendaBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Agenda Bpds</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
