<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'anggaran-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'anggaran_jenis_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kode_1',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kode_2',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kode_3',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kode_4',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kode_5',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kode_6',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'uraian',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'jumlah',array('class'=>'span5','maxlength'=>15)); ?>

	<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
