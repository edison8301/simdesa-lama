<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anggaran_jenis_id')); ?>:</b>
	<?php echo CHtml::encode($data->anggaran_jenis_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_1')); ?>:</b>
	<?php echo CHtml::encode($data->kode_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_2')); ?>:</b>
	<?php echo CHtml::encode($data->kode_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_3')); ?>:</b>
	<?php echo CHtml::encode($data->kode_3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_4')); ?>:</b>
	<?php echo CHtml::encode($data->kode_4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_5')); ?>:</b>
	<?php echo CHtml::encode($data->kode_5); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_6')); ?>:</b>
	<?php echo CHtml::encode($data->kode_6); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uraian')); ?>:</b>
	<?php echo CHtml::encode($data->uraian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	*/ ?>

</div>