<?php
$this->breadcrumbs=array(
	'Anggarans'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('anggaran-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Anggarans</h1>

<?php echo CHtml::link('Pencarian Lanjut','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'anggaran-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
		'id',
		'anggaran_jenis_id',
		'kode_1',
		'kode_2',
		'kode_3',
		'kode_4',
		'kode_5',
		'kode_6',
		'uraian',
		'jumlah',
		'keterangan',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>

</div>
