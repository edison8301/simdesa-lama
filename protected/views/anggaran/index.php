<?php
$this->breadcrumbs=array(
	'Anggarans',
);

$this->menu=array(
	array('label'=>'Tambah Anggaran','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Anggaran','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Anggarans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
