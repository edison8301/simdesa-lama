<?php
$this->breadcrumbs=array(
	'Anggarans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Anggaran','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Anggaran','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Anggaran','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Anggaran','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Anggaran</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'anggaran_jenis_id',
		'kode_1',
		'kode_2',
		'kode_3',
		'kode_4',
		'kode_5',
		'kode_6',
		'uraian',
		'jumlah',
		'keterangan',
),
)); ?>
