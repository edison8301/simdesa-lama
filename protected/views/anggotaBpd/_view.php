<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin_id')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agama_id')); ?>:</b>
	<?php echo CHtml::encode($data->agama_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jabatan')); ?>:</b>
	<?php echo CHtml::encode($data->jabatan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pendidikan_id')); ?>:</b>
	<?php echo CHtml::encode($data->pendidikan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_pengangkatan')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_pengangkatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_pengangkatan')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_pengangkatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_pemberhentian')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_pemberhentian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_pemberhentian')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_pemberhentian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	*/ ?>

</div>