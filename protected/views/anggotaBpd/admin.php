<?php
$this->breadcrumbs=array(
	'Anggota BPD'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('anggota-bpd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Anggota BPD</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'anggota-bpd-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'CDataColumn',
				'name'=>'jenis_kelamin_id',
				'header'=>'Jenis Kelamin',
				'type'=>'raw',
				'value'=>'$data->jenisKelamin->nama',
				'filter'=>CHtml::listData(JenisKelamin::model()->findAll(),'id','nama')
			),
			'tempat_lahir',
			'tanggal_lahir',
			array(
				'class'=>'CDataColumn',
				'name'=>'agama_id',
				'header'=>'Agama',
				'type'=>'raw',
				'value'=>'$data->agama->nama',
				'filter'=>CHtml::listData(Agama::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'pendidikan_id',
				'header'=>'Pendidikan',
				'type'=>'raw',
				'value'=>'$data->pendidikan->nama',
				'filter'=>CHtml::listData(Pendidikan::model()->findAll(),'id','nama')
			),
			'tanggal_pengangkatan',
			'nomor_pengangkatan',
			'tanggal_pemberhentian',
			'nomor_pemberhentian',
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
