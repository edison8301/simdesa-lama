<?php
$this->breadcrumbs=array(
	'Anggota Bpds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah AnggotaBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting AnggotaBpd','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus AnggotaBpd','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola AnggotaBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat AnggotaBpd</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
		'jenis_kelamin_id',
		'tempat_lahir',
		'tanggal_lahir',
		'agama_id',
		'jabatan',
		'pendidikan_id',
		'tanggal_pengangkatan',
		'nomor_pengangkatan',
		'tanggal_pemberhentian',
		'nomor_pemberhentian',
		'keterangan',
),
)); ?>
