<?php
$this->breadcrumbs=array(
	'Anggota Bpds',
);

$this->menu=array(
	array('label'=>'Tambah AnggotaBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola AnggotaBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Anggota Bpds</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
