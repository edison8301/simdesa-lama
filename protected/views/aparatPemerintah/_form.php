<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'aparat-pemerintah-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'niap',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'nip',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'jenis_kelamin_id',CHtml::listData(JenisKelamin::model()->findAll(),'id','nama'),array('empty'=>'-- Pilih Jenis Kelamin --')); ?>

	<?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'tanggal_lahir',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	
	<?php echo $form->dropDownListRow($model,'agama_id',CHtml::listData(Agama::model()->findAll(),'id','nama'),array('empty'=>'-- Pilih Agama --')); ?>
	
	<?php echo $form->dropDownListRow($model,'pangkat_golongan_id',CHtml::listData(PangkatGolongan::model()->findAll(),'id','nama'),array('empty'=>'-- Pilih Pangkat Golongan --')); ?>

	<?php echo $form->textFieldRow($model,'jabatan',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'pendidikan_id',CHtml::listData(Pendidikan::model()->findAll(),'id','nama'),array('empty'=>'-- Pilih Pendidikan --')); ?>
	
	<?php echo $form->textFieldRow($model,'nomor_pengangkatan',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'tanggal_pengangkatan',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
