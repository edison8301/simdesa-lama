<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'niap',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'nip',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'jenis_kelamin_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tanggal_lahir',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'agama_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'pangkat_golongan_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'jabatan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'pendidikan_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nomor_pengangkatan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tanggal_pengangkatan',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'icon'=>'search white',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
