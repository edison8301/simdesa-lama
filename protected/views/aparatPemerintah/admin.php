<?php
$this->breadcrumbs=array(
	'Aparat Pemerintahs'=>array('admin'),
	'Kelola',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('aparat-pemerintah-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Aparat Pemerintah Desa</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'aparat-pemerintah-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'name'=>'nama',
				'headerHtmlOptions'=>array('style'=>'min-width:200px')
			),
			'niap',
			'nip',
			array(
				'class'=>'CDataColumn',
				'name'=>'jenis_kelamin_id',
				'header'=>'Jenis Kelamin',
				'type'=>'raw',
				'value'=>'$data->jenisKelamin->nama',
				'filter'=>CHtml::listData(JenisKelamin::model()->findAll(),'id','nama')
			),
			'tempat_lahir',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'Bantu::tanggalSingkat($data->tanggal_lahir)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'agama_id',
				'header'=>'Agama',
				'type'=>'raw',
				'value'=>'$data->agama->nama',
				'filter'=>CHtml::listData(Agama::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'pangkat_golongan_id',
				'header'=>'Pangkat Golongan',
				'type'=>'raw',
				'value'=>'$data->pangkatGolongan->nama',
				'filter'=>CHtml::listData(PangkatGolongan::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'pendidikan_id',
				'header'=>'Pendidikan Terakhir',
				'type'=>'raw',
				'value'=>'$data->pendidikan->nama',
				'filter'=>CHtml::listData(Pendidikan::model()->findAll(),'id','nama')
		),
			'nomor_pengangkatan',
			'tanggal_pengangkatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_pengangkatan',
				'header'=>'Tanggal Pengangkatan',
				'type'=>'raw',
				'value'=>'Bantu::tanggalSingkat($data->tanggal_pengangkatan)',
			),
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
