<?php
$this->breadcrumbs=array(
	'Aparat Pemerintahs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah AparatPemerintah','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting AparatPemerintah','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus AparatPemerintah','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola AparatPemerintah','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat AparatPemerintah</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
		'niap',
		'nip',
		'jenis_kelamin_id',
		'tempat_lahir',
		'tanggal_lahir',
		'agama_id',
		'pangkat_golongan_id',
		'jabatan',
		'pendidikan_id',
		'nomor_pengangkatan',
		'tanggal_pengangkatan',
		'keterangan',
),
)); ?>
