<?php
$this->breadcrumbs=array(
	'Aparat Pemerintahs',
);

$this->menu=array(
	array('label'=>'Tambah AparatPemerintah','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola AparatPemerintah','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Aparat Pemerintahs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
