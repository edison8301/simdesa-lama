<?php
$this->breadcrumbs=array(
	'Dusuns'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('dusun-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Dusun</h1>


<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'dusun-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>

</div>
