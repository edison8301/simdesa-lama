<?php
$this->breadcrumbs=array(
	'Dusuns',
);

$this->menu=array(
	array('label'=>'Tambah Dusun','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Dusun','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Dusuns</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
