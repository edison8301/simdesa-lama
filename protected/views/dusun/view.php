<?php
$this->breadcrumbs=array(
	'Dusuns'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Dusun','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Dusun','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Dusun','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Dusun','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Dusun</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
