<?php
$this->breadcrumbs=array(
	'Ekspedisi'=>array('admin'),
	'Kelola',
);

?>

<h1>Ekspedisi</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'ekspedisi-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		
		'columns'=>array(
			'tanggal_pengiriman',
			'nomor_surat',
			'tanggal_surat',
			'isi_singkat',
			'tujuan_surat',
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>

</div>
