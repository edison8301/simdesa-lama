<?php
$this->breadcrumbs=array(
	'Ekspedisis',
);

$this->menu=array(
	array('label'=>'Tambah Ekspedisi','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Ekspedisi','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Ekspedisis</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
