<?php
$this->breadcrumbs=array(
	'Ekspedisis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah Ekspedisi','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat Ekspedisi','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola Ekspedisi','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting Ekspedisi</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>