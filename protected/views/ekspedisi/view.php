<?php
$this->breadcrumbs=array(
	'Ekspedisis'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Ekspedisi','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Ekspedisi','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Ekspedisi','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Ekspedisi','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Ekspedisi</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'tanggal_pengiriman',
		'nomor_surat',
		'tanggal_surat',
		'isi_singkat',
		'tujuan_surat',
		'keterangan',
),
)); ?>
