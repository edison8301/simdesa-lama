<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_pengiriman')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_pengiriman); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_surat')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_surat')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isi_singkat')); ?>:</b>
	<?php echo CHtml::encode($data->isi_singkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('surat_dituju')); ?>:</b>
	<?php echo CHtml::encode($data->surat_dituju); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun')); ?>:</b>
	<?php echo CHtml::encode($data->tahun); ?>
	<br />

	*/ ?>

</div>