<?php
$this->breadcrumbs=array(
	'Ekspedisi BPD'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('ekspedisi-bpd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Ekspedisi BPD</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'ekspedisi-bpd-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'tanggal_pengiriman',
			'tanggal_surat',
			'nomor_surat',
			'isi_singkat',
			'surat_dituju',
			'keterangan',
			'tahun',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
