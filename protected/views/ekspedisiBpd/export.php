<?php
$this->breadcrumbs=array(
	'Ekspedisi Bpds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah EkspedisiBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting EkspedisiBpd','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus EkspedisiBpd','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola EkspedisiBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat EkspedisiBpd</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'tanggal_pengiriman',
		'tanggal_surat',
		'nomor_surat',
		'isi_singkat',
		'surat_dituju',
		'keterangan',
		'tahun',
),
)); ?>
