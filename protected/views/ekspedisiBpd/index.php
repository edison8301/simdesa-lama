<?php
$this->breadcrumbs=array(
	'Ekspedisi Bpds',
);

$this->menu=array(
	array('label'=>'Tambah EkspedisiBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola EkspedisiBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Ekspedisi Bpds</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
