<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'inventaris-desa-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'jenis_barang',array('class'=>'span5','maxlength'=>255)); ?>
	
	<div>&nbsp;</div>
	
	<legend>Asal Barang / Bangunan</legend>
	<?php echo $form->textFieldRow($model,'dibeli_sendiri',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'bantuan_pemerintah',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'bantuan_provinsi',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'bantuan_kabkota',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'sumbangan',array('class'=>'span5','maxlength'=>255)); ?>
	
	<div>&nbsp;</div>
	
	<legend>Keadaan Barang Awal Tahun</legend>
	
	<?php echo $form->textFieldRow($model,'keadaan_awal_baik',array('class'=>'span2','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keadaan_awal_rusak',array('class'=>'span2','maxlength'=>255)); ?>
	
	<div>&nbsp;</div>
	
	<legend>Tanggal Penghapusan</legend>
	
	<?php echo $form->textFieldRow($model,'penghapusan_rusak',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'penghapusan_dijual',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'penghapusan_disumbangkan',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'tanggal_penghapusan',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	
	<div>&nbsp;</div>
	
	<legend>Keadaan Barang Awal Tahun</legend>
	
	<?php echo $form->textFieldRow($model,'keadaan_akhir_baik',array('class'=>'span2','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keadaan_akhir_rusak',array('class'=>'span2','maxlength'=>255)); ?>
	
	<hr>
	
	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
