<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'jenis_barang',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'dibeli_sendiri',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'bantuan_pemerintah',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'bantuan_provinsi',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'bantuan_kabkota',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'sumbangan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'keadaan_awal_baik',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'keadaan_awal_rusak',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'penghapusan_rusak',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'penghapusan_dijual',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'penghapusan_disumbangkan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tanggal_penghapusan',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'keadaan_akhir_baik',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'keadaan_akhir_rusak',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'icon'=>'search white',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
