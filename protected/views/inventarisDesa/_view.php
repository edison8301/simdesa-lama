<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_barang')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dibeli_sendiri')); ?>:</b>
	<?php echo CHtml::encode($data->dibeli_sendiri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bantuan_pemerintah')); ?>:</b>
	<?php echo CHtml::encode($data->bantuan_pemerintah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bantuan_provinsi')); ?>:</b>
	<?php echo CHtml::encode($data->bantuan_provinsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bantuan_kabkota')); ?>:</b>
	<?php echo CHtml::encode($data->bantuan_kabkota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sumbangan')); ?>:</b>
	<?php echo CHtml::encode($data->sumbangan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('keadaan_awal_baik')); ?>:</b>
	<?php echo CHtml::encode($data->keadaan_awal_baik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keadaan_awal_rusak')); ?>:</b>
	<?php echo CHtml::encode($data->keadaan_awal_rusak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penghapusan_rusak')); ?>:</b>
	<?php echo CHtml::encode($data->penghapusan_rusak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penghapusan_dijual')); ?>:</b>
	<?php echo CHtml::encode($data->penghapusan_dijual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penghapusan_disumbangkan')); ?>:</b>
	<?php echo CHtml::encode($data->penghapusan_disumbangkan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_penghapusan')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_penghapusan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keadaan_akhir_baik')); ?>:</b>
	<?php echo CHtml::encode($data->keadaan_akhir_baik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keadaan_akhir_rusak')); ?>:</b>
	<?php echo CHtml::encode($data->keadaan_akhir_rusak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	*/ ?>

</div>