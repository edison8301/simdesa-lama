<?php
$this->breadcrumbs=array(
	'Inventaris Desas'=>array('admin'),
	'Kelola',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('inventaris-desa-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Inventaris Desa</h1>


<div style="overflow:auto">
<?php $this->widget('TbGridViewPlus',array(
		'id'=>'inventaris-desa-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'addingHeaders' => array(	
			array(''=>1,'Asal Barang / Bangunan'=>5,'Keadaan Barang / Bangunan Awal Tahun'=>2,'Tanggal Penghapusan'=>4,'Keadaan Barang / Bangunan Akhir Tahun'=>2,'&nbsp;'=>2),
		),
		'columns'=>array(
				array(
					'name'=>'jenis_barang',
					'headerHtmlOptions'=>array('style'=>'min-width: 200px'),
				),
				
				'dibeli_sendiri',
				'bantuan_pemerintah',
				'bantuan_provinsi',
				'bantuan_kabkota',
				'sumbangan',
			
				array(
					'name'=>'keadaan_awal_baik',
					'headerHtmlOptions'=>array('style'=>'min-width: 70px'),
				),
				array(
					'name'=>'keadaan_awal_rusak',
					'headerHtmlOptions'=>array('style'=>'min-width: 70px'),
				),
				'penghapusan_rusak',
				'penghapusan_dijual',
				'penghapusan_disumbangkan',
				'tanggal_penghapusan',
				array(
					'name'=>'keadaan_akhir_baik',
					'headerHtmlOptions'=>array('style'=>'min-width: 80px'),
				),
				array(
					'name'=>'keadaan_akhir_rusak',
					'headerHtmlOptions'=>array('style'=>'min-width: 80px'),
				),
				'keterangan',
				array(
					'class'=>'bootstrap.widgets.TbButtonColumn',
					'template'=>'{update}{delete}'
				),
			),
)); ?>

</div>
