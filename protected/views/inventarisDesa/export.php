<?php
$this->breadcrumbs=array(
	'Inventaris Desas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah InventarisDesa','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting InventarisDesa','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus InventarisDesa','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola InventarisDesa','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat InventarisDesa</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'jenis_barang',
		'dibeli_sendiri',
		'bantuan_pemerintah',
		'bantuan_provinsi',
		'bantuan_kabkota',
		'sumbangan',
		'keadaan_awal_baik',
		'keadaan_awal_rusak',
		'penghapusan_rusak',
		'penghapusan_dijual',
		'penghapusan_disumbangkan',
		'tanggal_penghapusan',
		'keadaan_akhir_baik',
		'keadaan_akhir_rusak',
		'keterangan',
),
)); ?>
