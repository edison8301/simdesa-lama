<?php
$this->breadcrumbs=array(
	'Inventaris Desas',
);

$this->menu=array(
	array('label'=>'Tambah InventarisDesa','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola InventarisDesa','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Inventaris Desas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
