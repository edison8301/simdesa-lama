<?php
$this->breadcrumbs=array(
	'Inventaris Desas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah InventarisDesa','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat InventarisDesa','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola InventarisDesa','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting InventarisDesa</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>