<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'inventaris-proyek-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_proyek',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'volume',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'biaya',array('class'=>'span5','maxlength'=>15,'prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'lokasi',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tahun',array('class'=>'span2','maxlength'=>4)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
