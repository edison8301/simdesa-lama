<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nama_proyek',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'volume',array('class'=>'span5','maxlength'=>15)); ?>

		<?php echo $form->textFieldRow($model,'biaya',array('class'=>'span5','maxlength'=>15)); ?>

		<?php echo $form->textFieldRow($model,'lokasi',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'tahun',array('class'=>'span5','maxlength'=>4)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'icon'=>'search white',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
