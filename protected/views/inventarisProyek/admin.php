<?php
$this->breadcrumbs=array(
	'Inventaris Proyeks'=>array('admin'),
	'Kelola',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('inventaris-proyek-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Inventaris Proyek</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'inventaris-proyek-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama_proyek',
			array(
				'class'=>'CDataColumn',
				'name'=>'volume',
				'header'=>'Volume',
				'type'=>'raw',
				'value'=>'$data->volume'
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'biaya',
				'header'=>'Biaya',
				'type'=>'raw',
				'value'=>'Bantu::rp($data->biaya)'
			),
			'lokasi',
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
