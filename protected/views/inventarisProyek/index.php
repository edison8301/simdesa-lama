<?php
$this->breadcrumbs=array(
	'Inventaris Proyeks',
);

$this->menu=array(
	array('label'=>'Tambah InventarisProyek','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola InventarisProyek','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Inventaris Proyeks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
