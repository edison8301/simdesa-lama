<?php
$this->breadcrumbs=array(
	'Inventaris Proyeks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

public $menu = array(
			array('label'=>'Penduduk Sementara','url'=>array('admin'),'icon'=>'list'),
			array('label'=>'Tambah','url'=>array('create'),'icon'=>'plus'),
			array('label'=>'Export Word','url'=>array('exportWord'),'icon'=>'download-alt'),
			array('label'=>'Export Excel','url'=>array('export'),'icon'=>'download-alt'),
	);

?>

	<h1>Sunting InventarisProyek</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>