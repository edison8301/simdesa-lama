<?php
$this->breadcrumbs=array(
	'Inventaris Proyeks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Inventaris Proyek','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Inventaris Proyek','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Inventaris Proyek','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Inventaris Proyek','url'=>array('admin'),'icon'=>'th-list'),
	array('label'=>'Export Single','url'=>array('export','id'=>$model->id),'icon'=>'download-alt'),
	
);
?>

<h1>Lihat Inventaris Proyek</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nama_proyek',
			array(
				'label'=>'Volume',
				'type'=>'raw',
				'value'=>Bantu::rp($model->volume)
			),
			array(
				'label'=>'Biaya',
				'type'=>'raw',
				'value'=>Bantu::rp($model->biaya)
			),
			'lokasi',
			'keterangan',
			'tahun',
		),
)); ?>
