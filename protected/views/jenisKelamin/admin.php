<?php
$this->breadcrumbs=array(
	'Jenis Kelamins'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Tambah JenisKelamin','url'=>array('create'),'icon'=>'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('jenis-kelamin-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Jenis Kelamins</h1>

<?php echo CHtml::link('Pencarian Lanjut','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'jenis-kelamin-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'nama',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
