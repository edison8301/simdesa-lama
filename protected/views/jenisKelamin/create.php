<?php
$this->breadcrumbs=array(
	'Jenis Kelamins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola JenisKelamin','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tambah JenisKelamin</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>