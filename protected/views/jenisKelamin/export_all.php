<?php
$this->breadcrumbs=array(
	'Jenis Kelamins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah JenisKelamin','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting JenisKelamin','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus JenisKelamin','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola JenisKelamin','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat JenisKelamin</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
