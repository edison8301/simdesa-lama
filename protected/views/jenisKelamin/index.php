<?php
$this->breadcrumbs=array(
	'Jenis Kelamins',
);

$this->menu=array(
	array('label'=>'Tambah JenisKelamin','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola JenisKelamin','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Jenis Kelamins</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
