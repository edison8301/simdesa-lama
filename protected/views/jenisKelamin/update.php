<?php
$this->breadcrumbs=array(
	'Jenis Kelamins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah JenisKelamin','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat JenisKelamin','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola JenisKelamin','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting JenisKelamin</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>