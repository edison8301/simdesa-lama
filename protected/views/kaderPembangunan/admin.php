<?php
$this->breadcrumbs=array(
	'Kader Pembangunans'=>array('admin'),
	'Kelola',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kader-pembangunan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kader Pembangunan</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'kader-pembangunan-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			'umur',
			array(
				'class'=>'CDataColumn',
				'name'=>'jenis_kelamin_id',
				'header'=>'Jenis Kelamin',
				'type'=>'raw',
				'value'=>'$data->jenisKelamin->nama',
				'filter'=>CHtml::listData(JenisKelamin::model()->findAll(),'id','nama')
			),
			'pendidikan',
			'bidang',
			'alamat',
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>
