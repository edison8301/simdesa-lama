<?php if(isset($_POST['proses']) AND $_POST['proses']=='1') { ?>

<h1>Hasil Export Data Pekerjaan</h1>

<table border="1">
<tr>
	<th>No</th>
	<th>Nama</th>
	<th>Umur</th>
	<th>Jenis Kelamin</th>
	<th>Pendidikan</th>
	<th>Bidang</th>
	<th>Alamat</th>
	<th>Keterangan</th>
</tr>
	


<?php
	
	$criteria = new CDbCriteria;
	
	if(isset($_POST['jenis_kelamin_id']) AND $_POST['jenis_kelamin_id']!=null)
		$criteria->addCondition('jenis_kelamin_id='.$_POST['jenis_kelamin_id']);
		
	$model = KaderPembangunan::model()->findAll($criteria);
	
	if($model!==null)
	{
		$i = 1;
		foreach($model as $data) {

?>
<tr>
	<td style="vertical-align:top"><?php print $i; ?></td>
	<td style="vertical-align:top"><?php print $data->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->umur; ?></td>
	<td style="vertical-align:top"><?php print $data->jenisKelamin->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->pendidikan; ?></td>
	<td style="vertical-align:top"><?php print $data->bidang; ?></td>
	<td style="vertical-align:top"><?php print $data->alamat; ?></td>
	<td style="vertical-align:top"><?php print $data->keterangan; ?></td>
</tr>
	

<?php 	
		$i++; }
	}
?>

</table>

<?php } else { ?>

<?php $this->menu=array(
		array('label'=>'Kelola Kader Pembangunan','url'=>array('admin'),'icon'=>'list'),
); ?>

<h1>Export Kader Pembangunan</h1>

<div>&nbsp;</div>


<?php print CHtml::beginForm(array('exportAll')); ?>

<?php print CHtml::label('Jenis Kelamin',''); ?>
<?php print CHtml::dropDownList('jenis_kelamin_id','',CHtml::listData(JenisKelamin::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Jenis Kelamin --')); ?>

<?php print CHtml::hiddenField('proses','1'); ?>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Export','icon'=>'download-alt white','type'=>'primary')); ?>


<?php print CHtml::endForm(); ?>

<?php } ?>
