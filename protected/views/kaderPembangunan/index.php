<?php
$this->breadcrumbs=array(
	'Kader Pembangunans',
);

$this->menu=array(
	array('label'=>'Tambah KaderPembangunan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola KaderPembangunan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Kader Pembangunans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
