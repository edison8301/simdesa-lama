<?php
$this->breadcrumbs=array(
	'Kader Pembangunans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Kader Pembangunan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Kader Pembangunan','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Kader Pembangunan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Kader Pembangunan','url'=>array('admin'),'icon'=>'th-list'),
	array('label'=>'Export Single','url'=>array('export','id'=>$model->id),'icon'=>'download-alt'),
	
);
?>

<h1>Lihat Kader Pembangunan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'nama',
		'umur',
		array(
				'label'=>'Jenis Kelamin',
				'type'=>'raw',
				'value'=>CHtml::encode($model->jenisKelamin->nama)
			),
		'pendidikan',
		'bidang',
		'alamat',
		'keterangan',
),
)); ?>
