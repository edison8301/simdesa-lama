<?php print CHtml::beginForm(); ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'name'=>'tanggal_awal',
		// additional javascript options for the date picker plugin
		'options'=>array(
			'showAnim'=>'fold',
		),
		'htmlOptions'=>array(
			'style'=>'height:20px;',
			'placeholder'=>'Tanggal Awal',
		),
)); ?>&nbsp;
<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'name'=>'tanggal_akhir',
		// additional javascript options for the date picker plugin
		'options'=>array(
			'showAnim'=>'fold',
		),
		'htmlOptions'=>array(
			'style'=>'height:20px;',
			'placeholder'=>'Tanggal Akhir',
		),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Filter','icon'=>'search','type'=>'primary')); ?>

<?php print CHtml::endForm(); ?>
