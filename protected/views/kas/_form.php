<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'kas-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'kas_jenis_id',CHtml::listData(KasJenis::model()->findAll(),'id','nama')); ?>

	<?php echo $form->labelEx($model,'tanggal'); ?>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'Kas[tanggal]',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'changeMonth'=>true,
				'changeYear'=>true,
				'dateFormat'=>'yy-mm-dd',
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;'
			),
			'value'=>$model->tanggal
	)); ?>
	<?php print $form->error($model,'tanggal'); ?>
	
	<?php print CHtml::label('Kode Anggaran',''); ?>
	<?php echo $form->textField($model,'kode_1',array('class'=>'span1','maxlength'=>2)); ?> .

	<?php echo $form->textField($model,'kode_2',array('class'=>'span1','maxlength'=>2)); ?> .

	<?php echo $form->textField($model,'kode_3',array('class'=>'span1','maxlength'=>2)); ?> .

	<?php echo $form->textField($model,'kode_4',array('class'=>'span1','maxlength'=>2)); ?> .

	<?php echo $form->textField($model,'kode_5',array('class'=>'span1','maxlength'=>2)); ?> .

	<?php echo $form->textField($model,'kode_6',array('class'=>'span1','maxlength'=>2)); ?>
	
	<?php echo $form->textAreaRow($model,'uraian',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'nomor_bukti',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'jumlah',array('class'=>'span5','maxlength'=>15,'prepend'=>'Rp')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
