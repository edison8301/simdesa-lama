<?php
$this->breadcrumbs=array(
	'Kases'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Tambah Kas','url'=>array('create'),'icon'=>'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kas-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Kases</h1>

<?php echo CHtml::link('Pencarian Lanjut','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'kas-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'kas_jenis_id',
		'tanggal',
		'uraian',
		'nomor_bukti',
		'kode_1',
		/*
		'kode_2',
		'kode_3',
		'kode_4',
		'kode_5',
		'kode_6',
		'jumlah',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
