<?php

$this->menu=array(
	array('label'=>'Kas Umum','url'=>array('umum'),'icon'=>'tags'),
	array('label'=>'Kas Penerimaan','url'=>array('penerimaan'),'icon'=>'tags'),
	array('label'=>'Kas Pengeluaran','url'=>array('pengeluaran'),'icon'=>'tags'),
);

?>

<h1>Export Excel Data Buku Kas</h1>


<?php print CHtml::beginForm(); ?>
	
	<?php print CHtml::label('Jenis Buku Kas',''); ?>
	<?php print CHtml::dropDownList('jenis','',array('1'=>'Umum','2'=>'Penerimaan','3'=>'Pengeluaran')); ?>
	
	<?php print CHtml::label('Tanggal',''); ?>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'tanggal_awal',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat'=>'yy-mm-dd',
				'changeYear'=>true,
				'changeMonth'=>true
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'placeholder'=>'Tanggal Awal',
			),
			'value'=>date('Y-m').'-01'
	)); ?> - 	
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'tanggal_akhir',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat'=>'yy-mm-dd',
				'changeYear'=>true,
				'changeMonth'=>true
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'placeholder'=>'Tanggal Akhir',
			),
			'value'=>date('Y-m').'-'.date('t')
	)); ?>
	
	<?php print CHtml::label('Kode Anggaran',''); ?>
	<?php print CHtml::textField('kode_1',$_POST['kode_1'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_2',$_POST['kode_2'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_3',$_POST['kode_3'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_4',$_POST['kode_4'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_5',$_POST['kode_5'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_6',$_POST['kode_6'],array('class'=>'span1')); ?>
	
	<?php print CHtml::hiddenField('proses','1'); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Export','icon'=>'download-alt white','type'=>'primary')); ?>
	</div>

<?php print CHtml::endForm(); ?>




