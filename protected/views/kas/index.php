<?php
$this->breadcrumbs=array(
	'Kases',
);

$this->menu=array(
	array('label'=>'Tambah Kas','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Kas','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Kases</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
