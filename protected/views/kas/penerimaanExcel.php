<h1>Buku Kas Pembantu Penerimaan</h1>

<table border="1">

<tr>
	<th>No</th>
	<th>Tanggal</th>
	<th>Nomor Buku</th>
	<th>Uraian</th>
	<th>Jumlah</th>
</tr>

<?php $i=1; foreach(Kas::getDataKeuangan('penerimaan') as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print Bantu::tanggalSingkat($data->tanggal); ?></td>
	<td><?php print $data->getKodeAnggaran(); ?></td>
	<td><?php print $data->uraian; ?></td>
	<td><?php print $data->jumlah; ?></td>
</tr>

<?php $i++; } ?>

</table>