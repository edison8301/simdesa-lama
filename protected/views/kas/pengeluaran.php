<?php

$this->menu=array(
	array('label'=>'Tambah Penerimaan','url'=>array('create','kas_jenis_id'=>1),'icon'=>'plus'),
	array('label'=>'Tambah Pengeluaran','url'=>array('create','kas_jenis_id'=>2),'icon'=>'plus'),
	array('label'=>'Kas Umum','url'=>array('umum'),'icon'=>'tags'),
	array('label'=>'Kas Penerimaan','url'=>array('penerimaan'),'icon'=>'tags'),
	array('label'=>'Kas Pengeluaran','url'=>array('pengeluaran'),'icon'=>'tags'),
	array('label'=>'Export Word','url'=>array('exportWord'),'icon'=>'download-alt'),
	//array('label'=>'Export Excel','url'=>array('exportExcel'),'icon'=>'download-alt'),
);

?>

<h1>Buku Kas Pembantu Pengeluaran</h1>

<div>&nbsp;</div>

<table class="table">

<tr>
	<th>No</th>
	<th>Tanggal</th>
	<th>Nomor Buku</th>
	<th>Uraian</th>
	<th>Jumlah</th>
	<th>&nbsp;</th>
</tr>

<?php $i=1; $total = 0; foreach(Kas::getDataKeuangan('pengeluaran') as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print Bantu::tanggalSingkat($data->tanggal); ?></td>
	<td><?php print $data->getKodeAnggaran(); ?></td>
	<td><?php print $data->uraian; ?></td>
	<td><?php print Bantu::rp($data->jumlah); ?></td>
	<td>
		<?php print CHtml::link("<i class='icon-pencil'></i>",array('kas/update','id'=>$data->id)); ?>
		<?php print CHtml::link("<i class='icon-trash'></i>",'#',array('submit'=>array('kas/delete','id'=>$data->id),params=>array('returnUrl'=>$this->createUrl('kas/umum')),'confirm'=>'Yakin akan menghapus data?')); ?>
	</td>
</tr>

<?php $i++; $total = $total + $data->jumlah; } ?>
<tr>
	<th colspan="4" style="text-align:right">TOTAL</th>
	<th><?php print Bantu::rp($total); ?></th>
	<th>&nbsp;</th>
</tr>

</table>

<div>&nbsp;</div>

<h3>Filter Data</h3>

<?php print CHtml::beginForm(); ?>

	<?php print CHtml::label('Tanggal',''); ?>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'tanggal_awal',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat'=>'yy-mm-dd',
				'changeYear'=>true,
				'changeMonth'=>true
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'placeholder'=>'Tanggal Awal',
			),
			'value'=>$_POST['tanggal_awal']
	)); ?> - 	
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'tanggal_akhir',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat'=>'yy-mm-dd',
				'changeYear'=>true,
				'changeMonth'=>true
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'placeholder'=>'Tanggal Akhir',
			),
			'value'=>$_POST['tanggal_akhir']
	)); ?>
	
	<?php print CHtml::label('Kode Anggaran',''); ?>
	<?php print CHtml::textField('kode_1',$_POST['kode_1'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_2',$_POST['kode_2'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_3',$_POST['kode_3'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_4',$_POST['kode_4'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_5',$_POST['kode_5'],array('class'=>'span1')); ?> .
	<?php print CHtml::textField('kode_6',$_POST['kode_6'],array('class'=>'span1')); ?>
	
	<?php print CHtml::hiddenField('filter','1'); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Filter Data','icon'=>'search white','type'=>'primary')); ?>
	</div>

<?php print CHtml::endForm(); ?>



