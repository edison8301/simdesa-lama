<h1>Buku Kas Umum</h1>

<table border="1">

<tr>
	<th>No</th>
	<th>Tanggal</th>
	<th>Nomor Buku</th>
	<th>Uraian</th>
	<th>Penerimaan</th>
	<th>Pengeluaran</th>
</tr>

<?php $i=1; $total_penerimaan=0; $total_pengeluaran = 0; foreach(Kas::getDataKeuangan('umum') as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print Bantu::tanggalSingkat($data->tanggal); ?></td>
	<td><?php print $data->getKodeAnggaran(); ?></td>
	<td><?php print $data->uraian; ?></td>
	<td><?php if($data->kas_jenis_id==1) print $data->jumlah; else print ""; ?></td>
	<td><?php if($data->kas_jenis_id==2) print $data->jumlah; else print ""; ?></td>
</tr>

<?php $i++; if($data->kas_jenis_id == 1) $total_penerimaan = $total_penerimaan + $data->jumlah; if($data->kas_jenis_id == 2) $total_pengeluaran = $total_pengeluaran + $data->jumlah; } ?>
<tr>
	<th colspan="4" style="text-align:right">TOTAL</th>
	<th><?php print $total_penerimaan; ?></th>
	<th><?php print $total_pengeluaran; ?></th>
</tr>
</table>


