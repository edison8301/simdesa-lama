<?php
$this->breadcrumbs=array(
	'Kases'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Kas Umum','url'=>array('umum'),'icon'=>'tags'),
	array('label'=>'Kas Penerimaan','url'=>array('penerimaan'),'icon'=>'tags'),
	array('label'=>'Kas Pengeluaran','url'=>array('pengeluaran'),'icon'=>'tags'),
);

?>

	<h1>Sunting Data Keuangan</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>