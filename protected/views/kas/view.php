<?php
$this->breadcrumbs=array(
	'Kases'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Kas','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Kas','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Kas','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Kas','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Kas</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'kas_jenis_id',
			'tanggal',
			array(
				'label'=>'Kode Anggaran',
				'value'=>$model->getKodeAnggaran()
			),
			'uraian',
			'nomor_bukti',
			array(
				'label'=>'Jumlah',
				'value'=>'Rp '.$model->jumlah
			),
		),
)); ?>
