<?php
$this->breadcrumbs=array(
	'Kas Jenises'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola KasJenis','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tambah KasJenis</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>