<?php
$this->breadcrumbs=array(
	'Kas Jenises',
);

$this->menu=array(
	array('label'=>'Tambah KasJenis','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola KasJenis','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Kas Jenises</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
