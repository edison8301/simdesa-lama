<?php
$this->breadcrumbs=array(
	'Kas Jenises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah KasJenis','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat KasJenis','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola KasJenis','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting KasJenis</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>