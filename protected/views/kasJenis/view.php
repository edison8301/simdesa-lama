<?php
$this->breadcrumbs=array(
	'Kas Jenises'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah KasJenis','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting KasJenis','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus KasJenis','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola KasJenis','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat KasJenis</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
