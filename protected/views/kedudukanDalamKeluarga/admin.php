<?php
$this->breadcrumbs=array(
	'Kedudukan Dalam Keluarga'=>array('admin'),
	'Kelola',
);

?>

<h1>Kedudukan Dalam Keluarga</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'kedudukan-dalam-keluarga-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>
