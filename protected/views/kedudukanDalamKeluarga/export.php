<?php
$this->breadcrumbs=array(
	'Kedudukan Dalam Keluargas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah KedudukanDalamKeluarga','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting KedudukanDalamKeluarga','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus KedudukanDalamKeluarga','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola KedudukanDalamKeluarga','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat KedudukanDalamKeluarga</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
