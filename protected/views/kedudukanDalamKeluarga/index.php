<?php
$this->breadcrumbs=array(
	'Kedudukan Dalam Keluargas',
);

$this->menu=array(
	array('label'=>'Tambah KedudukanDalamKeluarga','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola KedudukanDalamKeluarga','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Kedudukan Dalam Keluargas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
