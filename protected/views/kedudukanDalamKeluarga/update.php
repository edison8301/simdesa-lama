<?php
$this->breadcrumbs=array(
	'Kedudukan Dalam Keluargas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah KedudukanDalamKeluarga','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat KedudukanDalamKeluarga','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola KedudukanDalamKeluarga','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting KedudukanDalamKeluarga</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>