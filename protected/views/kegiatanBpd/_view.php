<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tentang')); ?>:</b>
	<?php echo CHtml::encode($data->tentang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pelaksana')); ?>:</b>
	<?php echo CHtml::encode($data->pelaksana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pokok_kegiatan')); ?>:</b>
	<?php echo CHtml::encode($data->pokok_kegiatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hasil_kegiatan')); ?>:</b>
	<?php echo CHtml::encode($data->hasil_kegiatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun')); ?>:</b>
	<?php echo CHtml::encode($data->tahun); ?>
	<br />


</div>