<?php
$this->breadcrumbs=array(
	'Kegiatan BPD'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kegiatan-bpd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kegiatan BPD</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'kegiatan-bpd-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'tentang',
			'pelaksana',
			'pokok_kegiatan',
			'hasil_kegiatan',
			'keterangan',
			'tahun',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
