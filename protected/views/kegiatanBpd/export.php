<?php
$this->breadcrumbs=array(
	'Kegiatan Bpds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah KegiatanBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting KegiatanBpd','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus KegiatanBpd','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola KegiatanBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat KegiatanBpd</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'tentang',
		'pelaksana',
		'pokok_kegiatan',
		'hasil_kegiatan',
		'keterangan',
		'tahun',
),
)); ?>
