<?php
$this->breadcrumbs=array(
	'Kegiatan Bpds',
);

$this->menu=array(
	array('label'=>'Tambah KegiatanBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola KegiatanBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Kegiatan Bpds</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
