<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_proyek')); ?>:</b>
	<?php echo CHtml::encode($data->nama_proyek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('volume')); ?>:</b>
	<?php echo CHtml::encode($data->volume); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dana_pemerintah')); ?>:</b>
	<?php echo CHtml::encode($data->dana_pemerintah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dana_provinsi')); ?>:</b>
	<?php echo CHtml::encode($data->dana_provinsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dana_kabkota')); ?>:</b>
	<?php echo CHtml::encode($data->dana_kabkota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dana_swadaya')); ?>:</b>
	<?php echo CHtml::encode($data->dana_swadaya); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu')); ?>:</b>
	<?php echo CHtml::encode($data->waktu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('baru')); ?>:</b>
	<?php echo CHtml::encode($data->baru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lanjutan')); ?>:</b>
	<?php echo CHtml::encode($data->lanjutan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pelaksana')); ?>:</b>
	<?php echo CHtml::encode($data->pelaksana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun')); ?>:</b>
	<?php echo CHtml::encode($data->tahun); ?>
	<br />

	*/ ?>

</div>