<?php
$this->breadcrumbs=array(
	'Kegiatan Pembangunans'=>array('admin'),
	'Kelola',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kegiatan-pembangunan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kegiatan Pembangunan</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'kegiatan-pembangunan-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'nama_proyek',
		array(
			'class'=>'CDataColumn',
			'name'=>'volume',
			'header'=>'Volume',
			'type'=>'raw',
			'value'=>'$data->volume'
		),
		'dana_pemerintah',
		'dana_provinsi',
		'dana_kabkota',
		'dana_swadaya',
		array(
			'class'=>'CDataColumn',
			'name'=>'jumlah',
			'header'=>'Jumlah',
			'type'=>'raw',
			'value'=>'$data->jumlah'
		),
		'waktu',
		'baru',
		'lanjutan',
		'pelaksana',
		'keterangan',
		'tahun',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}'
		),
	),
)); ?>
