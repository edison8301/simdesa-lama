<h1>Hasil Export Data Penduduk</h1>

<table border="1">
<tr>
	<th style="vertical-align:top">Nama Proyek</th>
	<td style="vertical-align:top;width:500px"><?php print $model->nama_proyek; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Volume</th>
	<td style="vertical-align:top;width:500px"><?php print $model->volume; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dana Pemerintah</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dana_pemerintah; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dana Provinsi</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dana_provinsi; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dana KabKota</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dana_kabkota; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dana Swadaya</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dana_swadaya; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Jumlah</th>
	<td style="vertical-align:top;width:500px"><?php print $model->jumlah; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Waktu</th>
	<td style="vertical-align:top;width:500px"><?php print $model->waktu; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Baru</th>
	<td style="vertical-align:top;width:500px"><?php print $model->baru; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Lanjutan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->lanjutan; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Pekerjaan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->pelaksana; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Keterangan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->keterangan; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Tahun</th>
	<td style="vertical-align:top;width:500px"><?php print $model->tahun; ?></td>
</tr>
</table>