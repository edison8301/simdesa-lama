<?php if(isset($_POST['proses']) AND $_POST['proses']=='1') { ?>

<h1>Hasil Export Kegiatan Pembangunan</h1>

<table border="1">
<tr>
	<th>No</th>
	<th>Nama Proyek</th>
	<th>Volume</th>
	<th>Dana Pemerintah</th>
	<th>Dana Provinsi</th>
	<th>Dana KabKota</th>
	<th>Dana Swadaya</th>
	<th>Jumlah</th>
	<th>Waktu</th>
	<th>Baru</th>
	<th>Lanjutan</th>
	<th>Pelaksana</th>
	<th>Keterangan</th>
	<th>Tahun</th>
</tr>
	


<?php
	
	$criteria = new CDbCriteria;
	
	if(isset($_POST['tahun']) AND $_POST['tahun']!=null)
		$criteria->addCondition = 'tahun='.$_POST['tahun'];
		
	$model = KegiatanPembangunan::model()->findAll($criteria);
	
	if($model!==null)
	{
		$i = 1;
		foreach($model as $data) {

?>
<tr>
	<td style="vertical-align:top"><?php print $i; ?></td>
	<td style="vertical-align:top"><?php print $data->nama_proyek; ?></td>
	<td style="vertical-align:top"><?php print $data->volume; ?></td>
	<td style="vertical-align:top"><?php print $data->dana_pemerintah; ?></td>
	<td style="vertical-align:top"><?php print $data->dana_provinsi; ?></td>
	<td style="vertical-align:top"><?php print $data->dana_kabkota; ?></td>
	<td style="vertical-align:top"><?php print $data->dana_swadaya; ?></td>
	<td style="vertical-align:top"><?php print $data->jumlah; ?></td>
	<td style="vertical-align:top"><?php print $data->waktu; ?></td>
	<td style="vertical-align:top"><?php print $data->baru; ?></td>
	<td style="vertical-align:top"><?php print $data->lanjutan; ?></td>
	<td style="vertical-align:top"><?php print $data->pelaksana; ?></td>
	<td style="vertical-align:top"><?php print $data->keterangan; ?></td>
	<td style="vertical-align:top"><?php print $data->tahun; ?></td>
</tr>
	

<?php 	
		$i++; }
	}
?>

</table>

<?php } else { ?>

<?php $this->menu=array(
		array('label'=>'Kelola Kegiatan Pembangunan','url'=>array('admin'),'icon'=>'list'),
); ?>

<h1>Export Kegiatan Pembangunan</h1>

<div>&nbsp;</div>


<?php print CHtml::beginForm(array('exportAll')); ?>

<?php print CHtml::label('Tahun',''); ?>
<?php print CHtml::textField('tahun',''); ?>

<?php print CHtml::hiddenField('proses','1'); ?>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Export','icon'=>'download-alt white','type'=>'primary')); ?>


<?php print CHtml::endForm(); ?>

<?php } ?>
