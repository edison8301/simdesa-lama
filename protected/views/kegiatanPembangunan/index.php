<?php
$this->breadcrumbs=array(
	'Kegiatan Pembangunans',
);

$this->menu=array(
	array('label'=>'Tambah KegiatanPembangunan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola KegiatanPembangunan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Kegiatan Pembangunans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
