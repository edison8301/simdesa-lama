<?php
$this->breadcrumbs=array(
	'Kegiatan Pembangunans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Kegiatan Pembangunan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Kegiatan Pembangunan','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Kegiatan Pembangunan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Kegiatan Pembangunan','url'=>array('admin'),'icon'=>'th-list'),
	array('label'=>'Export Single','url'=>array('export','id'=>$model->id),'icon'=>'download-alt'),
	
);
?>

<h1>Lihat Kegiatan Pembangunan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nama_proyek',
			array(
				'label'=>'Volume',
				'value'=>Bantu::rp($model->volume)
			),
			array(
				'label'=>'Dana Pemerintah',
				'value'=>Bantu::rp($model->dana_pemerintah)
			),
			array(
				'label'=>'Dana Provinsi',
				'value'=>Bantu::rp($model->dana_provinsi)
			),
			array(
				'label'=>'Dana Kab/Kota',
				'value'=>Bantu::rp($model->dana_kabkota)
			),
			array(
				'label'=>'Dana Swadaya',
				'value'=>Bantu::rp($model->dana_swadaya)
			),
			array(
				'label'=>'Jumlah',
				'value'=>Bantu::rp($model->jumlah)
			),
			'waktu',
			'baru',
			'lanjutan',
			'pelaksana',
			'keterangan',
			'tahun',
		),
)); ?>
