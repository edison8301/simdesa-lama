<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_keputusan')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_keputusan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_keputusan')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_keputusan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tentang')); ?>:</b>
	<?php echo CHtml::encode($data->tentang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uraian_singkat')); ?>:</b>
	<?php echo CHtml::encode($data->uraian_singkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />


</div>