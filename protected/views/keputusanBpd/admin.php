<?php
$this->breadcrumbs=array(
	'Keputusan BPD'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('keputusan-bpd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Keputusan BPD</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'keputusan-bpd-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'tanggal_keputusan',
			'nomor_keputusan',
			'tentang',
			'uraian_singkat',
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
