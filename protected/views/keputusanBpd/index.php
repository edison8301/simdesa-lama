<?php
$this->breadcrumbs=array(
	'Keputusan Bpds',
);

$this->menu=array(
	array('label'=>'Tambah KeputusanBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola KeputusanBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Keputusan Bpds</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
