<?php
$this->breadcrumbs=array(
	'Keputusan Bpds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah KeputusanBpd','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting KeputusanBpd','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus KeputusanBpd','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola KeputusanBpd','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat KeputusanBpd</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'tanggal_keputusan',
		'nomor_keputusan',
		'tentang',
		'uraian_singkat',
		'keterangan',
),
)); ?>
