<?php
$this->breadcrumbs=array(
	'Keputusan Kades'=>array('admin'),
	'Kelola',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('keputusan-kades-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Keputusan Kepala Desa</h1>


<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'keputusan-kades-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nomor_keputusan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_keputusan',
				'header'=>'Tanggal Keputusan',
				'type'=>'raw',
				'value'=>'Bantu::tanggalSingkat($data->tanggal_keputusan)',
			),
			'tentang',
			'uraian_singkat',
			'nomor_dilaporkan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_dilaporkan',
				'header'=>'Tanggal Dilaporkan',
				'type'=>'raw',
				'value'=>'Bantu::tanggalSingkat($data->tanggal_dilaporkan)',
			),
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
