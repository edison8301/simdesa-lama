<?php
$this->breadcrumbs=array(
	'Keputusan Kades'=>array('admin'),
	'Tambah',
);

?>

<h1>Tambah Keputusan Kepala Desa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>