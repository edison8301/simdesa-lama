<?php
$this->breadcrumbs=array(
	'Keputusan Kades'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah KeputusanKades','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting KeputusanKades','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus KeputusanKades','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola KeputusanKades','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat KeputusanKades</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nomor_keputusan',
		'tanggal_keputusan',
		'tentang',
		'uraian_singkat',
		'nomor_dilaporkan',
		'tanggal_dilaporkan',
		'keterangan',
),
)); ?>
