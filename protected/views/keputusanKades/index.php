<?php
$this->breadcrumbs=array(
	'Keputusan Kades',
);

$this->menu=array(
	array('label'=>'Tambah KeputusanKades','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola KeputusanKades','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Keputusan Kades</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
