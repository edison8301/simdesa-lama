<?php
$this->breadcrumbs=array(
	'Keputusan Kades'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah KeputusanKades','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat KeputusanKades','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola KeputusanKades','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting KeputusanKades</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>