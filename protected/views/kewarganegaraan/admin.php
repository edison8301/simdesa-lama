<?php
$this->breadcrumbs=array(
	'Kewarganegaraans'=>array('admin'),
	'Kelola',
);

?>

<h1>Kewarganegaraan</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'kewarganegaraan-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
