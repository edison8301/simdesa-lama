<?php
$this->breadcrumbs=array(
	'Kewarganegaraans',
);

$this->menu=array(
	array('label'=>'Tambah Kewarganegaraan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Kewarganegaraan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Kewarganegaraans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
