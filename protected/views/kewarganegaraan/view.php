<?php
$this->breadcrumbs=array(
	'Kewarganegaraans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Kewarganegaraan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Kewarganegaraan','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Kewarganegaraan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Kewarganegaraan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Kewarganegaraan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
