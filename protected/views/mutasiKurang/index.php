<?php
$this->breadcrumbs=array(
	'Mutasi Kurangs',
);

$this->menu=array(
	array('label'=>'Tambah MutasiKurang','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola MutasiKurang','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Mutasi Kurangs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
