<?php
$this->breadcrumbs=array(
	'Mutasi Kurangs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah MutasiKurang','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting MutasiKurang','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus MutasiKurang','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola MutasiKurang','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat MutasiKurang</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
