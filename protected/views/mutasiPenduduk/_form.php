<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'mutasi-penduduk-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'tanggal_lahir',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	
	<?php echo $form->dropDownListRow($model,'jenis_kelamin_id',CHtml::listData(JenisKelamin::model()->findAll(),'id','nama')); ?>
	
	<?php echo $form->dropDownListRow($model,'kewarganegaraan_id',CHtml::listData(Kewarganegaraan::model()->findAll(),'id','nama')); ?>


	<legend>Penambahan</legend>
	<?php echo $form->textFieldRow($model,'datang',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'datang_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'lahir',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'lahir_tanggal',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>

	<legend>Pengurangan</legend>
	<?php echo $form->textFieldRow($model,'pindah',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'pindah_tanggal',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	
	<?php echo $form->textFieldRow($model,'mati',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'mati_tanggal',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	<hr>
	
	<?php echo $form->dropDownListRow($model,'dusun_id',CHtml::listData(Dusun::model()->findAll(),'id','nama')); ?>

	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
