<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin_id')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kewarganegaraan')); ?>:</b>
	<?php echo CHtml::encode($data->kewarganegaraan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datang')); ?>:</b>
	<?php echo CHtml::encode($data->datang); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('datang_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->datang_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lahir')); ?>:</b>
	<?php echo CHtml::encode($data->lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lahir_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->lahir_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pindah')); ?>:</b>
	<?php echo CHtml::encode($data->pindah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pindah_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->pindah_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mati')); ?>:</b>
	<?php echo CHtml::encode($data->mati); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mati_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->mati_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	*/ ?>

</div>