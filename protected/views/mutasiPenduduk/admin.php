<?php
$this->breadcrumbs=array(
	'Mutasi Penduduk'=>array('admin'),
	'Kelola',
);

?>

<h1>Mutasi Penduduk</h1>


<div style="overflow:auto">
<?php $this->widget('TbGridViewPlus',array(
		'id'=>'mutasi-penduduk-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'addingHeaders' => array(	
			array('&nbsp;'=>5,'Penambahan'=>4,'Pengeluaran'=>4,''=>1),
		),
		'columns'=>array(
			'nama',
			'tempat_lahir',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_lahir',
				'header'=>'Tanggal Lahir',
				'value'=>'Bantu::tanggalSingkat($data->tanggal_lahir)'
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'jenis_kelamin_id',
				'header'=>'Jenis Kelamin',
				'type'=>'raw',
				'value'=>'$data->jenisKelamin->nama',
				'filter'=>CHtml::listData(JenisKelamin::model()->findAll(),"id","nama")
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'kewarganegaraan_id',
				'header'=>'Kewarganegaraan',
				'type'=>'raw',
				'value'=>'$data->kewarganegaraan->nama',
				'filter'=>CHtml::listData(Kewarganegaraan::model()->findAll(),"id","nama")
			),
			'datang',
			array(
				'class'=>'CDataColumn',
				'name'=>'datang_tanggal',
				'header'=>'Datang Tanggal',
				'value'=>'Bantu::tanggalSingkat($data->datang_tanggal)'
			),
			'lahir',
			array(
				'class'=>'CDataColumn',
				'name'=>'lahir_tanggal',
				'header'=>'Lahir Tanggal',
				'value'=>'Bantu::tanggalSingkat($data->lahir_tanggal)'
			),
			'pindah',
			array(
				'class'=>'CDataColumn',
				'name'=>'pindah_tanggal',
				'header'=>'Pindah Tanggal',
				'value'=>'Bantu::tanggalSingkat($data->pindah_tanggal)'
			),
			'mati',
			array(
				'class'=>'CDataColumn',
				'name'=>'mati_tanggal',
				'header'=>'Mati Tanggal',
				'value'=>'Bantu::tanggalSingkat($data->mati_tanggal)'
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'dusun_id',
				'header'=>'Dusun',
				'type'=>'raw',
				'value'=>'$data->dusun->nama',
				'filter'=>CHtml::listData(Dusun::model()->findAll(),"id","nama")
			),
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
