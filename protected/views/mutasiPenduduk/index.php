<?php
$this->breadcrumbs=array(
	'Mutasi Penduduks',
);

$this->menu=array(
	array('label'=>'Tambah MutasiPenduduk','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola MutasiPenduduk','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Mutasi Penduduks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
