<?php
$this->breadcrumbs=array(
	'Mutasi Penduduks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah MutasiPenduduk','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting MutasiPenduduk','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus MutasiPenduduk','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola MutasiPenduduk','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat MutasiPenduduk</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
		'tempat_lahir',
		'tanggal_lahir',
		'jenis_kelamin_id',
		'kewarganegaraan',
		'datang',
		'datang_tanggal',
		'lahir',
		'lahir_tanggal',
		'pindah',
		'pindah_tanggal',
		'mati',
		'mati_tanggal',
		'keterangan',
),
)); ?>
