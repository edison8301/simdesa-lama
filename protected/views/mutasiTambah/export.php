<?php
$this->breadcrumbs=array(
	'Mutasi Tambahs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah MutasiTambah','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting MutasiTambah','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus MutasiTambah','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola MutasiTambah','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat MutasiTambah</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
