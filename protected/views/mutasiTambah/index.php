<?php
$this->breadcrumbs=array(
	'Mutasi Tambahs',
);

$this->menu=array(
	array('label'=>'Tambah MutasiTambah','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola MutasiTambah','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Mutasi Tambahs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
