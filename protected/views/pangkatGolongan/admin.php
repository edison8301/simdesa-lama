<?php
$this->breadcrumbs=array(
	'Pangkat Golongans'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('pangkat-golongan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Pangkat Golongans</h1>

<?php echo CHtml::link('Pencarian Lanjut','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pangkat-golongan-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
		'id',
		'nama',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>

</div>
