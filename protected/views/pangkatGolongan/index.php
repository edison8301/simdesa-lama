<?php
$this->breadcrumbs=array(
	'Pangkat Golongans',
);

$this->menu=array(
	array('label'=>'Tambah PangkatGolongan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola PangkatGolongan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Pangkat Golongans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
