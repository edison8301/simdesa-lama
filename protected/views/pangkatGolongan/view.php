<?php
$this->breadcrumbs=array(
	'Pangkat Golongans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah PangkatGolongan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting PangkatGolongan','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus PangkatGolongan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola PangkatGolongan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat PangkatGolongan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
