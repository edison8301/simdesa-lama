<?php
$this->breadcrumbs=array(
	'Pekerjaan'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Pekerjaan','url'=>array('admin'),'icon'=>'list'),
	array('label'=>'Tambah','url'=>array('create'),'icon'=>'plus'),
);


?>

<h1>Pekerjaan</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pekerjaan-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>
