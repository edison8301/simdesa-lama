<?php
$this->breadcrumbs=array(
	'Pekerjaan'=>array('admin'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Pekerjaan','url'=>array('admin'),'icon'=>'list'),
	array('label'=>'Tambah','url'=>array('create'),'icon'=>'plus'),
);
?>

<h1>Tambah Pekerjaan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>