<?php
$this->breadcrumbs=array(
	'Pekerjaans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Pekerjaan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Pekerjaan','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Pekerjaan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Pekerjaan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Pekerjaan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
