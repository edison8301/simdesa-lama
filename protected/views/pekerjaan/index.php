<?php
$this->breadcrumbs=array(
	'Pekerjaans',
);

$this->menu=array(
	array('label'=>'Tambah Pekerjaan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Pekerjaan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Pekerjaans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
