<?php
$this->breadcrumbs=array(
	'Pendidikans'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Pendidikan','url'=>array('admin'),'icon'=>'list'),
	array('label'=>'Tambah','url'=>array('create'),'icon'=>'plus'),
);

?>

<h1>Pendidikan</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pendidikan-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>
