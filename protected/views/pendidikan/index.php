<?php
$this->breadcrumbs=array(
	'Pendidikans',
);

$this->menu=array(
	array('label'=>'Tambah Pendidikan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Pendidikan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Pendidikans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
