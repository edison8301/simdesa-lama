<?php
$this->breadcrumbs=array(
	'Pendidikans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah Pendidikan','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat Pendidikan','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola Pendidikan','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting Pendidikan</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>