<?php
$this->breadcrumbs=array(
	'Pendidikans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Pendidikan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Pendidikan','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Pendidikan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Pendidikan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Pendidikan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
),
)); ?>
