<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'penduduk-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
			'class'=>'CDataColumn',
			'name'=>'nama',
			'headerHtmlOptions'=>array('style'=>'min-width:200px')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'jenis_kelamin_id',
				'header'=>'Jenis Kelamin',
				'type'=>'raw',
				'value'=>'$data->jenisKelamin->nama',
				'filter'=>CHtml::listData(JenisKelamin::model()->findAll(),'id','nama')
			),
		array(
				'class'=>'CDataColumn',
				'name'=>'status_perkawinan_id',
				'header'=>'Status Perkawinan',
				'type'=>'raw',
				'value'=>'$data->statusPerkawinan->nama',
				'filter'=>CHtml::listData(StatusPerkawinan::model()->findAll(),'id','nama')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'tempat_lahir',
			'header'=>'Tempat Lahir',
			'type'=>'raw',
			'value'=>'$data->tempat_lahir',
			'headerHtmlOptions'=>array('style'=>'min-width:120px')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'tanggal_lahir',
			'header'=>'Tanggal Lahir',
			'type'=>'raw',
			'value'=>'Bantu::tanggalSingkat($data->tanggal_lahir)',
			'headerHtmlOptions'=>array('style'=>'min-width:120px')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'agama_id',
				'header'=>'Agama',
				'type'=>'raw',
				'value'=>'$data->agama->nama',
				'filter'=>CHtml::listData(Agama::model()->findAll(),'id','nama')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'pendidikan_id',
				'header'=>'Pendidikan Terakhir',
				'type'=>'raw',
				'value'=>'$data->pendidikan->nama',
				'filter'=>CHtml::listData(Pendidikan::model()->findAll(),'id','nama')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'pekerjaan_id',
				'header'=>'Pekerjaan',
				'type'=>'raw',
				'value'=>'$data->pekerjaan->nama',
				'filter'=>CHtml::listData(Pekerjaan::model()->findAll(),'id','nama')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'dapat_membaca',
				'header'=>'Dapat Membaca Huruf',
				'type'=>'raw',
				'value'=>'$data->dapat_membaca==1 ? "Ya" : "Tidak"',
				'filter'=>array('1'=>'Ya','2'=>'Tidak')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'kewarganegaraan',
				'header'=>'Kewarganegaraan',
				'type'=>'raw',
				'value'=>'$data->kewarganegaraan->nama',
				'filter'=>CHtml::listData(Kewarganegaraan::model()->findAll(),'id','nama')
		),
		array(	
			'name'=>'alamat',
			'headerHtmlOptions'=>array('style'=>'min-width:200px')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'dusun_id',
				'header'=>'Dusun',
				'type'=>'raw',
				'value'=>'$data->dusun->nama',
				'filter'=>CHtml::listData(Dusun::model()->findAll(),'id','nama')
		),
		'rw',
		'rt',
		array(
				'class'=>'CDataColumn',
				'name'=>'kedudukan_dalam_keluarga_id',
				'header'=>'Kedudukan Dalam Keluarga',
				'type'=>'raw',
				'value'=>'$data->kedudukanDalamKeluarga->nama',
				'filter'=>CHtml::listData(KedudukanDalamKeluarga::model()->findAll(),'id','nama')
		),
		'nomor_ktp',
		'nomor_ksk',
		array(
				'class'=>'CDataColumn',
				'name'=>'mutasi_tambah_id',
				'header'=>'Mutasi Penambahan',
				'type'=>'raw',
				'value'=>'$data->mutasiTambah->nama',
				'filter'=>CHtml::listData(MutasiTambah::model()->findAll(),'id','nama')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'mutasi_tambah_tanggal',
				'header'=>'Tanggal',
				'type'=>'raw',
				'value'=>'Bantu::tanggalSingkat($data->mutasi_tambah_tanggal)',
				
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'mutasi_kurang_id',
				'header'=>'Mutasi Pengurangan',
				'type'=>'raw',
				'value'=>'$data->mutasiKurang->nama',
				'filter'=>CHtml::listData(MutasiKurang::model()->findAll(),'id','nama')
		),
		array(
				'class'=>'CDataColumn',
				'name'=>'mutasi_kurang_tanggal',
				'header'=>'Tanggal',
				'type'=>'raw',
				'value'=>'$data->mutasi_kurang_id == null ? "" : Bantu::tanggalSingkat($data->mutasi_kurang_tanggal)',	
		),
		'keterangan',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}'
		),
		
	),
)); ?>