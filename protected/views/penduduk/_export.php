<?php print CHtml::beginForm(); ?>

<?php print CHtml::label('Jenis Export',''); ?>
<?php print CHtml::dropDownList('jenis_export',isset($_GET['jenis_export']) ? $_GET['jenis_export'] : '',array('1'=>'Seluruh Penduduk','2'=>'Usia Balita','3'=>'Wajib Belajar','4'=>'Pemilih Pemilu')); ?>

<hr>

<?php print CHtml::label('Jenis Kelamin',''); ?>
<?php print CHtml::dropDownList('jenis_kelamin_id','',CHtml::listData(JenisKelamin::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Jenis Kelamin --')); ?>

<?php print CHtml::label('Status Perkawinan',''); ?>
<?php print CHtml::dropDownList('status_perkawinan_id','',CHtml::listData(StatusPerkawinan::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Status Perkawinan --')); ?>

<?php print CHtml::label('Agama',''); ?>
<?php print CHtml::dropDownList('agama_id','',CHtml::listData(Agama::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Agama --')); ?>

<?php print CHtml::label('Pendidikan',''); ?>
<?php print CHtml::dropDownList('pendidikan_id','',CHtml::listData(Pendidikan::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Pendidikan --')); ?>

<?php print CHtml::label('Pekerjaan',''); ?>
<?php print CHtml::dropDownList('pekerjaan_id','',CHtml::listData(Pekerjaan::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Pendidikan --')); ?>

<?php print CHtml::label('Dusun',''); ?>
<?php print CHtml::dropDownList('dusun_id','',CHtml::listData(Dusun::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Dusun --')); ?>

<?php print CHtml::label('RW',''); ?>
<?php print CHtml::textField('rw','',array('class'=>'span2')); ?>

<?php print CHtml::label('RT',''); ?>
<?php print CHtml::textField('rt','',array('class'=>'span2')); ?>


<?php print CHtml::label('Kewarganegaraa',''); ?>
<?php print CHtml::dropDownList('kewarganegaraan_id','',CHtml::listData(Kewarganegaraan::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Kewarganegaraan --')); ?>

<?php print CHtml::label('Kedudukan Dalam Keluarga',''); ?>
<?php print CHtml::dropDownList('kedudukan_dalam_keluarga_id','',CHtml::listData(KedudukanDalamKeluarga::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Kedudukan Keluarga --')); ?>

<?php print CHtml::hiddenField('proses','1'); ?>

<div>&nbsp;</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Export','icon'=>'download-alt white','type'=>'primary')); ?>
</div>

<?php print CHtml::endForm(); ?>