<?php

Yii::app()->clientScript->registerScript('mutasi',"
	
	
	$(document).ready(function() {
		
		if($('#Penduduk_mutasi_tambah_id').val()==2)
		{
			$('.mutasi-datang').show('slow');
		}
		
		if($('#Penduduk_mutasi_tambah_id').val()==1)
		{
			$('.mutasi-datang').hide('slow');
		}
		
		if($('#Penduduk_mutasi_kurang_id').val()==2)
		{
			$('.mutasi-kurang').show('slow');
			$('.mutasi-kurang-keterangan').show('slow');
		}
		
		if($('#Penduduk_mutasi_kurang_id').val()==1)
		{
			$('.mutasi-kurang').hide('slow');
		}
		
	});
	
	$('#Penduduk_mutasi_tambah_id').change(function() {
	
		if($(this).val()==2)
		{
			$('.mutasi-datang').show('slow');
		}
		
		if($(this).val()==1)
		{
			$('.mutasi-datang').hide('slow');
		}
	
	});
	
	$('#Penduduk_mutasi_kurang_id').change(function() {
	
		
		if($(this).val()==0)
		{
			$('.mutasi-kurang').hide('slow');
		}

		if($(this).val()==1)
		{
			$('.mutasi-kurang').show('slow');
			$('.mutasi-kurang-keterangan').hide('slow');
		}
		
		if($(this).val()==2)
		{
			$('.mutasi-kurang').show('slow');
			$('.mutasi-kurang-keterangan').show('slow');
		}
		

	});

");
	
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'penduduk-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php //echo $form->fileFieldRow($model,'foto',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php echo $form->dropDownListRow($model , 'jenis_kelamin_id',CHtml::listData(JenisKelamin::model()->findAll(), 'id', 'nama')); ?>

	<?php echo $form->dropDownListRow($model,'status_perkawinan_id',CHtml::listData(StatusPerkawinan::model()->findAll(), 'id', 'nama')); ?>

	<?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->labelEx($model,'tanggal_lahir'); ?>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'Penduduk[tanggal_lahir]',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'changeMonth'=>true,
				'changeYear'=>true,
				'dateFormat'=>'yy-mm-dd',
				'yearRange'=>'1940:2020',
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;'
			),
			'value'=>$model->tanggal_lahir
	)); ?>
	<?php print $form->error($model,'tanggal'); ?>
	<?php echo $form->dropDownListRow($model,'agama_id',CHtml::listData(Agama::model()->findAll(), 'id', 'nama')); ?>

	<?php echo $form->dropDownListRow($model,'pendidikan_id',CHtml::listData(Pendidikan::model()->findAll(), 'id', 'nama')); ?>

	<?php echo $form->dropDownListRow($model,'pekerjaan_id',CHtml::listData(Pekerjaan::model()->findAll(), 'id', 'nama')); ?>

	<?php echo $form->dropDownListRow($model,'dapat_membaca',array('1'=>'Ya','2'=>'Tidak')); ?>

	<?php echo $form->dropDownListRow($model,'kewarganegaraan_id',CHtml::listData(Kewarganegaraan::model()->findAll(), 'id', 'nama')); ?>

	<?php echo $form->textFieldRow($model,'alamat',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php echo $form->dropDownListRow($model,'dusun_id',CHtml::listData(Dusun::model()->findAll(), 'id', 'nama')); ?>
	
	<?php echo $form->textFieldRow($model,'rw',array('class'=>'span2','maxlength'=>10)); ?>
	
	<?php echo $form->textFieldRow($model,'rt',array('class'=>'span2','maxlength'=>10)); ?>
	
	<?php echo $form->dropDownListRow($model,'kedudukan_dalam_keluarga_id',CHtml::listData(KedudukanDalamKeluarga::model()->findAll(), 'id', 'nama')); ?>

	<?php echo $form->textFieldRow($model,'nomor_ktp',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'nomor_ksk',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>
	
	<div>&nbsp;</div>
	
	<legend>Data Mutasi Penambahan</legend>
	
	<?php echo $form->dropDownListRow($model,'mutasi_tambah_id',CHtml::listData(MutasiTambah::model()->findAll(), 'id', 'nama')); ?>
	
	<div class="mutasi-datang hide">
	
	<?php print $form->labelEx($model,'mutasi_tambah_tanggal'); ?>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'Penduduk[mutasi_tambah_tanggal]',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'changeMonth'=>true,
				'changeYear'=>true,
				'dateFormat'=>'yy-mm-dd',
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;'
			),
			'value'=>$model->mutasi_tambah_tanggal
	)); ?>
	<?php print $form->error($model,'mutasi_tambah_tanggal'); ?>
	
	<?php print $form->textFieldRow($model,'mutasi_tambah_keterangan',array('class'=>'span5')); ?>
	
	</div>
	
	
	<div>&nbsp;</div>
	
	<legend>Data Mutasi Pengurangan</legend>
	
	<?php echo $form->dropDownListRow($model,'mutasi_kurang_id',CHtml::listData(MutasiKurang::model()->findAll(), 'id', 'nama'),array('empty'=>'-- Penduduk Masih Aktif --')); ?>
	
	<div class="mutasi-kurang hide">
	
	<?php print $form->labelEx($model,'mutasi_kurang_tanggal'); ?>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'Penduduk[mutasi_kurang_tanggal]',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'changeMonth'=>true,
				'changeYear'=>true,
				'dateFormat'=>'yy-mm-dd',
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;'
			),
			'value'=>$model->mutasi_kurang_tanggal
	)); ?>
	<?php print $form->error($model,'mutasi_kurang_tanggal'); ?>
	
	<div class="mutasi-kurang-keterangan hide">
	<?php print $form->textFieldRow($model,'mutasi_kurang_keterangan',array('class'=>'span5')); ?>
	</div>
	
	</div>
	
	
	<div>&nbsp;</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
