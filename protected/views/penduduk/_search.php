<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->dropDownListRow($model,'jenis_kelamin_id',CHtml::listData(JenisKelamin::model()->findAll(),'id','nama')); ?>

		<?php echo $form->textFieldRow($model,'status_perkawinan_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tanggal_lahir',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'agama_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'pendidikan_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'pekerjaan_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'dapat_membaca',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'kewarganegaraan_id',array('class'=>'span5')); ?>

		<?php echo $form->textAreaRow($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'kedudukan_dalam_keluarga_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nomor_ktp',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'nomor_ksk',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'icon'=>'search white',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
