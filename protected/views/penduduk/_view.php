<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin_id')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_perkawinan_id')); ?>:</b>
	<?php echo CHtml::encode($data->status_perkawinan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agama_id')); ?>:</b>
	<?php echo CHtml::encode($data->agama_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pendidikan_id')); ?>:</b>
	<?php echo CHtml::encode($data->pendidikan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pekerjaan_id')); ?>:</b>
	<?php echo CHtml::encode($data->pekerjaan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dapat_membaca')); ?>:</b>
	<?php echo CHtml::encode($data->dapat_membaca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kewarganegaraan_id')); ?>:</b>
	<?php echo CHtml::encode($data->kewarganegaraan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kedudukan_dalam_keluarga_id')); ?>:</b>
	<?php echo CHtml::encode($data->kedudukan_dalam_keluarga_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_ktp')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_ksk')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_ksk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	*/ ?>

</div>