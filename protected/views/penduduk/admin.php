<?php
$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Seluruh Penduduk',
);

$this->menu = array(
	array('label'=>'Data Induk Penduduk','url'=>array('penduduk/admin'),'icon'=>'list'),
	array('label'=>'Tambah','url'=>array('penduduk/create'),'icon'=>'plus'),
	array('label'=>'Export Word','url'=>array('penduduk/exportWord'),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/exportExcel'),'icon'=>'download-alt'),

);


?>

<h1>Data Induk Penduduk</h1>


<div style="overflow:auto">
	<?php $this->renderPartial('_admin',array('model'=>$model)); ?>
</div>
