<?php
$this->breadcrumbs=array(
	'Penduduks'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Tambah Penduduk','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Export All','url'=>array('exportall'),'icon'=>'download-alt'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('penduduk-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Penduduk Berdasarkan Agama</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'penduduk-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'nama',
		array(
				'class'=>'CDataColumn',
				'name'=>'agama_id',
				'header'=>'Agama',
				'type'=>'raw',
				'value'=>'$data->agama->nama',
				'filter'=>CHtml::listData(Agama::model()->findAll(),'id','nama')
		),
		/*
		'agama_id',
		'pendidikan_id',
		'pekerjaan_id',
		'dapat_membaca',
		'kewarganegaraan_id',
		'alamat',
		'kedudukan_dalam_keluarga_id',
		'nomor_ktp',
		'nomor_ksk',
		'keterangan',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
