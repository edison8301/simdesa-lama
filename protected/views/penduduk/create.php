<?php
$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Tambah Penduduk','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Seluruh Penduduk','url'=>array('admin'),'icon'=>'tags'),
	array('label'=>'Usia Balita','url'=>array('usiaBalita'),'icon'=>'tags'),
	array('label'=>'Wajib Belajar','url'=>array('wajibBelajar'),'icon'=>'tags'),
	array('label'=>'Pemilih Pemilu','url'=>array('pemilihPemilu'),'icon'=>'tags'),
	array('label'=>'Export Word','url'=>array('exportWord'),'icon'=>'download-alt'),
);


?>

<h1>Tambah Penduduk</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>