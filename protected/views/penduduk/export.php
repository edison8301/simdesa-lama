<h1>Hasil Export Data Penduduk</h1>

<table border="1">
<tr>
	<th style="vertical-align:top">Nama Penduduk</th>
	<td style="vertical-align:top;width:500px"><?php print $model->nama; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Jenis Kelamin</th>
	<td style="vertical-align:top;width:500px"><?php print $model->jenisKelamin->nama; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Status Perkawinan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->statusPerkawinan->nama; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Tempat Lahir</th>
	<td style="vertical-align:top;width:500px"><?php print $model->tempat_lahir; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Tanggal Lahir</th>
	<td style="vertical-align:top;width:500px"><?php print $model->tanggal_lahir; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Agama</th>
	<td style="vertical-align:top;width:500px"><?php print $model->agama->nama; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Pendidikan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->pendidikan->nama; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Pekerjaan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->pekerjaan->nama; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dapat Membaca</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dapat_membaca; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Kewarganegaraan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->kewarganegaraan->nama; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Alamat</th>
	<td style="vertical-align:top;width:500px"><?php print $model->alamat; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Kedudukan Dalam keluarga</th>
	<td style="vertical-align:top;width:500px"><?php print $model->kedudukanDalamKeluarga->nama; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">No KTP</th>
	<td style="vertical-align:top;width:500px"><?php print $model->nomor_ktp; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">No KSK</th>
	<td style="vertical-align:top;width:500px"><?php print $model->nomor_ksk; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Keterangan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->keterangan; ?></td>
</tr>

</table>