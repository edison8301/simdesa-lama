<?php if(isset($_POST['proses']) AND $_POST['proses']=='1') { ?>

<h1>Hasil Export Data Pekerjaan</h1>

<table border="1">
<tr>
	<th>No</th>
	<th>Nama</th>
	<th>Jenis Kelamin</th>
	<th>Status Perkawinan</th>
	<th>Tempat Lahir</th>
	<th>Tanggal lahir</th>
	<th>Agama</th>
	<th>Pendidikan</th>
	<th>Pekerjaan</th>
	<th>Dapat Membaca</th>
	<th>Kewarganegaraan</th>
	<th>Alamat</th>
	<th>Kedudkan Dalam Keluarga</th>
	<th>No KTP</th>
	<th>No KSK</th>
	<th>Keterangan</th>
</tr>
	


<?php
	
	$criteria = new CDbCriteria;
	
	if(isset($_POST['jenis_kelamin_id']) AND $_POST['jenis_kelamin_id']!=null)
		$criteria->addCondition('jenis_kelamin_id='.$_POST['jenis_kelamin_id']);
	
	if(isset($_POST['status_perkawinan_id']) AND $_POST['status_perkawinan_id']!=null)
		$criteria->addCondition('status_perkawinan_id='.$_POST['status_perkawinan_id']);
		
	if(isset($_POST['agama_id']) AND $_POST['agama_id']!=null)
		$criteria->addCondition('agama_id='.$_POST['agama_id']);

	if(isset($_POST['pendidikan_id']) AND $_POST['pendidikan_id']!=null)
		$criteria->addCondition('pendidikan_id='.$_POST['pendidikan_id']);

	if(isset($_POST['pekerjaan_id']) AND $_POST['pekerjaan_id']!=null)
		$criteria->addCondition('pekerjaan_id='.$_POST['pekerjaan_id']);

	if(isset($_POST['kewarganegaraan_id']) AND $_POST['kewarganegaraan_id']!=null)
		$criteria->addCondition('kewarganegaraan_id='.$_POST['kewarganegaraan_id']);

	if(isset($_POST['kedudukan_dalam_keluarga_id']) AND $_POST['kedudukan_dalam_keluarga_id']!=null)
		$criteria->addCondition('kedudukan_dalam_keluarga_id='.$_POST['kedudukan_dalam_keluarga_id']);
		
	$model = Penduduk::model()->findAll($criteria);
	
	if($model!==null)
	{
		$i = 1;
		foreach($model as $data) {

?>
<tr>
	<td style="vertical-align:top"><?php print $i; ?></td>
	<td style="vertical-align:top"><?php print $data->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->jenisKelamin->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->statusPerkawinan->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->tempat_lahir; ?></td>
	<td style="vertical-align:top"><?php print $data->tanggal_lahir; ?></td>
	<td style="vertical-align:top"><?php print $data->agama->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->pendidikan->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->pekerjaan->nama; ?></td>
	<td style="vertical-align:top"><?php $data->dapat_membaca == 1 ? print "Dapat Membaca" : "Tidak Dapat Membaca"; ?></td>
	<td style="vertical-align:top"><?php print $data->kewarganegaraan->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->alamat; ?></td>
	<td style="vertical-align:top"><?php print $data->kedudukanDalamKeluarga->nama; ?></td>
	<td style="vertical-align:top"><?php print $data->nomor_ktp; ?></td>
	<td style="vertical-align:top"><?php print $data->nomor_ksk; ?></td>
	<td style="vertical-align:top"><?php print $data->keterangan; ?></td>
</tr>
	

<?php 	
		$i++; }
	}
?>

</table>

<?php } else { ?>

<h1>Export Penduduk</h1>


<div>&nbsp;</div>


<?php print CHtml::beginForm(array('exportAll')); ?>

<?php print CHtml::label('Jenis Kelamin',''); ?>
<?php print CHtml::dropDownList('jenis_kelamin_id','',CHtml::listData(JenisKelamin::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Jenis Kelamin --')); ?>

<?php print CHtml::label('Status Perkawinan',''); ?>
<?php print CHtml::dropDownList('status_perkawinan_id','',CHtml::listData(StatusPerkawinan::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Status Perkawinan --')); ?>

<?php print CHtml::label('Agama',''); ?>
<?php print CHtml::dropDownList('agama_id','',CHtml::listData(Agama::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Agama --')); ?>

<?php print CHtml::label('Pendidikan',''); ?>
<?php print CHtml::dropDownList('pendidikan_id','',CHtml::listData(Pendidikan::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Pendidikan --')); ?>

<?php print CHtml::label('Pekerjaan',''); ?>
<?php print CHtml::dropDownList('pekerjaan_id','',CHtml::listData(Pekerjaan::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Pendidikan --')); ?>

<?php print CHtml::label('Kewarganegaraa',''); ?>
<?php print CHtml::dropDownList('kewarganegaraan_id','',CHtml::listData(Kewarganegaraan::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Kewarganegaraan --')); ?>

<?php print CHtml::label('Kedudukan Dalam Keluarga',''); ?>
<?php print CHtml::dropDownList('kedudukan_dalam_keluarga_id','',CHtml::listData(KedudukanDalamKeluarga::model()->findAll(),'id','nama'),array('empty'=>'-- Semua Kedudukan Keluarga --')); ?>

<?php print CHtml::hiddenField('proses','1'); ?>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Export','icon'=>'download-alt white','type'=>'primary')); ?>


<?php print CHtml::endForm(); ?>

<?php } ?>
