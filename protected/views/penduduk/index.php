<?php
$this->breadcrumbs=array(
	'Penduduks',
);

$this->menu=array(
	array('label'=>'Tambah Penduduk','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Penduduk','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Penduduks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
