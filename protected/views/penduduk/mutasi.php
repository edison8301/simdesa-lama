<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Kelompok Umur',
);



date_default_timezone_set('Asia/Jakarta');
		
$tanggal_awal = date('Y-m').'-01';
$tanggal_akhir = date('Y-m').'-'.date('t');
		
if(!empty($_POST['tanggal_awal']) AND !empty($_POST['tanggal_akhir']))
{
	$tanggal_awal = $_POST['tanggal_awal'];
	$tanggal_akhir = $_POST['tanggal_akhir'];
}

$this->menu = array(
	array('label'=>'Mutasi Penduduk','url'=>array('penduduk/mutasi'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/mutasiWord','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/mutasiExcel','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir),'icon'=>'download-alt'),
);

function cek($tanggal)
{
	if($tanggal >= $tanggal_awal AND $tanggal <= $tanggal_akhir)
		return true;
	else
		return false;
}

?>

<h1>Mutasi Penduduk <?php print Bantu::tanggalSingkat($tanggal_awal); ?> s.d <?php print Bantu::tanggalSingkat($tanggal_akhir); ?></h1>

<?php print CHtml::beginForm(array('penduduk/mutasi')); ?>

<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'tanggal_awal',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'changeMonth'=>true,
				'changeYear'=>true,
				'dateFormat'=>'yy-mm-dd',
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'placeholder'=>'Tanggal Awal',
				
			),
			'value'=>$tanggal_awal
)); ?>

<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'tanggal_akhir',
			// additional javascript options for the date picker plugin
			'options'=>array(
				'showAnim'=>'fold',
				'changeMonth'=>true,
				'changeYear'=>true,
				'dateFormat'=>'yy-mm-dd',
			),
			'htmlOptions'=>array(
				'style'=>'height:20px;',
				'placeholder'=>'Tanggal Akhir',
			),
			'value'=>$tanggal_akhir
)); ?>

<div>
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','type'=>'primary','icon'=>'search white','label'=>'Filter')); ?>
</div>

<?php print CHtml::endForm(); ?>

<div>&nbsp;</div>

<div style="overflow:auto">
<table class="table">
<tr>
	<th rowspan="2" style="text-align:center">No</th>
	<th rowspan="2" style="text-align:center;min-width:180px">Nama Lengkap / Panggilan</th>
	<th rowspan="2" style="text-align:center;min-width:150px">Tempat Lahir</th>
	<th rowspan="2" style="text-align:center;min-width:120px">Tanggal Lahir</th>
	<th rowspan="2" style="text-align:center;min-width:120px">Jenis Kelamin</th>
	<th rowspan="2" style="text-align:center">Kewarganegaraan</th>
	<th colspan="4" style="text-align:center">Penambahan</th>
	<th colspan="4" style="text-align:center">Pengeluaran</th>
	<th>Keterangan</th>
	<th>&nbsp;</th>
</tr>
<tr>
	<th style="text-align:center;min-width:100px">Datang Dari</th>
	<th style="text-align:center;min-width:100px">Tanggal</th>
	<th style="text-align:center;min-width:100px">Lahir</th>
	<th style="text-align:center;min-width:100px">Tanggal</th>
	<th style="text-align:center;min-width:100px">Pindah Ke</th>
	<th style="text-align:center;min-width:100px">Tanggal</th>
	<th style="text-align:center;min-width:100px">Mati</th>
	<th style="text-align:center;min-width:100px">Tanggal</th>
</tr>


<?php $i=1; foreach(Penduduk::model()->getDataMutasi() as $data) { ?>
<tr>
	<td style="text-align:center"><?php print $i; ?></td>
	<td style="text-align:center"><?php print $data->nama; ?></td>
	<td style="text-align:center"><?php print $data->tempat_lahir; ?></td>
	<td style="text-align:center"><?php print Bantu::tanggalSingkat($data->tanggal_lahir); ?></td>
	<td style="text-align:center"><?php print $data->jenisKelamin->nama; ?></td>
	<td style="text-align:center"><?php print $data->kewarganegaraan->nama; ?></td>
	
	<?php if($data->mutasi_tambah_tanggal >= $tanggal_awal AND $data->mutasi_tambah_tanggal <= $tanggal_akhir) { ?>
	<td style="text-align:center"><?php if($data->mutasi_tambah_id == 2) print $data->mutasi_tambah_keterangan; ?></td>
	<td style="text-align:center"><?php if($data->mutasi_tambah_id == 2) print Bantu::tanggalSingkat($data->mutasi_tambah_tanggal); ?></td>
	<td style="text-align:center"><?php if($data->mutasi_tambah_id == 1) print "Lahir" ?></td>
	<td style="text-align:center"><?php if($data->mutasi_tambah_id == 1) print Bantu::tanggalSingkat($data->mutasi_tambah_tanggal); ?></td>
	<?php } else { ?>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<?php } ?>
	
	<?php if($data->mutasi_kurang_tanggal >= $tanggal_awal AND $data->mutasi_kurang_tanggal <= $tanggal_akhir) { ?>
	<td style="text-align:center"><?php if($data->mutasi_kurang_id == 2) print $data->mutasi_kurang_keterangan; ?></td>
	<td style="text-align:center"><?php if($data->mutasi_kurang_id == 2) print Bantu::tanggalSingkat($data->mutasi_kurang_tanggal); ?></td>
	<td style="text-align:center"><?php if($data->mutasi_kurang_id == 1) print "Mati" ?></td>
	<td style="text-align:center"><?php if($data->mutasi_kurang_id == 1) print Bantu::tanggalSingkat($data->mutasi_kurang_tanggal); ?></td>
	<?php } else { ?>
	
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<?php } ?>
	
	<td>&nbsp;</td>
	<td><?php print CHtml::link("<i class='icon-pencil'></i>",array('penduduk/update','id'=>$data->id)); ?></td>
	
</tr>

<?php $i++; } ?>

</table>
</div>