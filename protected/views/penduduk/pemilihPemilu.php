<?php
$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Kelola',
);

$this->menu = array(
	array('label'=>'Pemilih Pemilu','url'=>array('penduduk/pemilihPemily'),'icon'=>'list'),
	array('label'=>'Tambah','url'=>array('penduduk/create'),'icon'=>'plus'),
	array('label'=>'Export Word','url'=>array('penduduk/exportWord','jenis_export'=>4),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/exportExcel','jenis_export'=>4),'icon'=>'download-alt'),
);

?>

<h1>Pemilih Pemilu</h1>


<div style="overflow:auto">
	<?php $this->renderPartial('_admin',array('model'=>$model)); ?>
</div>