<?php
$this->breadcrumbs=array(
	'Penduduks'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Tambah Penduduk','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Export All','url'=>array('exportall'),'icon'=>'download-alt'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('penduduk-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Penduduk Berdasarkan Pendidikan</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'penduduk-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'nama',
		array(
				'class'=>'CDataColumn',
				'name'=>'pendidikan_id',
				'header'=>'Pendidikan',
				'type'=>'raw',
				'value'=>'$data->pendidikan->nama',
				'filter'=>CHtml::listData(Pendidikan::model()->findAll(),'id','nama')
			),
		/*
		array(
				'class'=>'CDataColumn',
				'name'=>'status_perkawinan_id',
				'header'=>'Status Perkawinan',
				'type'=>'raw',
				'value'=>'$data->statusPerkawinan->nama',
				'filter'=>CHtml::listData(StatusPerkawinan::model()->findAll(),'id','nama')
			),
		'tempat_lahir',
		'tanggal_lahir',
		/*
		'agama_id',
		'pendidikan_id',
		'pekerjaan_id',
		'dapat_membaca',
		'kewarganegaraan_id',
		'alamat',
		'kedudukan_dalam_keluarga_id',
		'nomor_ktp',
		'nomor_ksk',
		'keterangan',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
