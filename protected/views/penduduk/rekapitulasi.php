<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Kelompok Umur',
);

$interval = 1;
if(isset($_GET['interval'])) $interval = $_GET['interval'];

$this->menu = array(
	array('label'=>'Rekapitulasi Jumlah Penduduk','url'=>array('penduduk/rekapitulasi'),'icon'=>'list'),
	array('label'=>'Export Excel','url'=>array('penduduk/rekapitulasiExcel'),'icon'=>'download-alt'),
	//array('label'=>'Export Word','url'=>array('penduduk/reportKelompokUmurWord','interval'=>$interval),'icon'=>'download-alt'),
	//array('label'=>'Export Excel','url'=>array('penduduk/reportKelompokUmurExcel','interval'=>$interval),'icon'=>'download-alt'),
);

Yii::app()->clientScript->registerScript('autosubmit',"
	$('.interval').change(function() {
	
		$('.form-interval').submit();
		
	});
	
");

?>

<h1>Rekapitulasi Jumlah Penduduk</h1>

<div>&nbsp;</div>

<div style="overflow:auto">
<table class="table" border="1">
<thead border=1>
<tr>
	<th rowspan="4" style="text-align:center">No</th>
	<th rowspan="4" style="text-align:center">Nama Dusun / Lingkungan</th>
	<th colspan="7" style="text-align:center">Jumlah Penduduk Awal Bulan</th>
	<th colspan="8" style="text-align:center">Tambahan Bulan Ini</th>
	<th colspan="8" style="text-align:center">Pengurangan Bulan Ini</th>
	<th colspan="7" rowspan="2" style="text-align:center">Jumlah Penduduk Akhir Bulan Ini</th>
	<th rowspan="4" style="text-align:center">Keterangan</th>
</tr>
<tr>
	<th rowspan="3" style="text-align:center">Jml KK</th>
	<th colspan="2">WNA</th>
	<th colspan="2">WNI</th>
	<th rowspan="3" style="text-align:center">Jml Anggota Keluarga</th>
	<th rowspan="3" style="text-align:center">Jml Jiwa</th>
	<th colspan="4" style="text-align:center">Lahir</th>
	<th colspan="4" style="text-align:center">Datang</th>
	<th colspan="4" style="text-align:center">Mati</th>
	<th colspan="4" style="text-align:center">Pindah</th>
</tr>
<tr>
	<th rowspan="2">L</th>
	<th rowspan="2">P</th>
	<th rowspan="2">L</th>
	<th rowspan="2">P</th>
	<th colspan="2">WNA</th>
	<th colspan="2">WNI</th>
	<th colspan="2">WNA</th>
	<th colspan="2">WNI</th>
	<th colspan="2">WNA</th>
	<th colspan="2">WNI</th>
	<th colspan="2">WNA</th>
	<th colspan="2">WNI</th>
	<th colspan="2">WNA</th>
	<th colspan="2">WNI</th>
	<th rowspan="2">Jml KK</th>
	<th rowspan="2" style="text-align:center">Jml Anggota Keluarga</th>
	<th rowspan="2" style="text-align:center">Jml Jiwa (3+8)</th>
</tr>
<tr>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
	<th>L</th>
	<th>P</th>
</tr>
<?php

	$tanggal_awal = date('Y-m').'-01';
	$tanggal_akhir = date('Y-m').'-'.date('t');
		
	if(!empty($_POST['tanggal_awal'])) $tanggal_awal = $_POST['tanggal_awal'];
	
	if(!empty($_POST['tanggal_akhir'])) $tanggal_akhir = $_POST['tanggal_akhir'];
		
	if(!empty($_GET['tanggal_awal'])) $tanggal_awal = $_GET['tanggal_awal'];
	
	if(!empty($_GET['tanggal_akhir'])) $tanggal_akhir = $_GET['tanggal_akhir'];

?>
<?php $i=1; foreach(Dusun::model()->findAll() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->nama; ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'awal','kedudukan_dalam_keluarga'=>'kk','tanggal_awal'=>$tanggal_awal)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'awal','tanggal_awal'=>$tanggal_awal,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'awal','tanggal_awal'=>$tanggal_awal,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'awal','tanggal_awal'=>$tanggal_awal,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'awal','tanggal_awal'=>$tanggal_awal,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'awal','kedudukan_dalam_keluarga'=>'nonkk','tanggal_awal'=>$tanggal_awal)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'awal','tanggal_awal'=>$tanggal_awal)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_tambah_id'=>1,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_tambah_id'=>1,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_tambah_id'=>1,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_tambah_id'=>1,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_tambah_id'=>2,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_tambah_id'=>2,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_tambah_id'=>2,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_tambah_id'=>2,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_kurang_id'=>1,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_kurang_id'=>1,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_kurang_id'=>1,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_kurang_id'=>1,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_kurang_id'=>2,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_kurang_id'=>2,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_kurang_id'=>2,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countMutasi(array('status'=>'tengah','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir,'mutasi_kurang_id'=>2,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'akhir','tanggal_akhir'=>$tanggal_akhir,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'akhir','tanggal_akhir'=>$tanggal_akhir,'kewarganegaraan_id'=>2,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'akhir','tanggal_akhir'=>$tanggal_akhir,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>1)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'akhir','tanggal_akhir'=>$tanggal_akhir,'kewarganegaraan_id'=>1,'jenis_kelamin_id'=>2)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'akhir','kedudukan_dalam_keluarga'=>'kk','tanggal_akhir'=>$tanggal_akhir)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'akhir','kedudukan_dalam_keluarga'=>'nonkk','tanggal_akhir'=>$tanggal_akhir)); ?></td>
	<td><?php print $data->countRekapitulasi(array('status'=>'akhir','tanggal_akhir'=>$tanggal_akhir)); ?></td>
	<td>&nbsp;</div>
</tr>

<?php $i++; } ?>
</table>

</div>
