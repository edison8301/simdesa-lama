<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

?>

<h1>Laporan Penduduk Berdasarkan Pekerjaan</h1>

<table class="table">
<tr>
	<th>No</th>
	<th>Kategori</th>
	<th>Jumlah</th>
</tr>
<?php $i=1; foreach(Pekerjaan::model()->findAll() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->nama; ?></td>
	<td><?php print $data->countPenduduk(); ?></td>
</tr>
<?php $i++; } ?>
