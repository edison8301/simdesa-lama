<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

$this->menu = array(
	array('label'=>'Laporan Agama','url'=>array('penduduk/reportAgama'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/reportWord','kategori'=>'agama'),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/reportExcel','kategori'=>'agama'),'icon'=>'download-alt'),
);

?>

<h1>Laporan Agama</h1>

<div>&nbsp;</div>

<?php 
	$model = new Agama;
	$this->renderPartial('_report',array('model'=>$model)); 
?>
