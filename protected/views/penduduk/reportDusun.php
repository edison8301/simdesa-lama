<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

$this->menu = array(
	array('label'=>'Laporan Dusun','url'=>array('penduduk/reportDusun'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/reportWord','kategori'=>'dusun'),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/reportExcel','kategori'=>'dusun'),'icon'=>'download-alt'),
);

?>

<h1>Laporan Dusun</h1>

<div>&nbsp;</div>

<?php 
	$model = new Dusun;
	$this->renderPartial('_report',array('model'=>$model)); 
?>
