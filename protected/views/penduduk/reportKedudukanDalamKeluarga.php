<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

$this->menu = array(
	array('label'=>'Laporan Kedudukan Dalam Keluarga','url'=>array('penduduk/reportKedudukanDalamKeluarga'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/reportWord','kategori'=>'kedudukanDalamKeluarga'),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/reportExcel','kategori'=>'kedudukanDalamKeluarga'),'icon'=>'download-alt'),
);

?>

<h1>Laporan Kedudukan Dalam Keluarga</h1>

<div>&nbsp;</div>

<?php 
	$model = new KedudukanDalamKeluarga;
	$this->renderPartial('_report',array('model'=>$model)); 
?>
	