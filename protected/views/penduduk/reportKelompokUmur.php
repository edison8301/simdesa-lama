<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Kelompok Umur',
);

$interval = 1;
if(isset($_GET['interval'])) $interval = $_GET['interval'];

$this->menu = array(
	array('label'=>'Laporan Kelompok Umur','url'=>array('penduduk/reportKelompokUmur'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/reportKelompokUmurWord','interval'=>$interval),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/reportKelompokUmurExcel','interval'=>$interval),'icon'=>'download-alt'),
);

Yii::app()->clientScript->registerScript('autosubmit',"
	$('.interval').change(function() {
	
		$('.form-interval').submit();
		
	});
	
");

?>

<h1>Laporan Kelompok Umur</h1>

<?php print CHtml::beginForm(array('penduduk/reportKelompokUmur'),'GET',array('class'=>'form-interval')); ?>

<?php print CHtml::label('Interval Umur',''); ?>
<?php print CHtml::dropDownList('interval',$_GET['interval'],array('1'=>'1 Tahun','5'=>'5 Tahun','10'=>'10 Tahun'),array('class'=>'interval')); ?>

<?php print CHtml::endForm(); ?>

<table class="table">
<tr>
	<th rowspan="2" style="text-align:center">NO</th>
	<th rowspan="2" style="text-align:center">KATEGORI</th>
	<th colspan="2" style="text-align:center">JENIS KELAMIN</th>
	<th colspan="2" style="text-align:center">TOTAL</th>
</tr>
<tr>
	<th style="text-align:center">LAKI-LAKI</th>
	<th style="text-align:center">PEREMPUAN</th>
</tr>

<?php $i=1; $total_lk = 0; $total_pr = 0; $total_lk_pr = 0; $umur=0; $interval = 1; ?>

<?php if(isset($_GET['interval'])) $interval = $_GET['interval']; ?>

<?php while($umur<=100) { ?>

<?php
	
	$lk = Penduduk::model()->countByIntervalUmur($umur,$umur+$interval,'laki-laki'); 
	$pr = Penduduk::model()->countByIntervalUmur($umur,$umur+$interval,'perempuan');
	$lk_pr = $pr + $lk;
	
	$total_lk = $total_lk + $lk;
	$total_pr = $total_pr + $pr;
	$total_lk_pr = $total_lk_pr + $lk_pr;
		
?>

<tr>
	<td style="text-align:center"><?php print $i; ?></td>
	<td style="text-align:center"><?php print $umur; ?> - <?php print $umur+$interval; ?> Tahun</td>
	<td style="text-align:center"><?php print $lk; ?></td>
	<td style="text-align:center"><?php print $pr; ?></td>
	<td style="text-align:center"><?php print $lk_pr; ?></td>
</tr>

<?php $i++; $umur = $umur+$interval;  } ?>

<tr>
	<td>&nbsp;</td>
	<th style="text-align:center">TOTAL</th>
	<td style="text-align:center"><?php print $total_lk; ?></td>
	<td style="text-align:center"><?php print $total_pr; ?></td>
	<td style="text-align:center"><?php print $total_lk_pr; ?></td>
</tr>
</table>
