<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

?>

<h1>Laporan Penduduk Berdasarkan Pendidikan</h1>

<?php 
	$model = new Pendidikan;
	$this->renderPartial('_report',array('model'=>$model)); 
?>
	