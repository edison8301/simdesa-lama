<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

$this->menu = array(
	array('label'=>'Laporan Kewarganegaraan','url'=>array('penduduk/reportKewarganegaraan'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/reportWord','kategori'=>'kewarganegaraan'),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/reportExcel','kategori'=>'kewarganegaraan'),'icon'=>'download-alt'),
);

?>

<h1>Laporan Kewarganegaraan</h1>

<div>&nbsp;</div>

<?php 
	$model = new Kewarganegaraan;
	$this->renderPartial('_report',array('model'=>$model)); 
?>
