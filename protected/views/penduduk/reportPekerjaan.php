<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

$this->menu = array(
	array('label'=>'Laporan Pekerjaan','url'=>array('penduduk/reportPekerjaan'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/reportWord','kategori'=>'pekerjaan'),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/reportExcel','kategori'=>'pekerjaan'),'icon'=>'download-alt'),
);

?>

<h1>Laporan Pekerjaan</h1>

<div>&nbsp;</div>

<?php
	$this->widget('ext.fusioncharts.fusionChartsWidget', array( 
        'chartNoCache'=>true, // disabling chart cache
        'chartAction'=>Yii::app()->controller->createUrl('penduduk/chart',array('kategori'=>'pekerjaan')), // the chart action that we just generated the xml data at
        //'chartAction'=>'http://simpeg.bandung.lan.go.id/index.php?r=site/chartPendidikan',
		'chartId'=>'pekerjaan',
		'chartWidth'=>600,
		'chartType'=>'Column3D',
		'chartTransparent'=>true,
	)); // If you display more then one chart on a single page then make sure you specify and id	
	
?>



<?php 
	$model = new Pekerjaan;
	$this->renderPartial('_report',array('model'=>$model)); 
?>
