<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

$this->menu = array(
	array('label'=>'Laporan Pendidikan','url'=>array('penduduk/reportPendidikan'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/reportWord','kategori'=>'pendidikan'),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/reportExcel','kategori'=>'pendidikan'),'icon'=>'download-alt'),
);


?>

<h1>Laporan Pendidikan</h1>

<div>&nbsp;</div>

<?php 
	$model = new Pendidikan;
	$this->renderPartial('_report',array('model'=>$model)); 
?>
	