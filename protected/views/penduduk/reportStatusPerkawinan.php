<?php

$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Laporan Pendidikan',
);

$this->menu = array(
	array('label'=>'Laporan Status Perkawinan','url'=>array('penduduk/reportStatusPerkawinan'),'icon'=>'list'),
	array('label'=>'Export Word','url'=>array('penduduk/reportWord','kategori'=>'statusPerkawinan'),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/reportExcel','kategori'=>'statusPerkawinan'),'icon'=>'download-alt'),
);

?>

<h1>Laporan Status Perkawinan</h1>

<div>&nbsp;</div>

<?php 
	$model = new StatusPerkawinan;
	$this->renderPartial('_report',array('model'=>$model)); 
?>
