<?php
$this->breadcrumbs=array(
	'Penduduks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah Penduduk','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat Penduduk','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola Penduduk','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting Penduduk</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>