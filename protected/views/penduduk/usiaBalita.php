<?php
$this->breadcrumbs=array(
	'Penduduk'=>array('admin'),
	'Kelola',
);

$this->menu = array(
	array('label'=>'Usia Balita','url'=>array('penduduk/usiaBalita'),'icon'=>'list'),
	array('label'=>'Tambah','url'=>array('penduduk/create'),'icon'=>'plus'),
	array('label'=>'Export Word','url'=>array('penduduk/exportWord','jenis_export'=>2),'icon'=>'download-alt'),
	array('label'=>'Export Excel','url'=>array('penduduk/exportExcel','jenis_export'=>2),'icon'=>'download-alt'),

);

?>

<h1>Penduduk Usia Balita</h1>


<div style="overflow:auto">
	<?php $this->renderPartial('_admin',array('model'=>$model)); ?>
</div>
