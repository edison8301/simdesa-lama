<?php
$this->breadcrumbs=array(
	'Penduduks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Penduduk','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Penduduk','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Penduduk','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Penduduk','url'=>array('admin'),'icon'=>'th-list'),
	array('label'=>'Buat Surat','url'=>array('surat/create','penduduk_id'=>$model->id),'icon'=>'envelope'),
	array('label'=>'Export Single','url'=>array('export','id'=>$model->id),'icon'=>'download-alt'),
	
);
?>

<h1>Lihat Penduduk</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'nama',
		array(
			'label'=>'Jenis Kelamin',
			'type'=>'raw',
			'value'=>CHtml::encode($model->jenisKelamin->nama)
		),

		array(
				'label'=>'Status Perkawinan',
				'type'=>'raw',
				'value'=>CHtml::encode($model->statusPerkawinan->nama)
			),
		
		'tempat_lahir',
		'tanggal_lahir',

		array(
				'label'=>'Agama',
				'type'=>'raw',
				'value'=>CHtml::encode($model->agama->nama)
			),

		array(
				'label'=>'Pendidikan',
				'type'=>'raw',
				'value'=>CHtml::encode($model->pendidikan->nama)
			),

		array(
				'label'=>'Pekerjaan',
				'type'=>'raw',
				'value'=>CHtml::encode($model->pekerjaan->nama)
			),
		array(
			'label'=>'Dapat Membaca',
			'type'=>'raw',
			'value'=>$model->dapat_membaca == 1 ? "Ya" : "Tidak"
		),
		array(
				'label'=>'Kewarganegaraan',
				'type'=>'raw',
				'value'=>CHtml::encode($model->kewarganegaraan->nama)
			),

		'alamat',

		array(
				'label'=>'Kedudukan Dalam Keluarga',
				'type'=>'raw',
				'value'=>CHtml::encode($model->kedudukanDalamKeluarga->nama)
			),
		
		'nomor_ktp',
		'nomor_ksk',
		'keterangan',
),
)); ?>
