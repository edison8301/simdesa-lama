<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin_id')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_identitas')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_identitas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->pekerjaan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kebangsaan')); ?>:</b>
	<?php echo CHtml::encode($data->kebangsaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keturunan')); ?>:</b>
	<?php echo CHtml::encode($data->keturunan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datang_dari')); ?>:</b>
	<?php echo CHtml::encode($data->datang_dari); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maksud_kedatangan')); ?>:</b>
	<?php echo CHtml::encode($data->maksud_kedatangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_alamat_tujuan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_alamat_tujuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datang_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->datang_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pergi_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->pergi_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	*/ ?>

</div>