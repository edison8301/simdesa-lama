<?php
$this->breadcrumbs=array(
	'Penduduk Sementaras'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('penduduk-sementara-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Penduduk Sementara</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'penduduk-sementara-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			'jenis_kelamin_id',
			'nomor_identitas',
			'tempat_lahir',
			'tanggal_lahir',
			'pekerjaan',
			'kebangsaan',
			'keturunan',
			'datang_dari',
			'maksud_kedatangan',
			'nama_alamat_tujuan',
			'datang_tanggal',
			'pergi_tanggal',
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>

</div>
