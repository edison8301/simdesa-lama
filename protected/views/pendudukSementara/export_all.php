<?php
$this->breadcrumbs=array(
	'Penduduk Sementaras'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah PendudukSementara','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting PendudukSementara','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus PendudukSementara','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola PendudukSementara','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat PendudukSementara</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
		'jenis_kelamin_id',
		'nomor_identitas',
		'tempat_lahir',
		'tanggal_lahir',
		'pekerjaan',
		'kebangsaan',
		'keturunan',
		'datang_dari',
		'maksud_kedatangan',
		'nama_alamat_tujuan',
		'datang_tanggal',
		'pergi_tanggal',
		'keterangan',
),
)); ?>
