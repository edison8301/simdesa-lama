<?php
$this->breadcrumbs=array(
	'Penduduk Sementaras',
);

$this->menu=array(
	array('label'=>'Tambah PendudukSementara','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola PendudukSementara','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Penduduk Sementaras</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
