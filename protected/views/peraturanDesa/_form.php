<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'peraturan-desa-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'tanggal',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>

	<?php echo $form->textFieldRow($model,'tentang',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'uraian_singkat',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'nomor_persetujuan',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'tanggal_persetujuan',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	
	<?php echo $form->textFieldRow($model,'nomor_dilaporkan',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->datepickerRow($model,'tanggal_dilaporkan',array(
					'options'=>array(
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true,
						'changeYear'=>true,
						'changeMonth'=>true
					),
					'prepend'=>'<i class="icon-calendar"></i>'
	)); ?>
	
	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
