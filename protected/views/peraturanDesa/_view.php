<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tentang')); ?>:</b>
	<?php echo CHtml::encode($data->tentang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uraian_singkat')); ?>:</b>
	<?php echo CHtml::encode($data->uraian_singkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_persetujuan')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_persetujuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_persetujuan')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_persetujuan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_dilaporkan')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_dilaporkan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_dilaporkan')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_dilaporkan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />

	*/ ?>

</div>