<?php
$this->breadcrumbs=array(
	'Peraturan Desa'=>array('admin'),
	'Kelola',
);

?>

<h1>Peraturan Desa</h1>


<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'peraturan-desa-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nomor',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal',
				'header'=>'Tanggal',
				'value'=>'Bantu::tanggalSingkat($data->tanggal)'
			),
			
			'tentang',
			'uraian_singkat',
			'nomor_persetujuan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_persetujuan',
				'header'=>'Tanggal Persetujuan',
				'value'=>'Bantu::tanggalSingkat($data->tanggal_persetujuan)'
			),
			'nomor_dilaporkan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_dilaporkan',
				'header'=>'Tanggal Dilaporkan',
				'value'=>'Bantu::tanggalSingkat($data->tanggal_dilaporkan)'
			),
			'keterangan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
