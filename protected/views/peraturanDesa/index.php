<?php
$this->breadcrumbs=array(
	'Peraturan Desas',
);

$this->menu=array(
	array('label'=>'Tambah PeraturanDesa','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola PeraturanDesa','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Peraturan Desas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
