<?php
$this->breadcrumbs=array(
	'Peraturan Desas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah PeraturanDesa','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting PeraturanDesa','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus PeraturanDesa','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola PeraturanDesa','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat PeraturanDesa</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nomor',
		'tanggal',
		'tentang',
		'uraian_singkat',
		'nomor_persetujuan',
		'tanggal_persetujuan',
		'nomor_dilaporkan',
		'tanggal_dilaporkan',
		'keterangan',
),
)); ?>
