<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'rencana-pembangunan-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_proyek',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'lokasi',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'dana_pemerintah',array('class'=>'span2','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'dana_provinsi',array('class'=>'span2','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'dana_kabkota',array('class'=>'span2','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'dana_swadaya',array('class'=>'span2','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'jumlah',array('class'=>'span2','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'pelaksana',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'manfaat',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
