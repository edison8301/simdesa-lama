<?php
$this->breadcrumbs=array(
	'Rencana Pembangunans'=>array('admin'),
	'Kelola',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('rencana-pembangunan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Rencana Pembangunan</h1>

<div style="overflow:auto">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'rencana-pembangunan-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
			'name'=>'nama_proyek',
			'headerHtmlOptions'=>array('style'=>'min-width:200px')
		),
		'lokasi',
		'dana_pemerintah',
		'dana_provinsi',
		'dana_kabkota',
		'dana_swadaya',
		array(
			'class'=>'CDataColumn',
			'name'=>'jumlah',
			'header'=>'Jumlah',
			'type'=>'raw',
			'value'=>'$data->jumlah'
		),
		'pelaksana',
		'manfaat',
		'keterangan',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}'
		),
	),
)); ?>
</div>
