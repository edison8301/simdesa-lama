<h1>Hasil Export Data Penduduk</h1>

<table border="1">
<tr>
	<th style="vertical-align:top">Nama Penduduk</th>
	<td style="vertical-align:top;width:500px"><?php print $model->nama_proyek; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Lokasi</th>
	<td style="vertical-align:top;width:500px"><?php print $model->lokasi; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dana Pemerintah</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dana_pemerintah; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dana Provinsi</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dana_provinsi; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dana KabKota</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dana_kabkota; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Dana Swadaya</th>
	<td style="vertical-align:top;width:500px"><?php print $model->dana_swadaya; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Jumlah</th>
	<td style="vertical-align:top;width:500px"><?php print $model->jumlah; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Pelaksana</th>
	<td style="vertical-align:top;width:500px"><?php print $model->pelaksana; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Manfaat</th>
	<td style="vertical-align:top;width:500px"><?php print $model->manfaat; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Keterangan</th>
	<td style="vertical-align:top;width:500px"><?php print $model->keterangan; ?></td>
</tr>
<tr>
	<th style="vertical-align:top">Tahun</th>
	<td style="vertical-align:top;width:500px"><?php print $model->tahun; ?></td>
</tr>
</table>