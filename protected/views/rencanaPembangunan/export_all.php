<?php if(isset($_POST['proses']) AND $_POST['proses']=='1') { ?>

<h1>Hasil Export Rencana Pembangunan</h1>

<table border="1">
<tr>
	<th>No</th>
	<th>Nama Proyek</th>
	<th>Lokasi</th>
	<th>Dana Pemerintah</th>
	<th>Dana Provinsi</th>
	<th>Dana KabKota</th>
	<th>Dana Swadaya</th>
	<th>Jumlah</th>
	<th>Pelaksana</th>
	<th>Manfaat</th>
	<th>Keterangan</th>
	<th>Tahun</th>
</tr>
	


<?php
	
	$criteria = new CDbCriteria;
	
	if(isset($_POST['jenis_kelamin_id']) AND $_POST['jenis_kelamin_id']!=null)
		$criteria->addCondition = 'jenis_kelamin_id='.$_POST['jenis_kelamin_id'];
		
	$model = RencanaPembangunan::model()->findAll($criteria);
	
	if($model!==null)
	{
		$i = 1;
		foreach($model as $data) {

?>
<tr>
	<td style="vertical-align:top"><?php print $i; ?></td>
	<td style="vertical-align:top"><?php print $data->nama_proyek; ?></td>
	<td style="vertical-align:top"><?php print $data->lokasi; ?></td>
	<td style="vertical-align:top"><?php print $data->dana_pemerintah; ?></td>
	<td style="vertical-align:top"><?php print $data->dana_provinsi; ?></td>
	<td style="vertical-align:top"><?php print $data->dana_kabkota; ?></td>
	<td style="vertical-align:top"><?php print $data->dana_swadaya; ?></td>
	<td style="vertical-align:top"><?php print $data->jumlah; ?></td>
	<td style="vertical-align:top"><?php print $data->pelaksana; ?></td>
	<td style="vertical-align:top"><?php print $data->manfaat; ?></td>
	<td style="vertical-align:top"><?php print $data->keterangan; ?></td>
	<td style="vertical-align:top"><?php print $data->tahun; ?></td>
</tr>
	

<?php 	
		$i++; }
	}
?>

</table>

<?php } else { ?>

<h1>Export Rencana Pembangunan</h1>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'buttons'=>array(
		array('label'=>'Pilihan Menu','items'=>array(
			array('label'=>'Tambah','icon'=>'plus','url'=>array('/rencanaPembangunan/create')),
			array('label'=>'Kelola','icon'=>'th-list','url'=>array('/rencanaPembangunan/admin')),
		)),
    ),
)); ?>

<div>&nbsp;</div>


<?php print CHtml::beginForm(array('exportAll')); ?>

<?php print CHtml::label('Tahun',''); ?>
<?php print CHtml::textField('tahun',''); ?>

<?php print CHtml::hiddenField('proses','1'); ?>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Export','icon'=>'download-alt white','type'=>'primary')); ?>


<?php print CHtml::endForm(); ?>

<?php } ?>
