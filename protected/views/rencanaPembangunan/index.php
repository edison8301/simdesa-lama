<?php
$this->breadcrumbs=array(
	'Rencana Pembangunans',
);

$this->menu=array(
	array('label'=>'Tambah RencanaPembangunan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola RencanaPembangunan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Rencana Pembangunans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
