<?php
$this->breadcrumbs=array(
	'Rencana Pembangunan'=>array('admin'),
	'Sunting',
);

$this->menu=array(
		array('label'=>'Tambah RencanaPembangunan','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat RencanaPembangunan','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola RencanaPembangunan','url'=>array('admin'),'icon'=>'th-list'),
);

?>

	<h1>Sunting Rencana Pembangunan</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>