<?php
$this->breadcrumbs=array(
	'Rencana Pembangunans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Rencana Pembangunan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Rencana Pembangunan','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Rencana Pembangunan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Rencana Pembangunan','url'=>array('admin'),'icon'=>'th-list'),
	array('label'=>'Export','url'=>array('export','id'=>$model->id),'icon'=>'download-alt'),
	
);
?>

<h1>Lihat Rencana Pembangunan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nama_proyek',
			'lokasi',
			array(
				'label'=>'Sumber Dana Pemerintah',
				'value'=>Bantu::rp($model->dana_pemerintah)
			),
			array(
				'label'=>'Sumber Dana Provinsi',
				'value'=>Bantu::rp($model->dana_provinsi)
			),
			array(
				'label'=>'Sumber Dana Kab/Kota',
				'value'=>Bantu::rp($model->dana_kabkota)
			),
			array(
				'label'=>'Sumber Dana Swadaya',
				'value'=>Bantu::rp($model->dana_swadaya)
			),
			array(
				'label'=>'Jumlah',
				'value'=>Bantu::rp($model->jumlah)
			),
			'pelaksana',
			'manfaat',
			'keterangan',
			'tahun',
		),
)); ?>
