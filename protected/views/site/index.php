<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

$this->menu=array(
	array('label'=>'Data Induk Penduduk','url'=>array('penduduk/admin'),'icon'=>'tags'),
	array('label'=>'Buku Kas Umum','url'=>array('kas/umum'),'icon'=>'tags'),
	array('label'=>'Rencana Pembangunan','url'=>array('rencanaPembangunan/admin'),'icon'=>'tags'),
	array('label'=>'Anggota BPD','url'=>array('anggotaBpd/admin'),'icon'=>'tags'),
	array('label'=>'Tanah','url'=>array('tanah/admin'),'icon'=>'tags'),
	array('label'=>'Surat','url'=>array('surat/admin'),'icon'=>'tags'),
);


?>

<h1>Selamat Datang di Aplikasi SIM Desa</h1>

<p>Silahkan gunakan menu navigasi pada bagian atas atau
samping halaman untuk mengakses data aplikasi yang dibutuhkan</p>

<p><img src="<?php print Yii::app()->request->baseUrl; ?>/images/logo.png"></p>

<table class="table">

<tr>
	<th>Nama Desa</th>
	<td><?php print Pengaturan::model()->findByPk(1)->nilai; ?></td>
</tr>
<tr>
	<th>Alamat</th>
	<td><?php print Pengaturan::model()->findByPk(2)->nilai; ?></td>
</tr>
<tr>
	<th>Kecamatan</th>
	<td><?php print Pengaturan::model()->findByPk(3)->nilai; ?></td>
</tr>
<tr>
	<th>Telepon</th>
	<td><?php print Pengaturan::model()->findByPk(4)->nilai; ?></td>
</tr>
<tr>
	<th>Website</th>
	<td><?php print Pengaturan::model()->findByPk(5)->nilai; ?></td>
</tr>
<tr>
	<th>Email</th>
	<td><?php print Pengaturan::model()->findByPk(6)->nilai; ?></td>
</tr>
</table>
