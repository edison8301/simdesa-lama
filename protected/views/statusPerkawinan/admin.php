<?php
$this->breadcrumbs=array(
	'Status Perkawinan'=>array('admin'),
	'Kelola',
);

?>

<h1>Status Perkawinan</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'status-perkawinan-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
