<?php
$this->breadcrumbs=array(
	'Status Perkawinans',
);

$this->menu=array(
	array('label'=>'Tambah StatusPerkawinan','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola StatusPerkawinan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Status Perkawinans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
