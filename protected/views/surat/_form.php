<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'surat-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span5')); ?>

	<?php echo $form->hiddenField($model,'penduduk_id',array('value'=>isset($_GET['penduduk_id']) ? $_GET['penduduk_id'] : $model->penduduk_id)); ?>

	<?php echo $form->dropDownListRow($model,'surat_template_id',CHtml::listData(SuratTemplate::model()->findAll(),'id','nama')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
