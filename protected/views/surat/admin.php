<?php
$this->breadcrumbs=array(
	'Surat'=>array('admin'),
	'Kelola',
);

$this->menu = array(
	array('label'=>'Surat','url'=>array('surat/admin'),'icon'=>'list'),
	array('label'=>'Tambah','url'=>array('surat/pilihPenduduk'),'icon'=>'plus'),
);

?>

<h1>Surat</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'surat-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'nomor',
		'penduduk.nama',
		'surat_template.nama',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
