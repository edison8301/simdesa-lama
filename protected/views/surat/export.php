<?php
$this->breadcrumbs=array(
	'Surats'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Surat','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Surat','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Surat','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Surat','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Surat</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nomor',
		'penduduk_id',
		'surat_template_id',
),
)); ?>
