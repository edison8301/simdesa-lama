<?php
$this->breadcrumbs=array(
	'Surats',
);

$this->menu=array(
	array('label'=>'Tambah Surat','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Surat','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Surats</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
