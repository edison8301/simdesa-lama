<?php
$this->breadcrumbs=array(
	'Penduduks'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Kelola Surat','url'=>array('surat/admin'),'icon'=>'tags'),
	array('label'=>'Kelola Template','url'=>array('suratTemplate/admin'),'icon'=>'tags'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('penduduk-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Pilih Penduduk</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'penduduk-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'nama',
		array(
				'class'=>'CDataColumn',
				'name'=>'jenis_kelamin_id',
				'header'=>'Jenis Kelamin',
				'type'=>'raw',
				'value'=>'$data->jenisKelamin->nama',
				'filter'=>CHtml::listData(JenisKelamin::model()->findAll(),'id','nama')
		),
		'nomor_ktp',
		array(
			'class'=>'CDataColumn',
			'name'=>'id',
			'header'=>'',
			'type'=>'raw',
			'value'=>'CHtml::link("<i class=\'icon-envelope\'></i> Buat Surat",array("surat/create","penduduk_id"=>$data->id))',
		),
		/*
		'agama_id',
		'pendidikan_id',
		'pekerjaan_id',
		'dapat_membaca',
		'kewarganegaraan_id',
		'alamat',
		'kedudukan_dalam_keluarga_id',
		'nomor_ktp',
		'nomor_ksk',
		'keterangan',
		*/
		),
)); ?>
