<?php
$this->breadcrumbs=array(
	'Surats'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah Surat','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat Surat','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola Surat','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting Surat</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>