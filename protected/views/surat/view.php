<?php
$this->breadcrumbs=array(
	'Surat'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Surat','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Surat','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Surat','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Surat','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Surat</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'nomor',
		array(
			'label'=>'Penduduk',
			'value'=>$model->penduduk->nama
		),
		array(
			'label'=>'Template',
			'value'=>$model->surat_template->nama
		),
		
),
)); ?>

<h2>Atribut Surat</h2>

<?php print CHtml::beginForm(); ?>

<?php foreach($model->surat_template->getDataSuratTemplateAtribut() as $data) { ?>
	<?php print CHtml::label($data->key,''); ?>
	<?php print CHtml::textField('SuratAtribut['.$data->key.']',$model->getSuratAtributValueByKey($data->key)); ?>
<?php } ?>

<?php print CHtml::hiddenField('cetak','1'); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Cetak Surat','icon'=>'ok white','type'=>'primary')); ?>
</div>

<?php print CHtml::endForm(); ?>
