<?php
$this->breadcrumbs=array(
	'Surat Atributs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola SuratAtribut','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tambah SuratAtribut</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>