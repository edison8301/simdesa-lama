<?php
$this->breadcrumbs=array(
	'Surat Atributs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah SuratAtribut','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting SuratAtribut','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus SuratAtribut','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola SuratAtribut','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat SuratAtribut</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'surat_id',
		'key',
		'value',
),
)); ?>
