<?php
$this->breadcrumbs=array(
	'Surat Atributs',
);

$this->menu=array(
	array('label'=>'Tambah SuratAtribut','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola SuratAtribut','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Surat Atributs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
