<?php
$this->breadcrumbs=array(
	'Surat Atributs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah SuratAtribut','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat SuratAtribut','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola SuratAtribut','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting SuratAtribut</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>