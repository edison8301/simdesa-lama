<?php
$this->breadcrumbs=array(
	'Format/Template'=>array('admin'),
	'Kelola',
);

?>

<h1>Format/Template</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'surat-template-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'CDataColumn',
				'header'=>'File Template',
				'type'=>'raw',
				'name'=>'file',
				'value'=>'$data->file != null ? CHtml::link("<i class=\'icon-download-alt\'></i>Unduh Template",Yii::app()->request->baseUrl."/uploads/suratTemplate/".$data->file) : "" ',
			),
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
