<?php
$this->breadcrumbs=array(
	'Surat Templates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola SuratTemplate','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tambah SuratTemplate</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>