<?php
$this->breadcrumbs=array(
	'Surat Templates'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah SuratTemplate','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting SuratTemplate','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus SuratTemplate','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola SuratTemplate','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat SuratTemplate</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'nama',
		'file',
),
)); ?>
