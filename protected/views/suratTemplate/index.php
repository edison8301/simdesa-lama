<?php
$this->breadcrumbs=array(
	'Surat Templates',
);

$this->menu=array(
	array('label'=>'Tambah SuratTemplate','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola SuratTemplate','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Surat Templates</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
