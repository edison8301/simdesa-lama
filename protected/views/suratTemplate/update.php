<?php
$this->breadcrumbs=array(
	'Format Surat'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Sunting',
);

	$this->menu=array(
		array('label'=>'Tambah SuratTemplate','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat SuratTemplate','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola SuratTemplate','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting Template</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>