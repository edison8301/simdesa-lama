<?php
$this->breadcrumbs=array(
	'Surat Templates'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Atribut','url'=>array('suratTemplateAtribut/create','surat_template_id'=>$model->id),'icon'=>'plus'),
	array('label'=>'Tambah Template','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Template','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Template','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Template','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat Template</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nama',
			array(
				'label'=>'File Template',
				'type'=>'raw',
				'value'=>CHtml::link("<i class='icon-download-alt'></i> Unduh File",Yii::app()->request->baseUrl."/uploads/suratTemplate/".$model->file,array('target'=>'_blank'))
			),
		),
)); ?>

<h2>Atribut Template</h2>

<table class="table">
<tr>
	<th>No</th>
	<th>Nama Atribut</th>
	<th>&nbsp;</th>
</tr>
<?php $i=1; foreach($model->getDataSuratTemplateAtribut() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->key; ?></td>
	<td>
		<?php print CHtml::link('<i class="icon-pencil"></i>',array('suratTemplateAtribut/update','id'=>$data->id)); ?>
		<?php print CHtml::link("<i class='icon-trash'></i>",'#',array('submit'=>array('suratTemplateAtribut/delete','id'=>$data->id),params=>array('returnUrl'=>$this->createUrl('suratTemplate/view',array('id'=>$model->id))),'confirm'=>'Yakin akan menghapus data?')); ?>

	</td>
</tr>
<?php $i++; } ?>
</table>
