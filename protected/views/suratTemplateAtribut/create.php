<?php
$this->breadcrumbs=array(
	'Surat Template Atributs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola SuratTemplateAtribut','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tambah Atribut</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>