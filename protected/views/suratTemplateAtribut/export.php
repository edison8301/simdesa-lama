<?php
$this->breadcrumbs=array(
	'Surat Template Atributs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah SuratTemplateAtribut','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting SuratTemplateAtribut','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus SuratTemplateAtribut','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola SuratTemplateAtribut','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat SuratTemplateAtribut</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'surat_template_id',
		'key',
		'tipe',
),
)); ?>
