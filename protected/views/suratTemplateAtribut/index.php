<?php
$this->breadcrumbs=array(
	'Surat Template Atributs',
);

$this->menu=array(
	array('label'=>'Tambah SuratTemplateAtribut','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola SuratTemplateAtribut','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Surat Template Atributs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
