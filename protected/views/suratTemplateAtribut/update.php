<?php
$this->breadcrumbs=array(
	'Surat Template Atributs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah SuratTemplateAtribut','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat SuratTemplateAtribut','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola SuratTemplateAtribut','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting SuratTemplateAtribut</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>