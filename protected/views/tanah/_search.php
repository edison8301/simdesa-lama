<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'jumlah',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'hm',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'hgb',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'hp',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'hgu',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'hpl',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'ma',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'vi',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'tn',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'perumahan',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'perdagangan',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'perkantoran',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'industri',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'fasilitas_umum',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'sawah',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'tegalan',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'perkebunan',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'peternakan',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'hutan_belukar',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'hutan_lindung',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'tanah_kosong',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'lain_lain',array('class'=>'span5','maxlength'=>10)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'icon'=>'search white',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
