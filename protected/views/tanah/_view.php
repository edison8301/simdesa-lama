<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hm')); ?>:</b>
	<?php echo CHtml::encode($data->hm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hgb')); ?>:</b>
	<?php echo CHtml::encode($data->hgb); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hp')); ?>:</b>
	<?php echo CHtml::encode($data->hp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hgu')); ?>:</b>
	<?php echo CHtml::encode($data->hgu); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('hpl')); ?>:</b>
	<?php echo CHtml::encode($data->hpl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ma')); ?>:</b>
	<?php echo CHtml::encode($data->ma); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi')); ?>:</b>
	<?php echo CHtml::encode($data->vi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tn')); ?>:</b>
	<?php echo CHtml::encode($data->tn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perumahan')); ?>:</b>
	<?php echo CHtml::encode($data->perumahan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perdagangan')); ?>:</b>
	<?php echo CHtml::encode($data->perdagangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perkantoran')); ?>:</b>
	<?php echo CHtml::encode($data->perkantoran); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('industri')); ?>:</b>
	<?php echo CHtml::encode($data->industri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fasilitas_umum')); ?>:</b>
	<?php echo CHtml::encode($data->fasilitas_umum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sawah')); ?>:</b>
	<?php echo CHtml::encode($data->sawah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tegalan')); ?>:</b>
	<?php echo CHtml::encode($data->tegalan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perkebunan')); ?>:</b>
	<?php echo CHtml::encode($data->perkebunan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('peternakan')); ?>:</b>
	<?php echo CHtml::encode($data->peternakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hutan_belukar')); ?>:</b>
	<?php echo CHtml::encode($data->hutan_belukar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hutan_lindung')); ?>:</b>
	<?php echo CHtml::encode($data->hutan_lindung); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanah_kosong')); ?>:</b>
	<?php echo CHtml::encode($data->tanah_kosong); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lain_lain')); ?>:</b>
	<?php echo CHtml::encode($data->lain_lain); ?>
	<br />

	*/ ?>

</div>