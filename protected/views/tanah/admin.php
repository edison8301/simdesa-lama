<?php
$this->breadcrumbs=array(
	'Tanah'=>array('admin'),
	'Kelola',
);

?>

<h1>Tanah di Desa</h1>



<div style="overflow:auto">
<?php $this->widget('TbGridViewPlus',array(
		'id'=>'tanah-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'addingHeaders' => array(	
			array(''=>2,'Status Hak Tanah (m<sup>2</sup>)'=>8,'Penggunaan Tanah'=>13,'&nbsp;'=>1),
			array(''=>2,'Sudah Bersertifikat'=>5,'Belum Bersertifikat'=>3,'Non Pertanian'=>5,'Pertanian'=>8,'&nbsp;'=>1),
		
		),
		
		'columns'=>array(
			array(
				'name'=>'nama',
				'headerHtmlOptions'=>array('style'=>'min-width: 200px'),
			),
			'jumlah',
			'hm',
			'hgb',
			'hp',
			'hgu',
			'hpl',
			'ma',
			'vi',
			'tn',
			'perumahan',
			'perdagangan',
			'perkantoran',
			'industri',
			'fasilitas_umum',
			'sawah',
			'tegalan',
			'perkebunan',
			'peternakan',
			'hutan_belukar',
			'hutan_lindung',
			'tanah_kosong',
			'lain_lain',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
)); ?>

</div>
