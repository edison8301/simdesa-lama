<?php
$this->breadcrumbs=array(
	'Tanahs',
);

$this->menu=array(
	array('label'=>'Tambah Tanah','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola Tanah','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tanahs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
