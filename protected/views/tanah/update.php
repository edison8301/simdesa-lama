<?php
$this->breadcrumbs=array(
	'Tanahs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
		array('label'=>'Tambah Tanah','url'=>array('create'),'icon'=>'plus'),
		array('label'=>'Lihat Tanah','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
		array('label'=>'Kelola Tanah','url'=>array('admin'),'icon'=>'th-list'),
	);
	?>

	<h1>Sunting Tanah</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>