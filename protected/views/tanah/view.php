<?php
$this->breadcrumbs=array(
	'Tanahs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah Tanah','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting Tanah','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus Tanah','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Tanah','url'=>array('admin'),'icon'=>'th-list'),
	array('label'=>'Export Single','url'=>array('export','id'=>$model->id),'icon'=>'download-alt'),
	array('label'=>'Export All','url'=>array('exportall'),'icon'=>'download-alt'),
	
);
?>

<h1>Lihat Tanah</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'nama',
		array(
			'label'=>'Jumlah',
			'type'=>'raw',
			'value'=>$model->jumlah.' m<sup>2</sup>'
		),
		array(
			'label'=>'HM',
			'type'=>'raw',
			'value'=>$model->hm.' m<sup>2</sup>'
		),
		array(
			'label'=>'HGB',
			'type'=>'raw',
			'value'=>$model->hgb.' m<sup>2</sup>'
		),
		array(
			'label'=>'HP',
			'type'=>'raw',
			'value'=>$model->hp.' m<sup>2</sup>'
		),
		array(
			'label'=>'HGU',
			'type'=>'raw',
			'value'=>$model->hgu.' m<sup>2</sup>'
		),
		array(
			'label'=>'HPL',
			'type'=>'raw',
			'value'=>$model->hpl.' m<sup>2</sup>'
		),
		array(
			'label'=>'MA',
			'type'=>'raw',
			'value'=>$model->ma.' m<sup>2</sup>'
		),
		array(
			'label'=>'VI',
			'type'=>'raw',
			'value'=>$model->vi.' m<sup>2</sup>'
		),
		array(
			'label'=>'TN',
			'type'=>'raw',
			'value'=>$model->tn.' m<sup>2</sup>'
		),
		array(
			'label'=>'Perumahan',
			'type'=>'raw',
			'value'=>$model->perumahan.' m<sup>2</sup>'
		),
		array(
			'label'=>'Perdagangan',
			'type'=>'raw',
			'value'=>$model->perdagangan.' m<sup>2</sup>'
		),
		array(
			'label'=>'Perkantoran',
			'type'=>'raw',
			'value'=>$model->perkantoran.' m<sup>2</sup>'
		),
		array(
			'label'=>'Industri',
			'type'=>'raw',
			'value'=>$model->industri.' m<sup>2</sup>'
		),
		array(
			'label'=>'Fasilitas Umum',
			'type'=>'raw',
			'value'=>$model->fasilitas_umum.' m<sup>2</sup>'
		),
		array(
			'label'=>'Sawah',
			'type'=>'raw',
			'value'=>$model->sawah.' m<sup>2</sup>'
		),
		array(
			'label'=>'Tegalan',
			'type'=>'raw',
			'value'=>$model->tegalan.' m<sup>2</sup>'
		),
		array(
			'label'=>'Perkebunan',
			'type'=>'raw',
			'value'=>$model->perkebunan.' m<sup>2</sup>'
		),
		array(
			'label'=>'Peternakan',
			'type'=>'raw',
			'value'=>$model->peternakan.' m<sup>2</sup>'
		),
		array(
			'label'=>'Hutan Belukar',
			'type'=>'raw',
			'value'=>$model->hutan_belukar.' m<sup>2</sup>'
		),
		array(
			'label'=>'Hutan Lindung',
			'type'=>'raw',
			'value'=>$model->hutan_lindung.' m<sup>2</sup>'
		),
		array(
			'label'=>'Tanah Kosong',
			'type'=>'raw',
			'value'=>$model->tanah_kosong.' m<sup>2</sup>'
		),
		array(
			'label'=>'Lain-lain',
			'type'=>'raw',
			'value'=>$model->lain_lain.' m<sup>2</sup>'
		),
),
)); ?>
