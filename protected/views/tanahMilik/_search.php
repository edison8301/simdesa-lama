<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'asal_tanah',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'nomor_sertifikat',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'luas',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'klas',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'asli',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'bantuan_pemerintah',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'bantuan_provinsi',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'bantuan_kabkota',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'lain_lain',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tanggal_perolehan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'sawah',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tegal',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'kebun',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tambak',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tanah_kering',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'patok_ada',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'patok_tidak_ada',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'papan_ada',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'papan_tidak_ada',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'lokasi',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'peruntukkan',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'icon'=>'search white',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
