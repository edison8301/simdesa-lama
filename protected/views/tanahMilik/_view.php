<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('asal_tanah')); ?>:</b>
	<?php echo CHtml::encode($data->asal_tanah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_sertifikat')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_sertifikat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('luas')); ?>:</b>
	<?php echo CHtml::encode($data->luas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('klas')); ?>:</b>
	<?php echo CHtml::encode($data->klas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('asli')); ?>:</b>
	<?php echo CHtml::encode($data->asli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bantuan_pemerintah')); ?>:</b>
	<?php echo CHtml::encode($data->bantuan_pemerintah); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bantuan_provinsi')); ?>:</b>
	<?php echo CHtml::encode($data->bantuan_provinsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bantuan_kabkota')); ?>:</b>
	<?php echo CHtml::encode($data->bantuan_kabkota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lain_lain')); ?>:</b>
	<?php echo CHtml::encode($data->lain_lain); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_perolehan')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_perolehan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sawah')); ?>:</b>
	<?php echo CHtml::encode($data->sawah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tegal')); ?>:</b>
	<?php echo CHtml::encode($data->tegal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kebun')); ?>:</b>
	<?php echo CHtml::encode($data->kebun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tambak')); ?>:</b>
	<?php echo CHtml::encode($data->tambak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanah_kering')); ?>:</b>
	<?php echo CHtml::encode($data->tanah_kering); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('patok_ada')); ?>:</b>
	<?php echo CHtml::encode($data->patok_ada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('patok_tidak_ada')); ?>:</b>
	<?php echo CHtml::encode($data->patok_tidak_ada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('papan_ada')); ?>:</b>
	<?php echo CHtml::encode($data->papan_ada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('papan_tidak_ada')); ?>:</b>
	<?php echo CHtml::encode($data->papan_tidak_ada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lokasi')); ?>:</b>
	<?php echo CHtml::encode($data->lokasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('peruntukkan')); ?>:</b>
	<?php echo CHtml::encode($data->peruntukkan); ?>
	<br />

	*/ ?>

</div>