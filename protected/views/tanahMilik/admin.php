<?php
$this->breadcrumbs=array(
	'Tanah Miliks'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('tanah-milik-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Tanah Kas Desa</h1>


<div style="overflow:auto">
<?php $this->widget('TbGridViewPlus',array(
		'id'=>'tanah-milik-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'addingHeaders' => array(	
			array(''=>4,'Perolehan TKD'=>6,'Jenis TKD'=>5,'Patok Tanda Batas'=>2,'Papan Nama'=>2,'&nbsp;'=>2),
		),
		'columns'=>array(
			array(
				'name'=>'asal_tanah',
				'headerHtmlOptions'=>array('style'=>'min-width: 200px'),
			),
			array(
				'name'=>'nomor_sertifikat',
				'headerHtmlOptions'=>array('style'=>'min-width: 200px'),
			),
			'luas',
			'klas',
			'asli',
			'bantuan_pemerintah',
			'bantuan_provinsi',
			'bantuan_kabkota',
			'lain_lain',
			'tanggal_perolehan',
			'sawah',
			'tegal',
			'kebun',
			'tambak',
			'tanah_kering',
			'patok_ada',
			'patok_tidak_ada',
			'papan_ada',
			'papan_tidak_ada',
			'lokasi',
			'peruntukkan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>

</div>
