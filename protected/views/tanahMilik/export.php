<?php
$this->breadcrumbs=array(
	'Tanah Miliks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah TanahMilik','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting TanahMilik','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus TanahMilik','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola TanahMilik','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat TanahMilik</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'asal_tanah',
		'nomor_sertifikat',
		'luas',
		'klas',
		'asli',
		'bantuan_pemerintah',
		'bantuan_provinsi',
		'bantuan_kabkota',
		'lain_lain',
		'tanggal_perolehan',
		'sawah',
		'tegal',
		'kebun',
		'tambak',
		'tanah_kering',
		'patok_ada',
		'patok_tidak_ada',
		'papan_ada',
		'papan_tidak_ada',
		'lokasi',
		'peruntukkan',
),
)); ?>
