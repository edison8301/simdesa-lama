<?php
$this->breadcrumbs=array(
	'Tanah Miliks',
);

$this->menu=array(
	array('label'=>'Tambah TanahMilik','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola TanahMilik','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tanah Miliks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
