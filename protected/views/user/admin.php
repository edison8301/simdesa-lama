<?php
$this->breadcrumbs=array(
	'Users'=>array('admin'),
	'Kelola',
);

?>

<h1>User</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'user-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'username',
		array(
			'class'=>'CDataColumn',
			'header'=>'Password',
			'name'=>'password',
			'type'=>'raw',
			'value'=>'$data->hidePassword()'
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
),
)); ?>
