<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola User','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tambah User</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>