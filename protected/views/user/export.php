<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Tambah User','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Sunting User','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Hapus User','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola User','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Lihat User</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
		'id',
		'username',
		'password',
		'role_id',
),
)); ?>
