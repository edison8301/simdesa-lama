<?php
$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'Tambah User','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Kelola User','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Users</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
