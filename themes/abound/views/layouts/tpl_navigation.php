<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="#">SIM Desa</a>
          
          <div class="nav-collapse">
			<?php $this->widget('bootstrap.widgets.TbMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(
                        array('label'=>'Dashboard','url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Umum','icon'=>'tasks white','visible'=>!Yii::app()->user->isGuest,'url'=>'','items'=>array(
							array('label'=>'Peraturan Desa','url'=>array('peraturanDesa/admin'),'icon'=>'tags'),
							array('label'=>'Keputusan Kepala Desa','url'=>array('keputusanKades/admin'),'icon'=>'tags'),
							array('label'=>'Inventaris Desa','url'=>array('inventarisDesa/admin'),'icon'=>'tags'),
							array('label'=>'Aparat Pemerintah Desa','url'=>array('aparatPemerintah/admin'),'icon'=>'tags'),
							array('label'=>'Tanah Milik Desa','url'=>array('tanahMilik/admin'),'icon'=>'tags'),
							array('label'=>'Tanah di Desa','url'=>array('tanah/admin'),'icon'=>'tags'),
							array('label'=>'Agenda','url'=>array('agenda/admin'),'icon'=>'tags'),
							array('label'=>'Ekspedisi','url'=>array('ekspedisi/admin'),'icon'=>'tags'),
						
						)),
                        array('label'=>'Penduduk', 'icon'=>'user white','url'=>'#','visible'=>!Yii::app()->user->isGuest,'items'=>array(
							array('label'=>'Data Induk Penduduk','url'=>array('penduduk/admin'),'icon'=>'tags'),
							array('label'=>'Mutasi Penduduk','url'=>array('penduduk/mutasi'),'icon'=>'tags'),
							array('label'=>'Rekapitulasi Penduduk','url'=>array('penduduk/rekapitulasi'),'icon'=>'tags'),
							array('label'=>'Penduduk Sementara','url'=>array('pendudukSementara/admin'),'icon'=>'tags'),
							'---',
							array('label'=>'Usia Balita','url'=>array('penduduk/usiaBalita'),'icon'=>'tags'),
							array('label'=>'Wajib Belajar','url'=>array('penduduk/wajibBelajar'),'icon'=>'tags'),
							array('label'=>'Pemilih Pemilu','url'=>array('penduduk/pemilihPemilu'),'icon'=>'tags'),
							'---',
							array('label'=>'Laporan Pendidikan','url'=>array('penduduk/reportPendidikan'),'icon'=>'tags'),
							array('label'=>'Laporan Agama','url'=>array('penduduk/reportAgama'),'icon'=>'tags'),
							array('label'=>'Laporan Pekerjaan','url'=>array('penduduk/reportPekerjaan'),'icon'=>'tags'),
							array('label'=>'Laporan Kelompok Umur','url'=>array('penduduk/reportKelompokUmur'),'icon'=>'tags'),
							array('label'=>'Laporan Dusun','url'=>array('penduduk/reportDusun'),'icon'=>'tags'),
							array('label'=>'Laporan Status Perkawinan','url'=>array('penduduk/reportStatusPerkawinan'),'icon'=>'tags'),
							array('label'=>'Laporan Kewarganegaraan','url'=>array('penduduk/reportKewarganegaraan'),'icon'=>'tags'),
							array('label'=>'Laporan Kedudukan Keluarga','url'=>array('penduduk/reportKedudukanDalamKeluarga'),'icon'=>'tags'),
							
						)),
						array('label'=>'Keuangan', 'icon'=>'book white', 'url'=>'#','visible'=>!Yii::app()->user->isGuest,'items'=>array(
							//array('label'=>'Anggaran Penerimaan','url'=>array('anggaran/penerimaan'),'icon'=>'tags'),
							//array('label'=>'Anggaran Pengeluaran','url'=>array('anggaran/pengeluaran'),'icon'=>'tags'),
							array('label'=>'Buku Kas Umum','url'=>array('kas/umum'),'icon'=>'tags'),
							array('label'=>'Buku Kas Penerimaan','url'=>array('kas/penerimaan'),'icon'=>'tags'),
							array('label'=>'Buku Kas Pengeluaran','url'=>array('kas/pengeluaran'),'icon'=>'tags'),
						)),
						array('label'=>'Pembangunan','icon'=>'home white', 'url'=>'#','visible'=>!Yii::app()->user->isGuest,'items'=>array(
							array('label'=>'Rencana Pembangunan','url'=>array('rencanaPembangunan/admin'),'icon'=>'tags'),
							array('label'=>'Inventaris Proyek','url'=>array('inventarisProyek/admin'),'icon'=>'tags'),
							array('label'=>'Kegiatan Pembangunan','url'=>array('kegiatanPembangunan/admin'),'icon'=>'tags'),
							array('label'=>'Kader Pembangunan','url'=>array('kaderPembangunan/admin'),'icon'=>'tags'),
						)),
						array('label'=>'BPD','icon'=>'home white', 'url'=>'#','visible'=>!Yii::app()->user->isGuest,'items'=>array(
							array('label'=>'Anggota BPD','url'=>array('anggotaBpd/admin'),'icon'=>'tags'),
							array('label'=>'Keputusan BPD','url'=>array('keputusanBpd/admin'),'icon'=>'tags'),
							array('label'=>'Kegiatan BPD','url'=>array('kegiatanBpd/admin'),'icon'=>'tags'),
							array('label'=>'Agenda BPD','url'=>array('agendaBpd/admin'),'icon'=>'tags'),
							array('label'=>'Ekspedisi BPD','url'=>array('ekspedisiBpd/admin'),'icon'=>'tags'),
						)),
						array('label'=>'Surat', 'icon'=>'envelope white', 'url'=>'#','visible'=>!Yii::app()->user->isGuest,'items'=>array(
							array('label'=>'Surat','url'=>array('surat/admin'),'icon'=>'tags'),
							array('label'=>'Format/Template','url'=>array('suratTemplate/admin'),'icon'=>'tags'),
						)),
                        array('label'=>'Pengaturan','icon'=>'wrench white', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest,'items'=>array(
							array('label'=>'Profil','url'=>array('user/admin'),'icon'=>'tags'),
							array('label'=>'Dusun','url'=>array('dusun/admin'),'icon'=>'tags'),
							array('label'=>'Pekerjaan','url'=>array('pekerjaan/admin'),'icon'=>'tags'),
							array('label'=>'Pendidikan','url'=>array('pendidikan/admin'),'icon'=>'tags'),
							array('label'=>'Agama','url'=>array('agama/admin'),'icon'=>'tags'),
							array('label'=>'Status Perkawinan','url'=>array('statusPerkawinan/admin'),'icon'=>'tags'),
							array('label'=>'Kedudukan Dlm Keluarga','url'=>array('kedudukanDalamKeluarga/admin'),'icon'=>'tags'),
							array('label'=>'Kewarganegaraan','url'=>array('kewarganegaraan/admin'),'icon'=>'tags'),
							array('label'=>'User','url'=>array('user/admin'),'icon'=>'tags'),
						)),
						array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
						
                    ),
                )); ?>
    	</div>
    </div>
	</div>
</div>

<?php /*
<div class="subnav navbar navbar-fixed-top">
    <div class="navbar-inner">
    	<div class="container">
        
        	<div class="style-switcher pull-left">
                <a href="javascript:chooseStyle('none', 60)" checked="checked"><span class="style" style="background-color:#0088CC;"></span></a>
                <a href="javascript:chooseStyle('style2', 60)"><span class="style" style="background-color:#7c5706;"></span></a>
                <a href="javascript:chooseStyle('style3', 60)"><span class="style" style="background-color:#468847;"></span></a>
                <a href="javascript:chooseStyle('style4', 60)"><span class="style" style="background-color:#4e4e4e;"></span></a>
                <a href="javascript:chooseStyle('style5', 60)"><span class="style" style="background-color:#d85515;"></span></a>
                <a href="javascript:chooseStyle('style6', 60)"><span class="style" style="background-color:#a00a69;"></span></a>
                <a href="javascript:chooseStyle('style7', 60)"><span class="style" style="background-color:#a30c22;"></span></a>
          	</div>
           <form class="navbar-search pull-right" action="">
           	 
           <input type="text" class="search-query span2" placeholder="Search">
           
           </form>
    	</div><!-- container -->
    </div><!-- navbar-inner -->
</div><!-- subnav -->
*/ ?>