<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="row">
	
    <div style="width:300px;margin-left:auto;margin-right:auto;">
	<?php $this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>"Login",
	)); ?>

		<div class="form" style="padding:15px;text-align:center;padding-bottom:5px;">
		<p style="text-align:center">
			<img src="<?php print Yii::app()->request->baseUrl; ?>/images/logo.png" style="width:80px">
		</p>
		<p style="text-align:center;font-weight:bold;font-size:18px;border-bottom:1px solid #DDDDDD;padding-bottom:15px;margin-bottom:15px;">
			Desa <?php print Pengaturan::model()->findByPk(1)->nilai; ?>
		</p>
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
        
		<div class="">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('style'=>'text-align:center')); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
    
		<div class="">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('style'=>'text-align:center')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
    
		<div class="row buttons">
			<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','label'=>'Login','type'=>'primary','icon'=>'lock white')); ?>
		</div>
		
		
		<?php $this->endWidget(); ?>
		</div><!-- form -->
		
		<div class="login-logo">
		
			
		
		</div>

	<?php $this->endWidget();?>

    </div>

</div>

